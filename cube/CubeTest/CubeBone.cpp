#include "stdafx.h"
#include "CubeBone.h"


CubeBone::CubeBone(int _cube_size)
	:cube_size(_cube_size)
{
//	state = CubeBoneState::State_Rotate;
	int i = 0;
	v.resize(cube_size);
	for (int y_i = 0; y_i < cube_size; ++y_i) {
		v[y_i].resize(cube_size);
		for (int z_i = 0; z_i < cube_size; ++z_i) {
			for (int x_i = 0; x_i < cube_size; ++x_i) {
				v[y_i][z_i].push_back(CubeR(i++, x_i, y_i, z_i));
			}
		}
	}

}



void CubeBone::update()
{
	switch (state)
	{
	case CubeBoneState::State_Stop:
	{
		if(input.dir != 0)
			state = CubeBoneState::State_Rotate;
		break;
	}
	case CubeBoneState::State_Oper:
	{
		state = CubeBoneState::State_Rotate;
		break;
	}
	case CubeBoneState::State_Rotate:
	{
		// 조작이 끝나고 
		if (input.dir == 0) {
			state = CubeBoneState::State_Replace;
			break;
		}
		RotateDir dir = RotateDir::DIR_NOTHING;
		switch (input.dir) {
		case 1:
			dir = RotateDir::DIR_XCW;		break;
		case 2:
			dir = RotateDir::DIR_XCCW;		break;
		case 3:
			dir = RotateDir::DIR_YCW;		break;
		case 4:
			dir = RotateDir::DIR_YCCW;		break;
		case 5:
			dir = RotateDir::DIR_ZCW;		break;
		case 6:
			dir = RotateDir::DIR_ZCCW;		break;
		defalut:
			break;
		}
		rotateCube(dir, input.number);
		input.zero();
		state = CubeBoneState::State_Replace;
		break;
	}
	case CubeBoneState::State_Replace:
	{
		state = CubeBoneState::State_Stop;
		break;
	}
	default:
		state = CubeBoneState::State_Stop;
		break;
	}
}

model_coor CubeBone::getData()
{
	model_coor result;
	for (auto &i : v)
		for (auto &j : i)
			for (auto &k : j)
				result.v.push_back(CubeR(k));
	//	result.v = r;
	return result;
}

bool CubeBone::rotateCube(RotateDir _dir, int _linenumber)
{
	if (_linenumber >= cube_size)
		return false;
	std::vector<CubeR> v_cube;
	switch (_dir)
	{
	case RotateDir::DIR_XCW:
	case RotateDir::DIR_XCCW:
		for (int y = 0; y < cube_size; ++y) {
			for (int z = 0; z < cube_size; ++z) {
				v_cube.push_back(v[y][z][_linenumber]);
			}
		}
		break;

	case RotateDir::DIR_YCW:
	case RotateDir::DIR_YCCW:
		for (int z = 0; z < cube_size; ++z) {
			for (int x = 0; x < cube_size; ++x) {
				v_cube.push_back(v[_linenumber][z][x]);
			}
		}
		break;

	case RotateDir::DIR_ZCW:
	case RotateDir::DIR_ZCCW:
		for (int y = 0; y < cube_size; ++y) {
			for (int x = 0; x < cube_size; ++x) {
				v_cube.push_back(v[y][_linenumber][x]);
			}
		}
		break;
	default:
		break;
	}

	for (auto &i : v_cube)
		i.rotateCube(cube_size, _dir);

	for (auto &i : v_cube) {
		v[i.getY()][i.getZ()][i.getX()] = i;
	}
	return true;
}

CubeBone::~CubeBone()
{
}
