#include "stdafx.h"
#include "CubeR.h"


AxisDir CubeR::findAxis(Axis _a)
{
	if (_a.x == 1)
		return AxisDir::DIR_XP;
	if (_a.x == -1)
		return AxisDir::DIR_XM;
	if (_a.y == 1)
		return AxisDir::DIR_YP;
	if (_a.y == -1)
		return AxisDir::DIR_YM;
	if (_a.z == 1)
		return AxisDir::DIR_ZP;
	if (_a.z == -1)
		return AxisDir::DIR_ZM;
	return AxisDir::DIR_ERROR;
}

CubeR::CubeR(int _id, int _x, int _y, int _z)
	: id(_id), pos{ _x,_y,_z }
{
	localRight = { 1,0,0 };
	localLook = { 0,0,1 };
}



CubeR::~CubeR()
{
}

void CubeR::rotateCube(int _cubeSize, RotateDir _dir)
{
	int mid = _cubeSize - 1;
	pos[0] *= 2; pos[1] *= 2; pos[2] *= 2;
	pos[0] -= mid; pos[1] -= mid; pos[2] -= mid;

	int pos_temp[3];
	Axis lar_temp;	// local axis right temp
	Axis lal_temp;	// local axis look temp
	switch (_dir) {
	case RotateDir::DIR_XCW:
		pos_temp[0] = pos[0];
		pos_temp[1] = pos[2];
		pos_temp[2] = -pos[1];
		lar_temp = { localRight.x, localRight.z,-localRight.y };
		lal_temp = { localLook.x, localLook.z,-localLook.y };
		//m = { 1,0,0,0,0,1,0,-1,0 };
		break;
	case RotateDir::DIR_XCCW:
		pos_temp[0] = pos[0];
		pos_temp[1] = -pos[2];
		pos_temp[2] = pos[1];
		//m = { 1,0,0,0,0,-1,0,1,0 };
		lar_temp = { localRight.x, -localRight.z,localRight.y };
		lal_temp = { localLook.x, -localLook.z,localLook.y };
		break;
	case RotateDir::DIR_YCW:
		pos_temp[0] = -pos[2];
		pos_temp[1] = pos[1];
		pos_temp[2] = pos[0];
		//m = { 0,0,-1,0,1,0,1,0,0 };
		lar_temp = { -localRight.z, localRight.y,localRight.x };
		lal_temp = { -localLook.z, localLook.y,localLook.x };
		break;
	case RotateDir::DIR_YCCW:
		pos_temp[0] = pos[2];
		pos_temp[1] = pos[1];
		pos_temp[2] = -pos[0];
		//m = { 0,0,1,0,1,0,-1,0,0 };
		lar_temp = { localRight.z, localRight.y,-localRight.x };
		lal_temp = { localLook.z, localLook.y,-localLook.x };
		break;
	case RotateDir::DIR_ZCW:
		pos_temp[0] = pos[1];
		pos_temp[1] = -pos[0];
		pos_temp[2] = pos[2];
		lar_temp = { localRight.y, -localRight.x,localRight.z };
		lal_temp = { localLook.y, -localLook.x,localLook.z };
		//m = { 0,1,0,-1,0,0,0,0,1 };
		break;
	case RotateDir::DIR_ZCCW:
		pos_temp[0] = -pos[1];
		pos_temp[1] = pos[0];
		pos_temp[2] = pos[2];
		lar_temp = { -localRight.y, localRight.x,localRight.z };
		lal_temp = { -localLook.y, localLook.x,localLook.z };
		//m = { 0,-1,0,1,0,0,0,0,1 };
		break;
	}
	pos[0] = mid + pos_temp[0];
	pos[1] = mid + pos_temp[1];
	pos[2] = mid + pos_temp[2];
	pos[0] /= 2; pos[1] /= 2; pos[2] /= 2;
	localRight = lar_temp;
	localLook = lal_temp;
}
