#pragma once
#include "CubeR.h"
#include <vector>
#include <array>

enum CubeBoneState {
	State_Stop,		// 조작이 없는 상태
	State_Oper,		// 조작중인 상태
	State_Rotate,	// 조작이 끝나 회전하는 상태
	State_Replace	// 새로 바뀐 형태로 다시 저장하는 상태
};


struct model_coor {
	std::vector<CubeR> v;
//	DirectX::XMFLOAT4 f;
};

enum keyinput : int{
	nothing,
	x0cw,
	x0ccw,
	x1cw,
	x1ccw,
	x2cw,
	x2ccw, 
	y0cw,
	y0ccw,
	y1cw,
	y1ccw,
	y2cw,
	y2ccw,
	z0cw,
	z0ccw,
	z1cw,
	z1ccw,
	z2cw,
	z2ccw,
};

struct keystruct {
	int dir;
	int number;
	keystruct() {};
	keystruct(int _dir, int _num) :dir(_dir), number(_num) {};
	void zero() { number = dir = 0; }
};

class CubeBone
{
	std::vector<std::vector<std::vector<CubeR>>> v;
	CubeBoneState state = CubeBoneState::State_Rotate;;
	int cube_size;	// 큐브의 크기 
	float angle;
	keystruct input;


public:
	CubeBone(int _size);
	~CubeBone();
	void update();
	model_coor getData();
	const CubeBoneState getState() { return state; }
	void setState(CubeBoneState _state) { state= _state; }
	bool rotateCube(RotateDir _dir, int _linenumber);
	void keyInput(keystruct _k) { input = _k; }
};