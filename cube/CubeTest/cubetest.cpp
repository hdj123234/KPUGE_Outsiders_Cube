// cubetest.cpp
//
#include "stdafx.h"
#include "CubeBone.h"
#include <Windows.h>
#include <iostream>
#include <thread>
#include <mutex>
using namespace std;


struct cubemodel {
	int id;
	int top, front, left, right, bottom, back;
	cubemodel() {};
	cubemodel(int _id, int _top, int _front, int _left, int _right, int _bottom, int _back)
		: id(_id), top(_top), front(_front), left(_left), right(_right), bottom(_bottom), back(_back) {}

	void operator=(const cubemodel &rmodel) {
		id = rmodel.id;
		top = rmodel.top;
		front = rmodel.front;
		left = rmodel.left;
		right = rmodel.right;
		bottom = rmodel.bottom;
		back = rmodel.back;
	}
};


cubemodel model[27];
CubeBone c(3);

void 스랜(int _value) {
	if (_value / 10 == 0)
		cout << " ";
	cout << _value;
}
void 색랜(int _color) {
	switch (_color) {
	case 0:
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0);
		cout << "검";
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		break;
	case 1:
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
		cout << "빨";
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		break;
	case 5:
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
		cout << "보";
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		break;
	case 2:
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
		cout << "노";
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		break;
	case 3:
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		cout << "초";
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		break;
	case 4:
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 9);
		cout << "파";
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		break;
	case 6:
		cout << "흰";
		break;
	}
}

// 0, 1.... = top, front, left, right, bottom, back
void 통랜(CubeR _c, int _face)
{
	cubemodel ciw = model[_c.getID()];
	auto look = _c.getLocalLook();
	auto right = _c.getLocalRight();

	switch (look) {
	case AxisDir::DIR_ZP:
	{
		switch (right)
		{
		case AxisDir::DIR_YP:		// ZCW1
			swap(ciw.right, ciw.top);
			swap(ciw.top, ciw.left);
			swap(ciw.left, ciw.bottom);
			break;
		case AxisDir::DIR_XP:		// ZCW2
			swap(ciw.left, ciw.right);
			swap(ciw.top, ciw.bottom);
			break;
		case AxisDir::DIR_YM:		// ZCCW
			swap(ciw.right, ciw.bottom);
			swap(ciw.bottom, ciw.left);
			swap(ciw.left, ciw.top);
			break;
		}
	}
	break;
	case AxisDir::DIR_ZM:			// YCW2
	{
		swap(ciw.front, ciw.back);
		swap(ciw.left, ciw.right);
		switch (right)
		{
		case AxisDir::DIR_YP:		// ZCCW
			swap(ciw.right, ciw.bottom);
			swap(ciw.bottom, ciw.left);
			swap(ciw.left, ciw.top);
			break;
		case AxisDir::DIR_XM:		// ZCCW2
			swap(ciw.left, ciw.right);
			swap(ciw.top, ciw.bottom);
			break;
		case AxisDir::DIR_YM:		// ZCW
			swap(ciw.right, ciw.top);
			swap(ciw.top, ciw.left);
			swap(ciw.left, ciw.bottom);
			break;

		}
	}
	break;
	case AxisDir::DIR_XP:				// YCCW
		swap(ciw.left, ciw.front);
		swap(ciw.left, ciw.back);
		swap(ciw.right, ciw.back);
		switch (right)
		{
		case AxisDir::DIR_YM:			// XCCW
			swap(ciw.front, ciw.top);
			swap(ciw.top, ciw.back);
			swap(ciw.back, ciw.bottom);
			break;
		case AxisDir::DIR_ZM:			// XCCW2
			swap(ciw.front, ciw.back);
			swap(ciw.top, ciw.bottom);
			break;
		case AxisDir::DIR_YP:			// XCW
			swap(ciw.front, ciw.bottom);
			swap(ciw.back, ciw.bottom);
			swap(ciw.top, ciw.back);
			break;

		}
		break;
	case AxisDir::DIR_XM:				// YCW
		swap(ciw.right, ciw.front);
		swap(ciw.right, ciw.back);
		swap(ciw.left, ciw.back);
		switch (right)
		{
		case AxisDir::DIR_YM:			// XCCW
			swap(ciw.front, ciw.top);
			swap(ciw.top, ciw.back);
			swap(ciw.back, ciw.bottom);
			break;
		case AxisDir::DIR_ZP:			// XCCW2
			swap(ciw.front, ciw.back);
			swap(ciw.top, ciw.bottom);
			break;
		case AxisDir::DIR_YP:			// XCW
			swap(ciw.front, ciw.bottom);
			swap(ciw.back, ciw.bottom);
			swap(ciw.top, ciw.back);
			break;

		}
		break;
	case AxisDir::DIR_YP:				// XCW
		swap(ciw.front, ciw.bottom);
		swap(ciw.back, ciw.bottom);
		swap(ciw.top, ciw.back);
		switch (right)
		{
		case AxisDir::DIR_ZM:			// YCW
			swap(ciw.right, ciw.front);
			swap(ciw.right, ciw.back);
			swap(ciw.left, ciw.back);
			break;
		case AxisDir::DIR_XP:			// YCW2
			swap(ciw.left, ciw.right);
			swap(ciw.bottom, ciw.top);
			break;
		case AxisDir::DIR_ZP:			// YCCW
			swap(ciw.left, ciw.front);
			swap(ciw.left, ciw.back);
			swap(ciw.right, ciw.back);
			break;

		}
		break;
	case AxisDir::DIR_YM:				// XCCW
		swap(ciw.front, ciw.top);
		swap(ciw.top, ciw.back);
		swap(ciw.back, ciw.bottom);
		switch (right)
		{
		case AxisDir::DIR_ZM:			// YCW
			swap(ciw.right, ciw.front);
			swap(ciw.right, ciw.back);
			swap(ciw.left, ciw.back);
			break;
		case AxisDir::DIR_XP:			// YCW2
			swap(ciw.left, ciw.right);
			swap(ciw.bottom, ciw.top);
			break;
		case AxisDir::DIR_ZP:			// YCCW
			swap(ciw.left, ciw.front);
			swap(ciw.left, ciw.back);
			swap(ciw.right, ciw.back);
			break;

		}
		break;
	}
	switch (_face) {
	case 0:
		색랜(ciw.top);
		break;
	case 1:
		색랜(ciw.front);
		break;
	case 2:
		색랜(ciw.left);
		break;
	case 3:
		색랜(ciw.right);
		break;
	case 4:
		색랜(ciw.bottom);
		break;
	case 5:
		색랜(ciw.back);
		break;
	}
}
bool render()
{
	if (c.getState() != CubeBoneState::State_Replace)
		return 0;

	model_coor data = c.getData();

	cout << "                                  " << endl;
	cout << "          "; 통랜(data.v[18], 0); cout << "     "; 통랜(data.v[19], 0); cout << "     "; 통랜(data.v[20], 0); cout << "         " << endl;
	cout << "                          "; 통랜(data.v[20], 3); cout << "      " << endl;
	cout << "      "; 통랜(data.v[21], 0); cout << "     "; 통랜(data.v[22], 0); cout << "     "; 통랜(data.v[23], 0); cout << "            " << endl;
	cout << "                      "; 통랜(data.v[23], 3); cout << "  "; 통랜(data.v[11], 3); cout << "      " << endl;
	cout << "   "; 통랜(data.v[24], 0); cout << "     "; 통랜(data.v[25], 0); cout << "     "; 통랜(data.v[26], 0); cout << "               " << endl;
	cout << "                  "; 통랜(data.v[26], 3); cout << "  "; 통랜(data.v[14], 3); cout << "  "; 통랜(data.v[2], 3); cout << "      " << endl;
	cout << ""; 통랜(data.v[24], 1); cout << "     "; 통랜(data.v[25], 1); cout << "     "; 통랜(data.v[26], 1); cout << "                  " << endl;
	cout << "                  "; 통랜(data.v[17], 3); cout << "  "; 통랜(data.v[5], 3); cout << "          " << endl;
	cout << ""; 통랜(data.v[15], 1); cout << "     "; 통랜(data.v[16], 1); cout << "     "; 통랜(data.v[17], 1); cout << "                  " << endl;
	cout << "                  "; 통랜(data.v[8], 3); cout << "              " << endl;
	cout << ""; 통랜(data.v[6], 1); cout << "     "; 통랜(data.v[7], 1); cout << "     "; 통랜(data.v[8], 1); cout << "                  " << endl << endl;


	/*
	cout << "                                  " << endl;
	cout << "         22     22     22         " << endl;
	cout << "                          33      " << endl;
	cout << "      22     22     22            " << endl;
	cout << "                      33  33      " << endl;
	cout << "   22     22     22               " << endl;
	cout << "                  33  33  33      " << endl;
	cout << "11     11     11                  " << endl;
	cout << "                  33  33          " << endl;
	cout << "11     11     11                  " << endl;
	cout << "                  33              " << endl;
	cout << "11     11     11                  " << endl << endl;
		*//*
	cout << endl;
	cout << "         "; 스랜(data.v[18].getID()); cout << "     "; 스랜(data.v[19].getID()); cout << "     "; 스랜(data.v[20].getID()); cout << "         " << endl;
	cout << "                          "; 스랜(data.v[20].getID()); cout <<  "        " << endl;
	cout << "      "; 스랜(data.v[21].getID()); cout <<  "     "; 스랜(data.v[22].getID()); cout <<  "     " ; 스랜(data.v[23].getID()); cout <<  "            " << endl;
	cout << "                      "; 스랜(data.v[22].getID()); cout <<  "  "; 스랜(data.v[11].getID()); cout <<  "        " << endl;
	cout << "   "; 스랜(data.v[24].getID()); cout <<  "     "; 스랜(data.v[25].getID()); cout <<  "     " ; 스랜(data.v[26].getID()); cout <<  "               " << endl;
	cout << "                  "; 스랜(data.v[26].getID()); cout<<"  "; 스랜(data.v[14].getID()); cout <<  "  " ; 스랜(data.v[2].getID()); cout <<  "        " << endl;
	cout << ""; 스랜(data.v[24].getID()); cout <<  "     "; 스랜(data.v[25].getID()); cout <<  "     " ; 스랜(data.v[26].getID()); cout <<  "                  " << endl;
	cout << "                  "; 스랜(data.v[17].getID()); cout <<  "  "; 스랜(data.v[5].getID()); cout <<  "           " << endl;
	cout << ""; 스랜(data.v[15].getID()); cout <<  "     "; 스랜(data.v[16].getID()); cout <<  "     "; 스랜(data.v[17].getID()); cout <<  "                  " << endl;
	cout << "                  "; 스랜(data.v[8].getID()); cout <<  "              " << endl;
	cout << ""; 스랜(data.v[6].getID()); cout <<  "     " ; 스랜(data.v[7].getID()); cout <<  "     "; 스랜(data.v[8].getID()); cout <<  "                  " << endl;
	cout << endl;


	cout << "1  3  5  7  9  1  3  5  7  9  1" << endl;
	cout << "                               " << endl;
	cout << "                               " << endl;
	cout << "                               " << endl;
	cout << "                               " << endl;
	cout << "         22     22     22         " << endl;
	cout << "                          33        " << endl;
	cout << "      22     22     22            " << endl;
	cout << "                      33  33        " << endl;
	cout << "   22     22     22               " << endl;
	cout << "                  33  33  33        " << endl;
	cout << "11     11     11                  " << endl;
	cout << "                  33  33           " << endl;
	cout << "11     11     11                  " << endl;
	cout << "                  33              " << endl;
	cout << "11     11     11                  " << endl;
	*/

	cout << "10의자리 : 1, 2, 3, 4, 5, 6 = XCW, XCCW, YCW, YCCW, ZCW, ZCCW" << endl
		<< "1의자리 : 0, 1, 2 : 밑에서부터 1열, 2열, 3열" << endl <<
		"11 = XCW 2열,   40 = YCCW 1열,   62 = ZCCW3 열  cin >> ";
	return 0;
}

void loadmodel() {
 	 model[0] = cubemodel(0,  0, 0, 4, 0, 6, 5);
	 model[1] = cubemodel(1,  0, 0, 0, 0, 6, 5);
	 model[2] = cubemodel(2,  0, 0, 0, 3, 6, 5);
	 model[3] = cubemodel(3,  0, 0, 4, 0, 6, 0);
	 model[4] = cubemodel(4,  0, 0, 0, 0, 6, 0);
	 model[5] = cubemodel(5,  0, 0, 0, 3, 6, 0);
	 model[6] = cubemodel(6,  0, 1, 4, 0, 6, 0);
	 model[7] = cubemodel(7,  0, 1, 0, 0, 6, 0);
	 model[8] = cubemodel(8,  0, 1, 0, 3, 6, 0);

	 model[9] = cubemodel(9,  0, 0, 4, 0, 0, 5);
	model[10] = cubemodel(10, 0, 0, 0, 0, 0, 5);
	model[11] = cubemodel(11, 0, 0, 0, 3, 0, 5);
	model[12] = cubemodel(12, 0, 0, 4, 0, 0, 0);
	model[13] = cubemodel(13, 0, 0, 0, 0, 0, 0);
	model[14] = cubemodel(14, 0, 0, 0, 3, 0, 0);
	model[15] = cubemodel(15, 0, 1, 4, 0, 0, 0);
	model[16] = cubemodel(16, 0, 1, 0, 0, 0, 0);
	model[17] = cubemodel(17, 0, 1, 0, 3, 0, 0);

	model[18] = cubemodel(18, 2, 0, 4, 0, 0, 5);
	model[19] = cubemodel(19, 2, 0, 0, 0, 0, 5);
	model[20] = cubemodel(20, 2, 0, 0, 3, 0, 5);
	model[21] = cubemodel(21, 2, 0, 4, 0, 0, 0);
	model[22] = cubemodel(22, 2, 0, 0, 0, 0, 0);
	model[23] = cubemodel(23, 2, 0, 0, 3, 0, 0);
	model[24] = cubemodel(24, 2, 1, 4, 0, 0, 0);
	model[25] = cubemodel(25, 2, 1, 0, 0, 0, 0);
	model[26] = cubemodel(26, 2, 1, 0, 3, 0, 0);
}


void timer_thread() {
	int input;
	while (1) {
		cin >> input;
		c.keyInput(keystruct(input / 10, input % 10));
	}
}
int main()
{
	loadmodel();
	thread v(timer_thread);


	while (1) {
		c.update();
		render();
		Sleep(100);
	}


	v.join();
	return 0;
}

