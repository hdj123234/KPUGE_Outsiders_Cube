#pragma once


enum class RotateDir : unsigned char {
	DIR_NOTHING,
	DIR_XCW,
	DIR_XCCW,
	DIR_YCW,
	DIR_YCCW,
	DIR_ZCW,
	DIR_ZCCW
};


enum AxisDir : char {
	DIR_ERROR,
	DIR_XP,
	DIR_XM,
	DIR_YP,
	DIR_YM,
	DIR_ZP,
	DIR_ZM,
};

struct Axis {
	char x, y, z;
};

class CubeR
{
	int id;
	int pos[3];
	Axis localRight;
	Axis localLook;

	AxisDir findAxis(Axis _a);

public:
	CubeR(int _id, int _x, int _y, int _z);
	~CubeR();

	int getID() { return id; }
	int getX() { return pos[0]; }
	int getY() { return pos[1]; }
	int getZ() { return pos[2]; }
	void setRocate(int _x, int _y, int _z) { pos[0] = _x; pos[1] = _y; pos[2] = _z; }
	void rotateCube(int _cubeSize, RotateDir _dir);
	AxisDir getLocalRight() { return findAxis(localRight); }
	AxisDir getLocalLook() { return findAxis(localLook); }
};

