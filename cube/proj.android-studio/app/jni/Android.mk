LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos/audio/include)


# With Firebase libraries for the selected build configuration (ABI + STL)
STL:=$(firstword $(subst _, ,$(APP_STL)))


FIREBASE_CPP_SDK_DIR := ../../../firebase_cpp_sdk
FIREBASE_LIBRARY_PATH:= $(FIREBASE_CPP_SDK_DIR)/libs/android/$(TARGET_ARCH_ABI)/$(STL)

include $(CLEAR_VARS)
LOCAL_MODULE:=firebase_app
LOCAL_SRC_FILES:=$(FIREBASE_LIBRARY_PATH)/libapp.a
LOCAL_EXPORT_C_INCLUDES:=$(FIREBASE_CPP_SDK_DIR)/include
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE:=firebase_admob
LOCAL_SRC_FILES:=$(FIREBASE_LIBRARY_PATH)/libadmob.a
LOCAL_EXPORT_C_INCLUDES:=$(FIREBASE_CPP_SDK_DIR)/include
include $(PREBUILT_STATIC_LIBRARY)


LOCAL_MODULE := MyGame_shared

LOCAL_MODULE_FILENAME := libMyGame

LOCAL_LDLIBS:=-llog -landroid -latomic
LOCAL_ARM_MODE:=arm
LOCAL_LDFLAGS:=-Wl,-z,defs -Wl,--no-undefined
LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../../Classes/AppDelegate.cpp \
                   ../../../Classes/stdafx.cpp \
                   ../../../Classes/InGameData.cpp \
                   ../../../Classes/InGameScene.cpp \
                   ../../../Classes/UIManager.cpp \
                   ../../../Classes/NxNxNCube.cpp \
                   ../../../Classes/NxNxNCubeBlock.cpp \
                   ../../../Classes/NxNxNCubeBone.cpp \
                   ../../../Classes/Camera.cpp \
                   ../../../Classes/MyUtility_NEJ/MyINI.cpp \
                   ../../../Classes/MyUtility_NEJ/MyDebugUtility.cpp \
                   ../../../Classes/InputManager.cpp \
                   ../../../Classes/OutputManager.cpp \
                   ../../../Classes/SceneInitializer.cpp \
                   ../../../Classes/CC_Scene/TinyScene_CC.cpp \
                   ../../../Classes/CC_Scene/EntryPoint_CC.cpp \
                   ../../../Classes/CC_Scene/InGameScene_CC.cpp \
                   ../../../Classes/Picker.cpp \
                   ../../../Classes/Module_CC/ModelRawData_Cube.cpp \
                   ../../../Classes/Module_CC/RubiksCubeModel.cpp \
                   ../../../Classes/Module_CC/DefaultTextureSet.cpp \
                   ../../../Classes/SpinStateNxNxNCube.cpp \
                   ../../../Classes/Module_Manager/CCResourceManager.cpp \
                   ../../../Classes/Module_Manager/StartupManager.cpp \
                   ../../../Classes/Module_Manager/ResourceManager.cpp \
                   ../../../Classes/Module_Manager/CCSceneManager.cpp \
                   ../../../Classes/AssistantComponent.cpp \
                   ../../../Classes/Module_CC/CCComponent.cpp\
                   ../../../Classes/Module_CC/CCAudio.cpp\
                   ../../../Classes/FirebaseHelper.cpp\
                   ../../../Classes/CC_Scene/AdmobController.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../Classes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/$(FIREBASE_CPP_SDK_DIR)/include

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static
LOCAL_STATIC_LIBRARIES += firebase_app
LOCAL_STATIC_LIBRARIES += firebase_admob

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END


