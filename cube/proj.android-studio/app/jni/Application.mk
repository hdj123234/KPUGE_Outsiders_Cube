#NDK_TOOLCHAIN_VERSION := 4.9
APP_STL := gnustl_static

# Enable c++11 extentions in source code

APP_CPPFLAGS := -frtti -DCC_ENABLE_CHIPMUNK_INTEGRATION=1 -std=c++11 -fsigned-char
APP_LDFLAGS := -latomic

APP_ABI := armeabi
APP_SHORT_COMMANDS := true

APP_PLATFORM:=android-14
NDK_TOOLCHAIN_VERSION=clang

ifeq ($(NDK_DEBUG),1)
  APP_CPPFLAGS += -DCOCOS2D_DEBUG=1
  APP_OPTIM := debug
else
  APP_CPPFLAGS += -DNDEBUG
  APP_OPTIM := release
endif
