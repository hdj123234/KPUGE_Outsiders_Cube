#pragma once

#include "cocos2d.h"


class IMyFunctor {
public:
	virtual ~IMyFunctor() {}
	virtual void operator()(cocos2d::Ref*) = 0;
	virtual void operator()() = 0;
};
class MyFunctor {
	IMyFunctor &r;
public:
	MyFunctor(IMyFunctor &r) :r(r) {}

	virtual void operator()(cocos2d::Ref*p) { r.operator()(p); }
	virtual void operator()() { r.operator()(); }
};

class CCSceneMover :public IMyFunctor {
private:
	cocos2d::Scene *scene;
	bool bReplace;
public:
	CCSceneMover(cocos2d::Scene *scene, bool bReplace = false) :scene(scene), bReplace(bReplace) {}
	void operator()(cocos2d::Ref*) { operator()(); }
	void operator()() 
	{
		if (bReplace)
		{
			if (!scene)
				cocos2d::Director::getInstance()->end();
			else
				cocos2d::Director::getInstance()->replaceScene(scene);

		}
		else
		{
			if (!scene)
				cocos2d::Director::getInstance()->popScene();
			else	
				cocos2d::Director::getInstance()->pushScene(scene);

		}

	}

};