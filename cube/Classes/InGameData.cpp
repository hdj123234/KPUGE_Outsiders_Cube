#include"stdafx.h"

#include "InGameData.h"
#include "Camera.h"
#include "AssistantComponent.h"
#include "ITwistyPuzzle.h"
#include "NxNxNCube.h"
#include "InputEvent.h"
#include "Outputmsg.h"
#include "cocos2d.h"
#include "SceneInitializer.h"
#include "Types\Type_Component.h"
#include "EnumDefine.h"
#include "Types\Type_Audio.h"


InGameData::InGameData()
	:twistyPuzzle(nullptr), camera(nullptr), timeLabel(nullptr)
{
	std::random_device rd;
	std::uniform_int_distribution<int> dist(0, 2);
	switch (dist(rd)) {
	case 0:
		outputMsgCollector.addMsg(new OutputMsg_Audio(Type_Audio::BGM_Main));
		break;
	case 1:
		outputMsgCollector.addMsg(new OutputMsg_Audio(Type_Audio::BGM2));
		break;
	case 2:
		outputMsgCollector.addMsg(new OutputMsg_Audio(Type_Audio::BGM3));
		break;
	}
}

InGameData::~InGameData()
{
	if (twistyPuzzle != nullptr) {
		delete twistyPuzzle;
		twistyPuzzle = nullptr;
	}

	if (camera != nullptr) {
		delete camera;
		camera = nullptr;
	}

	if (timeLabel != nullptr) {
		delete timeLabel;
		timeLabel = nullptr;
	}

	for (auto i : assistantComponents) {
		delete i;
	}
	assistantComponents.clear();
}

void InGameData::handleInputData(std::vector<IInputEvent*> v)
{
	for (auto i : v)
	{
		if (gameState == GameState::Pause) {
			if (i->getType() == Type_InputEvent::ExitPause)
				continue;
			continue;
		}
		if (gameState == GameState::Shuffling) {
			twistyPuzzle->skipShuffling(10);
			for (auto i : v) {
				if (i != nullptr)
					delete i;
			}
			return;
		}
		switch (i->getType())
		{
		case Type_InputEvent::Touch:
			break;
		case Type_InputEvent::Spin:
		{
			switch (gameMode) {
			case Type_GameMode::Auto:
				if (gameState != GameState::Play) break;
				twistyPuzzle->recvInputEvent(i);
				break;
			case Type_GameMode::Manual:
				if (gameState == GameState::ManualShuffle || gameState == GameState::Play)
					twistyPuzzle->recvInputEvent(i);
				break;
			case Type_GameMode::Free:
				twistyPuzzle->recvInputEvent(i);
				break;
			}
			break;
		}
		case Type_InputEvent::RotationCamera:
		{
			auto data = static_cast<InputEvent_RotationCamera*>(i);
			camera->rotateXY(data->getX(), data->getY(), data->getAngle());
			outputMsgCollector.addMsg(new OutputMsg_CameraState(camera->getMatrix()));
			break;
		}
		case Type_InputEvent::RotationCamera_AxisZ:
		{
			auto data = static_cast<InputEvent_RotationCamera_AxisZ*>(i);
			camera->rotateZ(data->getAngle());
			outputMsgCollector.addMsg(new OutputMsg_CameraState(camera->getMatrix()));
			break;
		}
		case Type_InputEvent::ZoomCamera:
		{
			auto data = static_cast<InputEvent_ZoomCamera*>(i);
			if (camera->changeDistance(data->getRate()))
				outputMsgCollector.addMsg(new OutputMsg_CameraState(camera->getMatrix()));
			break;
		}
		case Type_InputEvent::StopRotationCamera:
		{
			auto data = static_cast<InputEvent_TypeOnly*>(i);
			break;
		}
		case Type_InputEvent::SelectOption:
		{
			/* 내가 처리 안함
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::OptionLable, true, true));
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::OptionButton, false, true));
			preState = gameState;
			gameState = GameState::Pause;
			timeLabel->stop();
			*/
			break;
		}
		case Type_InputEvent::ExitOption:
		{
			/* 내가 처리 안함
			if (gameState != GameState::Pause) break;
			gameState = preState;
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::OptionButton, true, true));
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::OptionLable, false, false));
			if (gameState != GameState::Play) break;
			timeLabel->continueTimer();
			*/
			break;
		}
		case Type_InputEvent::Start:
		{
			if (gameState != GameState::ManualShuffle) break;
			auto data = static_cast<InputEvent_TypeOnly*>(i);
			gameState = GameState::GameStart;
			break;
		}
		case Type_InputEvent::AutoSuffle:
		{
			if (gameState != GameState::ManualShuffle) break;
			auto data = static_cast<InputEvent_TypeOnly*>(i);
			gameState = GameState::Shuffling;
			twistyPuzzle->shuffle(6, 3);
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::StartButton, false, true));
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::AutoSuffleButton, false, true));
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::ZRotationButton1, false, true));
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::ZRotationButton2, false, true));

			break;
		}
		case Type_InputEvent::Exception:
		{
			/* 내가 처리 안함
			preState = gameState;
			gameState = GameState::Pause;
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::Pause, true, true));
			timeLabel->stop();
			*/
			break;
		}
		case Type_InputEvent::ExitPause:
		{
			/* 내가 처리 안함
			if (gameState != GameState::Pause) break;
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::Pause, false, false));
			gameState = preState;
			if (gameState != GameState::Play) break;
			timeLabel->continueTimer();
			*/
			break;
		}
		case Type_InputEvent::Retry:
		{
			//	if (gameState != GameState::Clear || gameState != GameState::Pause) break;

				/* 내가 초기화 안해도 될듯
				camera->initializeDefault();
				timeLabel->setZero();
				timeLabel->stop();
				outputMsgCollector.clear();
				twistyPuzzle->reset();
				gameState = GameState::InputShuffle;
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::TimerLable, false, true));
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::OptionButton, true, true));
				switch (gameMode)
				{
				case Type_GameMode::Auto:
					break;
				case Type_GameMode::Manual:
					outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::AutoSuffleButton, true, true));
					break;
				case Type_GameMode::Free:
					break;
				default:
					break;
				}
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::ClearLable, false, false));
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::OptionLable, false, false));
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::OptionButton, true, true));
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::Retry, false, false));
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::Exit, false, false));

				outputMsgCollector.addMsg(new OutputMsg_CameraState(camera->getMatrix()));
				*/
			break;
		}
		case Type_InputEvent::Back:
		{
			/* 내쪽에서 처리 안함
			switch (gameState)
			{
			case GameState::Clear:
				// 타이틀로 가야할라나
				break;

			case GameState::Play:
				preState = gameState;
				gameState = GameState::Pause;
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::Pause, true, true));
				timeLabel->stop();
				break;

			case GameState::Pause:
				if (gameState != GameState::Pause) break;
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::Pause, false, false));
				gameState = preState;
				if (gameState != GameState::Play) break;
				timeLabel->continueTimer();
				break;

			}
			if (gameState != GameState::Clear || gameState != GameState::Pause) break;
			outputMsgCollector.addMsg(new OutputMsg_TypeOnly(Type_OutputMsg::Exit));
			*/
			break;
		}
		case Type_InputEvent::Exit:
		{
			if (gameState != GameState::Clear || gameState != GameState::Pause) break;
			outputMsgCollector.addMsg(new OutputMsg_TypeOnly(Type_OutputMsg::Exit));
			break;
		}
		}
		delete i;
	}
	v.clear();
}

void InGameData::getOutputMsgs(std::vector<IOutputMsg*>& r)
{
	for (auto i : outputMsgCollector.getOutputMsg())
		r.push_back(i);
}

std::vector<IPickableObject*> InGameData::getObjects()
{
	return std::vector<IPickableObject*>{twistyPuzzle->getPickableObjects()};
}

ICamera * InGameData::getCamera()
{
	return camera;
}

Type_Puzzle InGameData::getPuzzleType()
{
	return twistyPuzzle->getPuzzleType();
}

void InGameData::visit(OutputMsgCollector * p)
{
	twistyPuzzle->getTarget()->visit(p);
	timeLabel->visit(p);
}

void InGameData::update(float dTime)
{
	switch (gameState)
	{
	case GameState::InputShuffle:
		// 셔플용 스핀스테이트를 입력하고 셔플링 상태로 넘어갑니다.
		if (gameMode == Type_GameMode::Auto)
			twistyPuzzle->shuffle(twistyPuzzle->getCubeSize() * 7, 0);
		gameState = GameState::Shuffling;
		outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::ZRotationButton1, false, true));
		outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::ZRotationButton2, false, true));

		break;

		// 셔플링이 끝났는지 확인하고 모드에 알맞는 상태로 변경합니다.
	case GameState::Shuffling:
	{
		if (!twistyPuzzle->isSpinning()) {
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::ZRotationButton1, true, true));
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::ZRotationButton2, true, true));
			switch (gameMode)
			{
			case Type_GameMode::Auto:
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::StartButton, true, true));
				break;
			case Type_GameMode::Manual:
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::StartButton, true, true));
				outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::AutoSuffleButton, true, true));
				break;
			case Type_GameMode::Free:
				break;
			}
			gameState = GameState::ManualShuffle;
		}
		twistyPuzzle->update(dTime);
		break;
	}

	// 수동모드에서 셔플을 수동으로 할 시 
	case GameState::ManualShuffle:
	{
		twistyPuzzle->update(dTime);
		break;
	}
	case GameState::GameStart:
		outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::StartButton, false, false));
		outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::AutoSuffleButton, false, false));
		outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::TimerLable, true, true));
		gameState = GameState::Play;
		timeLabel->start();
		twistyPuzzle->update(dTime);
		break;

	case GameState::Play:
		//~!@#$% 클리어 체크는 실시간으로 할 필요가 없으니 나중에 고치자
		if (twistyPuzzle->checkClear()) {
			gameState = GameState::Clear;
			outputMsgCollector.addMsg(new OutputMsg_TypeOnly(Type_OutputMsg::Clear));
			timeLabel->stop();
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::TimerLable, false, false));
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::OptionButton, false, false));
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::ClearLable, true, true));
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::Retry, true, true));
			outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::Exit, true, true));
			outputMsgCollector.addMsg(new OutputMsg_Audio(Type_Audio::SE_Clear));
		}
		twistyPuzzle->update(dTime);
		break;
	case GameState::Pause:
		break;
	case GameState::Clear:
		break;
	}

	timeLabel->update(dTime);
	outputMsgCollector.run(this);
}
