#pragma once
#include "BlockState.h"
#include "IOutputMsg.h"
#include "Utility.h"
#include "Types\Type_Audio.h"

enum class Type_OutputMsg : unsigned char;
struct BlockState;
enum class Type_Component : unsigned char;
enum class Type_Audio : unsigned char;

class OutputMsg_CubeState : public IOutputMsg
{
	std::vector<BlockState> blockStates;
	std::vector<CubeSpinState> spinState;
public:
	virtual void pure_virtual() {};
	OutputMsg_CubeState(std::vector<BlockState> _blockStates, std::vector<CubeSpinState> _spinState)
		: blockStates(_blockStates), spinState(_spinState) {};
	virtual ~OutputMsg_CubeState() {};
	virtual Type_OutputMsg getType() { return Type_OutputMsg::CubeState; }
	std::vector<BlockState>& getBlockState() { return blockStates; }
	std::vector<CubeSpinState>& getSpinState() { return spinState; }
};

class OutputMsg_CameraState : public IOutputMsg
{
	Matrix transform;
public:
	virtual void pure_virtual() {};
	OutputMsg_CameraState(Matrix _transform) : transform(_transform) {};
	virtual ~OutputMsg_CameraState() {};
	virtual Type_OutputMsg getType() { return Type_OutputMsg::CameraState; }
	Matrix getMatrix() { return transform; }
};

class OutputMsg_AssistantComponent : public IOutputMsg
{
	Type_Component component;
	bool bActive;
	bool bVisible;
public:
	virtual void pure_virtual() {};
	OutputMsg_AssistantComponent(Type_Component _component, bool _bActive, bool _bVisible)
		: component(_component), bActive(_bActive), bVisible(_bVisible) {};
	virtual ~OutputMsg_AssistantComponent() {};
	virtual Type_OutputMsg getType() { return Type_OutputMsg::AssistantComponent; };
	Type_Component getComponentType() { return component; }
	bool isOn() { return bActive; }
	bool isVisible() { return bVisible; }
};

class OutputMsg_SetTimer : public IOutputMsg
{
	float time;
public:
	virtual void pure_virtual() {};
	OutputMsg_SetTimer(float _time) : time(_time) {};
	virtual ~OutputMsg_SetTimer() {};
	virtual Type_OutputMsg getType() { return Type_OutputMsg::SetTimer; };
	float getTime() { return time; }
};

class OutputMsg_TypeOnly :public IOutputMsg
{
	Type_OutputMsg type;
public:
	virtual void pure_virtual() {};
	OutputMsg_TypeOnly(Type_OutputMsg _type) :type(_type) {};
	virtual ~OutputMsg_TypeOnly() {};
	virtual Type_OutputMsg getType() { return type; }
};

class OutputMsg_Back :public IOutputMsg
{
public:
	virtual void pure_virtual() {};
	OutputMsg_Back() {};
	virtual ~OutputMsg_Back() {};
	virtual Type_OutputMsg getType() { return Type_OutputMsg::Back; }
};

class OutputMsg_Audio :public IOutputMsg
{
	Type_Audio type;
public:
	virtual void pure_virtual() {};
	OutputMsg_Audio(Type_Audio _type) :type(_type) {};
	virtual ~OutputMsg_Audio() {};
	virtual Type_OutputMsg getType() { return Type_OutputMsg::RunAudio; }
	virtual Type_Audio getAudioType() {
		if (type != Type_Audio::SE_Spin) return type;
		std::random_device rd;
		std::uniform_int_distribution<int> dist(0, 7);
		switch (dist(rd))
		{
		case 0: return Type_Audio::SE_Spin;
		case 1:return Type_Audio::SE_Spin1;
		case 2:return Type_Audio::SE_Spin2;
		case 3:return Type_Audio::SE_Spin3;
		case 4:return Type_Audio::SE_Spin4;
		case 5:return Type_Audio::SE_Spin5;
		case 6:return Type_Audio::SE_Spin6;
		case 7:return Type_Audio::SE_Spin7;
		}
	}
};