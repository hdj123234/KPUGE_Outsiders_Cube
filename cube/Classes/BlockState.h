#pragma once
/* XP 0  XM 1  YP 2  YM 3  ZP 4  ZM 5 */
typedef unsigned char localAxisID;


struct BlockState {
	unsigned char blockID;
	unsigned char posID;
	localAxisID localRight;
	localAxisID localLook;
};



/*
	CubeSpinState의 axisID는 1부터 시작합니다.
	공용체의 bSpine == false 와 혼동을 막기 위함
	X : 1  ,   Y : 2   ,   Z : 3
*/
struct _CubeSpinState {
	unsigned char axisID;
	unsigned char lineID;
	bool bCW;
	float angle;
	_CubeSpinState() {};
	_CubeSpinState(unsigned char _axisID, unsigned char _lineID, bool _bCW, float _angle)
		: axisID(_axisID), lineID(_lineID), bCW(_bCW), angle(_angle) {}
};

struct CubeSpinState {
	bool bSpin;
	_CubeSpinState state;
	CubeSpinState() : bSpin(false) {};
	CubeSpinState(unsigned char _axisID, unsigned char _lineID, bool _bCW, float _angle)
		:bSpin(true), state(_axisID, _lineID, _bCW, _angle) {};
};