#pragma once

class IInputEvent;
class IPickableObject;
struct BlockState;
struct CubeSpinState;
class IOutputMsg;
class IOutputMsgTarget;
enum class Type_Puzzle : unsigned char;


class ITwistyPuzzle
{
public:
	virtual void pure_virtual() {};
	virtual ~ITwistyPuzzle() {};
	virtual void initialize() = 0;
	virtual void reset() = 0;
	virtual void recvInputEvent(IInputEvent *inputEvent) = 0;
	virtual IPickableObject* getPickableObjects() = 0;
	virtual bool checkClear() = 0;
	virtual Type_Puzzle getPuzzleType() = 0;
	virtual void getBlockState(std::vector<BlockState> &r) = 0;
	virtual void getSpinState(std::vector<CubeSpinState> &r) = 0;
	virtual void update(float dTime) = 0;
	virtual void setRotate(float dTime) = 0;
	virtual IOutputMsgTarget* getTarget() = 0;
	virtual void shuffle(unsigned int _num, float _speed) = 0;
	virtual bool isSpinning() = 0;
	virtual void skipShuffling(float _speed) = 0;
	virtual unsigned int getCubeSize() = 0;
};