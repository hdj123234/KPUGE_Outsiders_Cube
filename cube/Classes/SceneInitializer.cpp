#include "stdafx.h"
#include "SceneInitializer.h"

#include "EnumDefine.h"
#include "InGameScene.h"
#include "Camera.h"
#include "AssistantComponent.h"
#include "InGameData.h"
#include "ITwistyPuzzle.h"

#include "UIManager.h"
#include "NxNxNCube.h"
#include "OutputMsg.h"

#include"InputManager.h"
#include"Picker.h"
#include "Types\Type_Component.h"
#include"InitializeState.h"

void SceneInitializer::initializeScene(InGameScene * _scene)
{
	if (_scene == nullptr)		assert(false);
	if (_scene->inGameData != nullptr) delete _scene->inGameData;
	_scene->inGameData = createInGameData();

	if (_scene->uiManager != nullptr) delete _scene->uiManager;
	auto uiManager = new UIManager();

	switch (InitializeState::getInstance()->getPuzzleType())
	{
	case Type_Puzzle::Square2x2x2Cube:
		uiManager->inputManager->setCubeSize(2);
		break;
	case Type_Puzzle::RubiksCube:
		uiManager->inputManager->setCubeSize(3);
		break;
	case Type_Puzzle::Square4x4x4Cube:
		uiManager->inputManager->setCubeSize(4);
		break;
	case Type_Puzzle::Square5x5x5Cube:
		uiManager->inputManager->setCubeSize(5);
		break;
	default:
		uiManager->inputManager->setCubeSize(3);
		break;
	}
	uiManager->inputManager->picker.setSpace(_scene->inGameData);
	_scene->uiManager = uiManager;
}

void SceneInitializer::initializeScene(IScene * _scene)
{
	SceneInitializer::initializeScene(dynamic_cast<InGameScene*>(_scene));
}

InGameData * SceneInitializer::createInGameData()
{
	auto data = new InGameData();

	auto &initState = (*InitializeState::getInstance());

	switch (initState.getPuzzleType())
	{
	case Type_Puzzle::Square2x2x2Cube:
		data->twistyPuzzle = new NxNxNCube(2, initState.getPickableObjectPtr());
		break;
	case Type_Puzzle::RubiksCube:
		data->twistyPuzzle = new NxNxNCube(3, initState.getPickableObjectPtr());
		break;
	case Type_Puzzle::Square4x4x4Cube:
		data->twistyPuzzle = new NxNxNCube(4, initState.getPickableObjectPtr());
		break;
	case Type_Puzzle::Square5x5x5Cube:
		data->twistyPuzzle = new NxNxNCube(5, initState.getPickableObjectPtr());
		break;
	default:
		data->twistyPuzzle = new NxNxNCube(3, initState.getPickableObjectPtr());
		break;
	}


	data->outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::TimerLable, false, true));
	data->outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::OptionButton, true, true));
	data->outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::ZRotationButton1, false, false));
	data->outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::ZRotationButton2, false, false));
	//data->outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::CheckScore, false, false));

	//initState.gameMode = Type_GameMode::Manual;
	//initState.setState( Type_GameMode::Manual, initState.getPuzzleType());

	switch (initState.getGameMode())
	{
	case Type_GameMode::Auto:
		break;
	case Type_GameMode::Manual:
		// data->outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::ManualSuffleButton, true, true));
		data->outputMsgCollector.addMsg(new OutputMsg_AssistantComponent(Type_Component::AutoSuffleButton, true, true));
		break;
	case Type_GameMode::Free:
		break;
	default:
		break;
	}

	data->gameMode = initState.getGameMode();

	// 초기화 할 것들 ~!@#$%
	data->camera = new Camera(static_cast<unsigned char>(initState.getPuzzleType()));
	data->timeLabel = new TimerLabel();
	data->assistantComponents;	// 필요한거 추가 ~!@#$%
	data->gameState = GameState::InputShuffle;

	data->twistyPuzzle->initialize();




	// 초기화가 끝났다면 코코스에게 카메라 초기화 데이터를 한 번 보내줍니다.
	data->outputMsgCollector.addMsg(new OutputMsg_CameraState(data->camera->getMatrix()));

	return data;
}

