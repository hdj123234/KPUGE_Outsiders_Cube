#pragma once
#include "IInputEvent.h"

typedef unsigned char AxisID;
enum class Type_Exception : unsigned char;

class InputEvent_Spin : public IInputEvent
{
	AxisID axisID;
	unsigned char lineID;
	bool bCW;
public:
	virtual void pure_virtual() {};
	InputEvent_Spin(AxisID _axisID, unsigned char _lineID, bool _bCW)
		: axisID(_axisID), lineID(_lineID), bCW(_bCW) {};
	virtual ~InputEvent_Spin() {};
	virtual Type_InputEvent getType() { return Type_InputEvent::Spin; }
	AxisID getAxisID() { return axisID; }
	unsigned char getLineID() { return lineID; }
	bool isCW() { return bCW; }

};

class InputEvent_RotationCamera : public IInputEvent
{
	float dx, dy;
	float angle;

public:
	virtual void pure_virtual() {};
	InputEvent_RotationCamera(float _dx,float _dy, float _angle) : dx(_dx), dy(_dy), angle(_angle) {};
	virtual ~InputEvent_RotationCamera() {};
	virtual Type_InputEvent getType() { return Type_InputEvent::RotationCamera; }
	float getX() { return dx; }
	float getY() { return dy; }
	float getAngle() { return angle; }
};
/*
class InputEvent_RotationCamera : public IInputEvent
{
	Vec3 axis;
	float angle;

public:
	InputEvent_RotationCamera(Vec3 _axis, float _angle) : axis(_axis), angle(_angle) {};
	virtual ~InputEvent_RotationCamera() {};
	virtual Type_InputEvent getType() { return Type_InputEvent::RotationCamera; }
	Vec3 getAxis() { return axis; }
	float getAngle() { return angle; }
};
*/

class InputEvent_RotationCamera_AxisZ :public IInputEvent
{
	float angle;
public:
	virtual void pure_virtual() {};
	InputEvent_RotationCamera_AxisZ(float _angle) : angle(_angle) {};
	virtual ~InputEvent_RotationCamera_AxisZ() {};
	virtual Type_InputEvent getType() { return Type_InputEvent::RotationCamera_AxisZ; }
	float getAngle() { return angle; }

};


class InputEvent_ZoomCamera :public IInputEvent
{
	float rate;
public:
	virtual void pure_virtual() {};
	InputEvent_ZoomCamera(float _rate) : rate(_rate) {};
	virtual ~InputEvent_ZoomCamera() {};
	virtual Type_InputEvent getType() { return Type_InputEvent::ZoomCamera; }
	float getRate() { return rate; }
};


class InputEvent_TypeOnly :public IInputEvent
{
	Type_InputEvent type;
public:
	virtual void pure_virtual() {};
	InputEvent_TypeOnly(Type_InputEvent _type) :type(_type) {};
	virtual ~InputEvent_TypeOnly() {};
	virtual Type_InputEvent getType() { return type; }
};

class InputEvent_Exception : public IInputEvent
{
	Type_Exception exceptionType;
public:
	virtual void pure_virtual() {};
	InputEvent_Exception(Type_Exception _exceptionType) : exceptionType(_exceptionType) {};
	virtual ~InputEvent_Exception() {};
	virtual Type_InputEvent getType() { return Type_InputEvent::Exception; }
};

class InputEvent_Back : public IInputEvent
{
public:
	virtual void pure_virtual() {};
	InputEvent_Back() {};
	virtual ~InputEvent_Back() {};
	virtual Type_InputEvent getType() { return Type_InputEvent::Back; }
};