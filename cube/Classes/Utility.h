#pragma once

#include"math\Mat4.h"
typedef float Mat[16];
class Matrix : public cocos2d::Mat4
{
public:
	Matrix() :cocos2d::Mat4{ 1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1 }{}

	Matrix(Mat4 m)		:Mat4(m) {}
	void convertFloat16(Mat &m) {
		memcpy(&m, this->m, sizeof(Mat));
	}
};

typedef unsigned char CCTag;