#pragma once
#include"stdafx.h"

#include"Cube_Adapter.h"
 

#include INCLUDE_PATH_CUBE_R_CPP 
#include INCLUDE_PATH_CUBE_Bone_CPP 



const Msg_CubeRotation Msg_CubeRotation::cubeRotation_RCW  = { RotateDir::DIR_XCW	, 2 };
const Msg_CubeRotation Msg_CubeRotation::cubeRotation_RCCW = { RotateDir::DIR_XCCW	, 2 };
const Msg_CubeRotation Msg_CubeRotation::cubeRotation_LCW  = { RotateDir::DIR_XCW	, 0 };
const Msg_CubeRotation Msg_CubeRotation::cubeRotation_LCCW = { RotateDir::DIR_XCCW	, 0 };
const Msg_CubeRotation Msg_CubeRotation::cubeRotation_UCW  = { RotateDir::DIR_YCW	, 2 };
const Msg_CubeRotation Msg_CubeRotation::cubeRotation_UCCW = { RotateDir::DIR_YCCW	, 2 };
const Msg_CubeRotation Msg_CubeRotation::cubeRotation_DCW  = { RotateDir::DIR_YCW	, 0 };
const Msg_CubeRotation Msg_CubeRotation::cubeRotation_DCCW = { RotateDir::DIR_YCCW	, 0 };
const Msg_CubeRotation Msg_CubeRotation::cubeRotation_FCW  = { RotateDir::DIR_ZCW	, 2 };
const Msg_CubeRotation Msg_CubeRotation::cubeRotation_FCCW = { RotateDir::DIR_ZCCW	, 2 };
const Msg_CubeRotation Msg_CubeRotation::cubeRotation_BCW  = { RotateDir::DIR_ZCW	, 0 };
const Msg_CubeRotation Msg_CubeRotation::cubeRotation_BCCW = { RotateDir::DIR_ZCCW	, 0 };