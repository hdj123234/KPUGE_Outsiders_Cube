#include"stdafx.h"

#include"RubiksCubeModel.h"
#include"interface\ICCTextureSet.h"
#include"interface\IModelRawData.h"
#include"interface\IModelRawData_Picking.h"
#include"CCOutputMsg_CubeState.h"
 

#include"cocos2d.h"

RubiksCubeModel::RubiksCubeModel(const IModelRawData * renderingData, const IModelRawData_Picking * pickingData)
	:pickingData(pickingData),
	mat_idently(*(new cocos2d::Mat4()))
{
	auto &buildData= renderingData->getBuildData();

	//노드 생성
	sprite = cocos2d::Sprite3D::create();

	//메시 생성
//	// 상,전,좌,우,하,뒤
//	for (unsigned char i = 0; i < 6; ++i)
//	{
//		meshs_Plane.push_back(cocos2d::Mesh::create(buildData.vertexPositions, buildData.normals, buildData.texcoords, buildData.indices.at(i)));
//		meshs_HiddenPlane.push_back(cocos2d::Mesh::create(buildData.vertexPositions, buildData.normals, buildData.texcoords, buildData.indices.at(i)));
//		
//	}
	//모델 생성
	nBlock = buildData.blockDatas.size();
	cubeSize = (std::pow(nBlock, 1.0 / 3)+0.1);

	models.reserve(nBlock);
	for (auto &r : buildData.blockDatas)
	{
		auto model = cocos2d::Sprite3D::create();
		for (int i = 0; i < 6; ++i)
		{
			//메시 생성
			auto mesh = cocos2d::Mesh::create(buildData.vertexPositions, buildData.normals, buildData.texcoords, buildData.indices.at(i));
			model->addMesh(mesh);
			if (r.facesColorIDs[i] == 0)
				hiddenMeshs.push_back(mesh);
			else
				meshMap[i].push_back(mesh);
		}
		model->setPosition3D(cocos2d::Vec3(r.position.x, r.position.y, r.position.z));
		models.push_back(model);
		sprite->addChild(model);
	}

	sprite->retain();
	spinMatrixs.reserve(28);
	reset();
}

RubiksCubeModel::~RubiksCubeModel()
{
	delete &mat_idently;

	sprite->autorelease();

	for(auto data : meshMap) 
		for(auto data2 : data.second)
			data2->autorelease();
	
	for (auto data : hiddenMeshs)	data->autorelease();
	for(auto data : models)data->autorelease();

	blockSpins.clear();
}
 

void RubiksCubeModel::show()
{
	sprite->setVisible(true);
}

void RubiksCubeModel::hide()
{
	sprite->setVisible(false);
}

void RubiksCubeModel::setCCParent(cocos2d::Node * parent)
{
	parent->addChild(sprite);
//	for (auto p : models)
//		parent->addChild(p);
}

void RubiksCubeModel::setState(ICCOutputMsg * pMsg)
{
	//spin 정보 초기화
	for (auto &r : blockSpins)
		r = &mat_idently;
	spinMatrixs.clear();

	auto cubeStateMsg = static_cast<CCOutputMsg_CubeState *>(pMsg);

	//spin 처리
	for (auto &r : cubeStateMsg->getSpinState())
	{
		if (!r.bSpin) continue;
		auto &state = r.state;

		std::vector<unsigned char> ids = getSpinBlockIDs(state.axisID, state.lineID);
		addSpinMatrix(state.axisID, state.bCW, state.angle);

		cocos2d::Mat4 * spin = &(spinMatrixs.back());
//		spinMatrixs.push_back(cocos2d::Mat4());

		for (auto data : ids)
			blockSpins[data] = spin;
	}


//	auto &blockState = pMsg->getBlockState();
	for (auto &r : cubeStateMsg->getBlockState())
		setBlock(	models[r.blockID], 
					getLocalAxisFromDir(r.localRight),
					getLocalAxisFromDir(r.localLook),
					r.posID);



}

void RubiksCubeModel::setTextureSet(ICCTextureSet *textureSet)
{
	const auto &r = textureSet->getTextures();
	for (auto p : hiddenMeshs)
		p->setTexture(r[0]);

	auto iter = r.begin();
	auto iEnd = r.end();
	auto iTarget = meshMap.begin();

	while (++iter != iEnd)	//1부터 시작
		for(auto p : (iTarget++)->second)
			p->setTexture(*iter);
	
	for (auto p : models)
		p->genMaterial();
}

void RubiksCubeModel::reset()
{
	//Spin 행렬 초기화
	blockSpins.clear();
	blockSpins.reserve(nBlock);
	for (int i = 0; i < nBlock; ++i)
		blockSpins.push_back(&mat_idently);


	spinMatrixs.clear();


	////블록 초기화
	//auto idently = cocos2d::Quaternion(cocos2d::Mat4());
	//for (int i=0;i<nBlock;++i)
	//{
	//	auto block = models[i];
	//	block->setRotationQuat(idently);
	//	auto tmp = pickingData->getPositionFromPosID(i);
	//	block->setPosition3D(cocos2d::Vec3(tmp.x, tmp.y, tmp.z));
	//}

}
 

const PickingData::PickingTargetData * RubiksCubeModel::getPickingTargetData()const
{
	return &pickingData->getInitialPickingTargetData();;
//	pickingTargetData = pickingData->getInitialPickingTargetData();
//
//	//회전에 대한 처리 추후 작성
//
//	return &pickingTargetData;
}


void RubiksCubeModel::setBlock(cocos2d::Sprite3D * block, cocos2d::Vec3 localRight, cocos2d::Vec3 localLook, unsigned char posID)
{
	using namespace cocos2d;
	block->setPosition3D(Vec3(0, 0, 0));

	auto &spinMatrix = *blockSpins[posID];

	Vec3 localUp;
	Vec3::cross(localLook, localRight, &localUp);
//	Mat4 result;
//	Mat4 lm(
//		-1, 0, 0, 0,
//		0, -1, 0, 0,
//		0, 0, 1, 0,
//		0, 0, 0, 1
//	);
	Mat4 matrix(
		localRight.x, localUp.x, localLook.x, 0,
		localRight.y, localUp.y, localLook.y, 0,
		localRight.z, localUp.z, localLook.z, 0,
		0, 0, 0, 1
	);
	matrix = spinMatrix*matrix; 
	block->setRotationQuat(cocos2d::Quaternion(matrix));


	//고정
	auto tmp = pickingData->getPositionFromPosID(posID);
	Vec3 pos = Vec3(tmp.x, tmp.y, tmp.z);
	pos = spinMatrix*pos;
	block->setPosition3D(Vec3(pos.x, pos.y, pos.z));
	
}

cocos2d::Vec3 RubiksCubeModel::getLocalAxisFromDir(localAxisID dir)
{
	/*
	XP 0
	XM 1
	YP 2
	YM 3
	ZP 4
	ZM 5
	*/
	switch (dir)
	{
	case 0:
		return cocos2d::Vec3(1, 0, 0);
	case 1:
		return cocos2d::Vec3(-1, 0, 0);
	case 2:
		return cocos2d::Vec3(0, 1, 0);
	case 3:
		return cocos2d::Vec3(0, -1, 0);
	case 4:
		return cocos2d::Vec3(0, 0, 1);
	case 5:
		return cocos2d::Vec3(0, 0, -1);
	}
	return cocos2d::Vec3();
}

std::vector<unsigned char> RubiksCubeModel::getSpinBlockIDs(unsigned char axisID, unsigned char lineID)
{
	std::vector<unsigned char> retval;
	int * target;
	int x,y,z, posID=0;

	switch (axisID)
	{
	case 1:	//X
		target = &x;
		break;
	case 2:	//Y
		target = &y;
		break;
	case 3:	//Z
		target = &z;
		break;
	}
	for (y = 0; y < cubeSize; ++y)
		for (z = 0; z < cubeSize; ++z)
			for (x = 0; x < cubeSize; ++x, ++posID)
				if (*target == lineID) retval.push_back(posID);


	return std::move(retval);
}

void RubiksCubeModel::addSpinMatrix(unsigned char axisID, bool bCW, float angle)
{
	/*
	CubeSpinState의 axisID는 1부터 시작합니다.
	공용체의 bSpine == false 와 혼동을 막기 위함
	X : 1  ,   Y : 2   ,   Z : 3
	*/
	float sign;

	cocos2d::Mat4 spin;
	switch (axisID)
	{
	case 1:
		if (bCW) sign = 1;
		else sign = -1;
		cocos2d::Mat4::createRotationX(sign*angle* (3.14159265358979323846 /180.0f) ,&spin);
		break;
	case 2:
		if (bCW) sign = 1;
		else sign = -1;
		cocos2d::Mat4::createRotationY(sign*angle* (3.14159265358979323846 / 180.0f), &spin);
		break;
	case 3:
		if (bCW) sign = 1;
		else sign = -1;
		cocos2d::Mat4::createRotationZ(sign*angle* (3.14159265358979323846 / 180.0f), &spin);
		break;
	}


	spinMatrixs.push_back(spin);
}
