#pragma once
 
#include"interface\ICCTextureSet.h"

#include<string>

namespace cocos2d {
	class Texture2D;
	class Image;

}


class RawCCTexture {
private:
	cocos2d::Texture2D *texture;
	cocos2d::Image * image;
	unsigned char cbit[16];
	std::string name;

public:
	RawCCTexture(const std::string &name, unsigned int colorCode);
	~RawCCTexture();

	cocos2d::Texture2D* getTexture() { return texture; }
};




class DefaultTextureSet : public ICCTextureSet {
private:
	std::vector<RawCCTexture *> rawCCTextures;
	std::vector<cocos2d::Texture2D*> textures;
	
private:
	DefaultTextureSet();
	virtual ~DefaultTextureSet();

public:

	virtual std::vector<cocos2d::Texture2D*> getTextures() { return textures; }
	std::vector<cocos2d::Texture2D*> getTextureSet_Rubiks();

	static DefaultTextureSet* getInstance() {
		static DefaultTextureSet instance;
		return &instance;
	}

};