
#include"stdafx.h"

#include"CubeModel.h"
#include"Cube_Adapter.h"
#include"cocos2d.h"
#include"BlockState.h"
#include<vector>
#include"ModelRawData_Cube.h"

//

static const float CubeBlockSizeValue = 0.5f;


// ------------------------------------------ CubeBlockBuilder ------------------------------------------ 

std::vector<CubeTexture*> * CubeBlockBuilder::pTextures = nullptr;

const std::vector<float> CubeBlockBuilder::vertexPositions ={
	+CubeBlockSizeValue,+CubeBlockSizeValue,+CubeBlockSizeValue,
	+CubeBlockSizeValue,+CubeBlockSizeValue,-CubeBlockSizeValue,
	+CubeBlockSizeValue,-CubeBlockSizeValue,+CubeBlockSizeValue,
	+CubeBlockSizeValue,-CubeBlockSizeValue,-CubeBlockSizeValue,
	-CubeBlockSizeValue,+CubeBlockSizeValue,+CubeBlockSizeValue,
	-CubeBlockSizeValue,+CubeBlockSizeValue,-CubeBlockSizeValue,
	-CubeBlockSizeValue,-CubeBlockSizeValue,+CubeBlockSizeValue,
	-CubeBlockSizeValue,-CubeBlockSizeValue,-CubeBlockSizeValue,
};
const std::vector<float> CubeBlockBuilder::texcoords = {
	0.5f,0.5f,
	0.5f,0.5f,
	0.5f,0.5f,
	0.5f,0.5f,
	0.5f,0.5f,
	0.5f,0.5f,
	0.5f,0.5f,
	0.5f,0.5f,
};
const std::vector<cocos2d::Vec3> CubeBlockBuilder::blockPositions = {
	cocos2d::Vec3(-1.0f, -1.0f, -1.0f),	cocos2d::Vec3(+0.0f, -1.0f, -1.0f),	cocos2d::Vec3(+1.0f, -1.0f, -1.0f),
	cocos2d::Vec3(-1.0f, -1.0f, +0.0f),	cocos2d::Vec3(+0.0f, -1.0f, +0.0f),	cocos2d::Vec3(+1.0f, -1.0f, +0.0f),
	cocos2d::Vec3(-1.0f, -1.0f, +1.0f),	cocos2d::Vec3(+0.0f, -1.0f, +1.0f),	cocos2d::Vec3(+1.0f, -1.0f, +1.0f),

	cocos2d::Vec3(-1.0f, +0.0f, -1.0f),	cocos2d::Vec3(+0.0f, +0.0f, -1.0f),	cocos2d::Vec3(+1.0f, +0.0f, -1.0f),
	cocos2d::Vec3(-1.0f, +0.0f, +0.0f),	cocos2d::Vec3(+0.0f, +0.0f, +0.0f),	cocos2d::Vec3(+1.0f, +0.0f, +0.0f),
	cocos2d::Vec3(-1.0f, +0.0f, +1.0f),	cocos2d::Vec3(+0.0f, +0.0f, +1.0f),	cocos2d::Vec3(+1.0f, +0.0f, +1.0f),

	cocos2d::Vec3(-1.0f, +1.0f, -1.0f),	cocos2d::Vec3(+0.0f, +1.0f, -1.0f),	cocos2d::Vec3(+1.0f, +1.0f, -1.0f),
	cocos2d::Vec3(-1.0f, +1.0f, +0.0f),	cocos2d::Vec3(+0.0f, +1.0f, +0.0f),	cocos2d::Vec3(+1.0f, +1.0f, +0.0f),
	cocos2d::Vec3(-1.0f, +1.0f, +1.0f),	cocos2d::Vec3(+0.0f, +1.0f, +1.0f),	cocos2d::Vec3(+1.0f, +1.0f, +1.0f),
};
const std::vector<std::vector<unsigned char>> CubeBlockBuilder::facesColorIDs = {
	std::vector<unsigned char>{0, 0, 4, 0, 6, 5},
	std::vector<unsigned char>{0, 0, 0, 0, 6, 5},
	std::vector<unsigned char>{0, 0, 0, 3, 6, 5},
	std::vector<unsigned char>{0, 0, 4, 0, 6, 0},
	std::vector<unsigned char>{0, 0, 0, 0, 6, 0},
	std::vector<unsigned char>{0, 0, 0, 3, 6, 0},
	std::vector<unsigned char>{0, 1, 4, 0, 6, 0},
	std::vector<unsigned char>{0, 1, 0, 0, 6, 0},
	std::vector<unsigned char>{0, 1, 0, 3, 6, 0},
	std::vector<unsigned char>{0, 0, 4, 0, 0, 5},
	std::vector<unsigned char>{0, 0, 0, 0, 0, 5},
	std::vector<unsigned char>{0, 0, 0, 3, 0, 5},
	std::vector<unsigned char>{0, 0, 4, 0, 0, 0},
	std::vector<unsigned char>{0, 0, 0, 0, 0, 0},
	std::vector<unsigned char>{0, 0, 0, 3, 0, 0},
	std::vector<unsigned char>{0, 1, 4, 0, 0, 0},
	std::vector<unsigned char>{0, 1, 0, 0, 0, 0},
	std::vector<unsigned char>{0, 1, 0, 3, 0, 0},
	std::vector<unsigned char>{2, 0, 4, 0, 0, 5},
	std::vector<unsigned char>{2, 0, 0, 0, 0, 5},
	std::vector<unsigned char>{2, 0, 0, 3, 0, 5},
	std::vector<unsigned char>{2, 0, 4, 0, 0, 0},
	std::vector<unsigned char>{2, 0, 0, 0, 0, 0},
	std::vector<unsigned char>{2, 0, 0, 3, 0, 0},
	std::vector<unsigned char>{2, 1, 4, 0, 0, 0},
	std::vector<unsigned char>{2, 1, 0, 0, 0, 0},
	std::vector<unsigned char>{2, 1, 0, 3, 0, 0}, 
};

const std::map<CubeBlockFace, std::vector<unsigned short>>CubeBlockBuilder::indices = {
	std::make_pair(CubeBlockFace::Front	, std::vector<unsigned short>{0,4,2,4,6,2,}),
	std::make_pair(CubeBlockFace::Back	, std::vector<unsigned short>{1,3,5,5,3,7,}),
	std::make_pair(CubeBlockFace::Up	, std::vector<unsigned short>{1,5,0,5,4,0,}),
	std::make_pair(CubeBlockFace::Down	, std::vector<unsigned short>{7,3,2,7,2,6,}),
	std::make_pair(CubeBlockFace::Left	, std::vector<unsigned short>{4,5,7,4,7,6,}),
	std::make_pair(CubeBlockFace::Right	, std::vector<unsigned short>{0,2,3,0,3,1,}),
};

// ------------------------------------------ CubeTexture ------------------------------------------ 

const std::map<CubeTexture::CubeTextureColor, std::string> CubeTexture::nameMap = {
	std::make_pair(CubeTexture::CubeTextureColor::None	,"None"),
	std::make_pair(CubeTexture::CubeTextureColor::White	,"White"),
	std::make_pair(CubeTexture::CubeTextureColor::Yellow,"Yellow"),
	std::make_pair(CubeTexture::CubeTextureColor::Red	,"Red"),
	std::make_pair(CubeTexture::CubeTextureColor::Orenge,"Orenge"),
	std::make_pair(CubeTexture::CubeTextureColor::Blue	,"Blue"),
	std::make_pair(CubeTexture::CubeTextureColor::Green	,"Green"),
};
const std::map<CubeTexture::CubeTextureColor, unsigned int> CubeTexture::colorBitMap = {
	std::make_pair(CubeTexture::CubeTextureColor::None	,0xffffffff),
	std::make_pair(CubeTexture::CubeTextureColor::Yellow,0xff00ffff),
	std::make_pair(CubeTexture::CubeTextureColor::Red	,0xff0000ff),
	std::make_pair(CubeTexture::CubeTextureColor::Blue	,0xffff0000),
	std::make_pair(CubeTexture::CubeTextureColor::Green	,0xff00ff00),
	std::make_pair(CubeTexture::CubeTextureColor::White	,0xffffffff),
	std::make_pair(CubeTexture::CubeTextureColor::Orenge,0xff00a5ff),
};



void CubeTexture::createTexture(CubeTexture::CubeTextureColor eColor)
{

	auto colorValue = colorBitMap.at(eColor);

	memcpy(cbit, &colorValue, 4);
	memcpy(cbit + 4, &colorValue, 4);
	memcpy(cbit + 8, &colorValue, 4);
	memcpy(cbit + 12, &colorValue, 4);

	pImage = new cocos2d::Image();
	pImage->initWithRawData(cbit, 16, 2, 2, 32);
	//pImage->initWithRawData(cbit, 4, 1, 1, 8);
	pTexture = cocos2d::CCTextureCache::sharedTextureCache()->addImage(pImage, nameMap.at(eColor));

}

CubeTexture::CubeTexture(CubeTextureColor eColor)
	:pTexture(nullptr)
{
	createTexture(eColor);
}


// ------------------------------------------ CubeBlockModel ------------------------------------------ 
void CubeBlockModel::addPlaneMesh(const std::vector<unsigned short> &indices, CubeTexture *pTexture)
{

	using namespace cocos2d;

	
	std::vector<float> normals; 
	
	auto mesh = Mesh::create(CubeBlockBuilder::getVertexPositions(), normals, CubeBlockBuilder::getTexCoords(), indices);
	
	mesh->setTexture(pTexture->getTexture());
//	pModel->setScale(10);
	
	pMeshs.push_back(mesh);

} 

void CubeBlockModel::createModel(unsigned char id)
{
	pModel = cocos2d::Sprite3D::create();

	CubeBlockFace faces[6] = { CubeBlockFace::Up,	CubeBlockFace::Front,		CubeBlockFace::Left,CubeBlockFace::Right,	CubeBlockFace::Down,	CubeBlockFace::Back, };


	auto & ColorIDs = CubeBlockBuilder::getColorIDs(id);

	for( auto face : faces)
		addPlaneMesh(CubeBlockBuilder::getIndices(face),CubeBlockBuilder::getTexture(ColorIDs[face]));

	for(auto p : pMeshs)		pModel->addMesh(p);

	pModel->genMaterial();
	pModel->setPosition3D(CubeBlockBuilder::getBlockPosition(id));
}
 
void CubeBlockModel::setEngine(cocos2d::Node * pNode)
{ 
	pNode->addChild(pModel);
}

void CubeBlockModel::setBlock(cocos2d::Vec3 & localRight, cocos2d::Vec3 & localLook, int posID)
{
	using namespace cocos2d;

	pModel->setPosition3D(Vec3(0, 0, 0));

	Vec3 localUp; 
	Vec3::cross(localLook, localRight, &localUp);
	Mat4 result;
	Mat4 lm(
		-1,0,0,0,
		0,-1,0,0,
		0,0,1,0,
		0, 0, 0, 1
	);
	Mat4 matrix(
		localRight.x, localUp.x, localLook.x, 0,
		localRight.y, localUp.y, localLook.y, 0,
		localRight.z, localUp.z, localLook.z, 0,
		0,0,0, 1
		);	
	//Mat4 matrix(
	//	localRight.x, localRight.y, localRight.z, 0,
	//	localUp.x, localUp.y, localUp.z, 0,
	//	localLook.x, localLook.y, localLook.z, 0,
	//	0, 0, 0, 1
	//	);lm
//	auto rlm = lm.getInversed();
//	Mat4::multiply(lm, matrix, &result);
////	Mat4::multiply(result, rlm, &result);
	
//	pModel->setRotationQuat(cocos2d::Quaternion(result));
	pModel->setRotationQuat(cocos2d::Quaternion(matrix));
	

	//����
	pModel->setPosition3D(CubeBlockBuilder::getBlockPosition(posID));
		
}
// ------------------------------------------ CubeModel ------------------------------------------ 

CubeModel::CubeModel(int nBlockCount)
{
	node = cocos2d::Sprite3D::create();
	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::None));
	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::Red));
	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::Yellow));
	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::Green));
	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::Blue));
	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::Orenge));
	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::White));
	CubeBlockBuilder::setTextures(&textures);
	for(int i=0;i<nBlockCount;++i)
		blocks.push_back(new CubeBlockModel(i));

}

CubeModel::~CubeModel()
{
	for (auto p : textures) delete p;
	for (auto p : blocks) delete p;
	blocks.clear();
	textures.clear();
}

void CubeModel::setModel(model_coor *cubeState)
{


	enum AxisDir : char {
		DIR_ERROR,
		DIR_XP,
		DIR_XM,
		DIR_YP,
		DIR_YM,
		DIR_ZP,
		DIR_ZM,
	};

	for (int i = 0; i < 27; ++i)
	{
		auto &r = cubeState->v[i];
		blocks[r.getID()]->setBlock(
			getLocalAxisFromDir(r.getLocalRight()),
			getLocalAxisFromDir(r.getLocalLook()),
			i );
	}
}

void CubeModel::setModel(std::vector<BlockState> &v)
{
	for (int i = 0; i < 27; ++i)
	{
		auto &r = v[i];
		blocks[r.blockID]->setBlock(
			getLocalAxisFromDir(r.localRight),
			getLocalAxisFromDir(r.localLook),
			r.posID);
	}
}

void CubeModel::setEngine(cocos2d::Node * pNode)
{

//	for (auto p : blocks)
//		p->setEngine(pNode);
	for (auto p : blocks)
		node->addChild( p->getModel());

	pNode->addChild(node);
}
//
//std::vector<cocos2d::Sprite3D*> CubeModel::getModel()
//{
//	std::vector<cocos2d::Sprite3D*> result;
//	for (auto p : blocks)
//		result.push_back(p->getModel());
//	return result;
//}

cocos2d::Vec3 CubeModel::getLocalAxisFromDir(AxisDir dir)
{
	switch (dir)
	{
	case AxisDir::DIR_XP:
		return cocos2d::Vec3(1,0,0);
	case AxisDir::DIR_XM:
		return cocos2d::Vec3(-1, 0, 0);
	case AxisDir::DIR_YP:
		return cocos2d::Vec3(0, 1, 0);
	case AxisDir::DIR_YM:
		return cocos2d::Vec3(0, -1, 0);
	case AxisDir::DIR_ZP:
		return cocos2d::Vec3(0, 0, 1);
	case AxisDir::DIR_ZM:
		return cocos2d::Vec3(0, 0, -1);
	}
}

cocos2d::Vec3 CubeModel::getLocalAxisFromDir(localAxisID dir)
{
	/*
	XP 0
	XM 1
	YP 2
	YM 3
	ZP 4
	ZM 5
	*/
	switch (dir)
	{
	case 0:
		return cocos2d::Vec3(1, 0, 0);
	case 1:
		return cocos2d::Vec3(-1, 0, 0);
	case 2:
		return cocos2d::Vec3(0, 1, 0);
	case 3:
		return cocos2d::Vec3(0, -1, 0);
	case 4:
		return cocos2d::Vec3(0, 0, 1);
	case 5:
		return cocos2d::Vec3(0, 0, -1);
	}
	return cocos2d::Vec3();
}
 

//
//
//// ------------------------------------------ TexTest ------------------------------------------ 
//
//std::vector<CubeTexture*> TexTest::textures;
//TexTest::TexTest(int n)
//{
//	using namespace cocos2d;
//	using namespace std;
//
//	vector<float> vertices = {
//		-1,-1,0,
//		-1,+1,0,
//		+1,+1,0,
//		+1,-1,0};
//	vector<float> normals;
//	vector<float> texcoords = { 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, };
//	vector<unsigned short> indices = { 0,3,1,3,1,2 };
//
//
//
//	pMesh = Mesh::create(vertices, normals, texcoords, indices);
//	pMesh->setTexture(textures[n]->getTexture());
//
//	pModel = Sprite3D::create();
//	pModel->addMesh(pMesh);
//
//	pModel->genMaterial();
//}
//
//void TexTest::setEngine(cocos2d::Node * pNode)
//{
//	pNode->addChild(pModel);
//}
//
//void TexTest::setTexture(int i)
//{
//	pModel->setVisible(false);
//
//	pMesh->setTexture(textures[i]->getTexture());
//	pModel->genMaterial();
//
//	pModel->setVisible(true);
//}
//
//void TexTest::setPos(cocos2d::Vec3 & v)
//{
//	pModel->setPosition3D(v);
//}
//
//void TexTest::setTex()
//{
//	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::None));
//	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::Red));
//	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::Yellow));
//	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::Green));
//	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::Blue));
//	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::Orenge));
//	textures.push_back(new CubeTexture(CubeTexture::CubeTextureColor::White));
//
//	CubeBlockBuilder::setTextures(&textures);
//}
