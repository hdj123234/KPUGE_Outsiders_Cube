#pragma once
#include"interface\ICCOutputMsg.h"
 
#include "BlockState.h"

#include<vector>
 
//�����ۼ�
class CCOutputMsg_CubeState : public ICCOutputMsg{ 
private: 
	std::vector<BlockState> block;
	std::vector<CubeSpinState> spin;
public:
	CCOutputMsg_CubeState()	{}
	CCOutputMsg_CubeState(std::vector<BlockState> &_block, std::vector<CubeSpinState> &_spin)
		: block(_block), spin(_spin) {};
	virtual ~CCOutputMsg_CubeState() {}

	std::vector<BlockState>& getBlockState() { return block; }
	std::vector<CubeSpinState>& getSpinState() { return spin; }
	virtual Type_CCOutputMsg getType() { return Type_CCOutputMsg::CubeState; }
	
};

