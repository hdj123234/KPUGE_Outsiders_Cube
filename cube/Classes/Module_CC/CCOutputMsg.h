#pragma once
#include"interface\ICCOutputMsg.h"

#include"..\Types\Type_Component.h"
#include<vector>

enum class Type_Audio : unsigned char;

class CCOutputMsg_CameraState : public ICCOutputMsg {
public:
	float transform[16];

public:
	//   CCOutputMsg_CameraState(float m[16])  {}
	virtual ~CCOutputMsg_CameraState() {}

	virtual Type_CCOutputMsg getType() { return Type_CCOutputMsg::CameraState; }

	//   Matrix* getWroldTransformMatrix() { return transform; }
};


class CCOutputMsg_Resource : public ICCOutputMsg {
private:
	Type_Component objectType;
	bool bVisible;
	bool bEnable;

	CCOutputMsg_Resource(Type_Component objectType, bool bVisible, bool bEnable) :  objectType(objectType), bVisible(bVisible), bEnable(bEnable){}
public:
	virtual ~CCOutputMsg_Resource() {}

	virtual Type_CCOutputMsg getType() { return Type_CCOutputMsg::AssistantComponent; }
	Type_Component getObjectType() { return objectType; }

	bool isVisible() const { return bVisible; }
	bool isEnable() const{ return bEnable; }

	static CCOutputMsg_Resource * createMsg(Type_Component objectType,bool bVisible, bool bEnable) { return new CCOutputMsg_Resource(objectType, bVisible,bEnable); }
	
};

class CCOutputMsg_SetTimer :public ICCOutputMsg
{
private:
	float time;

public:
	CCOutputMsg_SetTimer(float _time) :time(_time) {};
	virtual ~CCOutputMsg_SetTimer() {}
	virtual Type_CCOutputMsg getType() { return Type_CCOutputMsg::SetTimer; }
	float getTime() { return time; }
};

class CCOutputMsg_Clear :public ICCOutputMsg
{
private:

public:
	CCOutputMsg_Clear() {};
	virtual ~CCOutputMsg_Clear() {}
	virtual Type_CCOutputMsg getType() { return Type_CCOutputMsg::Clear; }
};

class CCOutputMsg_Back :public ICCOutputMsg
{
private:

public:
	CCOutputMsg_Back() {};
	virtual ~CCOutputMsg_Back() {}
	virtual Type_CCOutputMsg getType() { return Type_CCOutputMsg::Back; }
};

class CCOutputMsg_RunAudio :public ICCOutputMsg
{
private:
	Type_Audio audio;
public:
	CCOutputMsg_RunAudio(Type_Audio audio) :audio(audio){};
	virtual ~CCOutputMsg_RunAudio() {}
	virtual Type_CCOutputMsg getType() { return Type_CCOutputMsg::RunAudio; }

	Type_Audio getAudio() const { return audio; }
};
