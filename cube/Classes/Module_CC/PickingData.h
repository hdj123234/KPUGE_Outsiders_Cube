#pragma once
#include<vector>
#include"math\Vec3.h"

typedef unsigned char FaceDirectionID;


namespace PickingData {
	typedef cocos2d::Vec3 Vertex;
	typedef std::vector<Vertex> Polygon;
	typedef std::map<FaceDirectionID,Polygon> Block;	//�� ID, ������
	struct PickingTargetData { std::vector<Block> blocks; };
}