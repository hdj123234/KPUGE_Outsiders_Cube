#pragma once

#include"interface\IModelRawData_Picking.h"
#include"interface\IModelRawData.h"

#include"math\Vec3.h"

#include<vector>
#include<map>
#include<array> 

class CubeBlockModel;
struct BlockState;


enum CubeBlockFace : FaceDirectionID {
	Up, Front, Left, Right, Down, Back
};
/*
면 순서 :
	업
	프론트
	레프트
	라이트
	다운
	백
*/

class ModelRawData_Cube  :	public IModelRawData_Picking ,
							public IModelRawData {
private:
private:
	static float cubeBlockHalfSize;
	static float cubeBlockHalfOffset;

	std::map<CubeBlockFace, std::array<unsigned short, 4>> faceIndices;
	std::array<cocos2d::Vec3, 8> aabbVertices;

	unsigned int size;


	ModelBuildData buildData;
	PickingData::PickingTargetData pickingTargetData;
private:

public:
	ModelRawData_Cube(unsigned int size);
	~ModelRawData_Cube();

	//override IModelRawData
	virtual ITwistyPuzzleModel * createPuzzleModel()const;
	virtual const ModelBuildData & getBuildData()const {return buildData;	}
	//override IModelRawData_Picking
	virtual const PickingData::PickingTargetData& getInitialPickingTargetData() const { return pickingTargetData; }
	virtual PickingData::Vertex getPositionFromPosID(unsigned char id)const{ return buildData.blockDatas[id].position; }

	//override ICCResourceBuilder
	virtual ICCResource * buildResource();

	static ModelRawData_Cube * create(unsigned int nSize) { return new ModelRawData_Cube(nSize); }


private:
//	void initializeBlockPosition();
//	void initializeFaceColorIDs();
	void initializeBlockDatas();
	//특정 면을 구성하는 정점 4개의 인덱스를 반시계방향으로 받음
	void initializeIndices();
	void initializeIndices(const CubeBlockFace face, const std::array<unsigned short,4> &indices);
	void initializePickingTargetData();
};

