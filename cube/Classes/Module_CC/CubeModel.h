#pragma once

#include "cocos2d.h"
#include<vector>
#include<array>
#include<map>

struct model_coor;
class CubeTexture;
class CubeBlockModel;
struct BlockState;

namespace cocos2d {
	class Node;
	class Layer;
	class Texture2D;
	class Mesh;
	class Vec3;
}
enum AxisDir : char;

typedef unsigned char localAxisID;

/*
면 순서 :
	업
	프론트
	레프트
	라이트
	다운
	백
*/

enum CubeBlockFace : unsigned char;
class CubeBlockBuilder {
private:
	static std::vector<CubeTexture*> *pTextures;
	static const std::vector<float> vertexPositions;
	static const std::vector<float> texcoords;
	static const std::vector<cocos2d::Vec3> blockPositions;
	static const std::vector<std::vector<unsigned char>> facesColorIDs;
	static const std::map<CubeBlockFace, std::vector<unsigned short>>indices;

//public:


	static const std::vector<float>& getVertexPositions() { return vertexPositions; }
	static const std::vector<float>& getTexCoords() { return texcoords; }
	static const std::vector<unsigned short>& getIndices(CubeBlockFace face) { return indices.at(face); }

	static CubeTexture* getTexture(unsigned char colorID) { assert(pTextures); return (*pTextures)[colorID]; }
	static const std::vector<unsigned char>& getColorIDs(unsigned char blockID) { return facesColorIDs[blockID]; }
	static const cocos2d::Vec3 getBlockPosition(unsigned char blockID) { return blockPositions[blockID]; }
	friend class CubeBlockModel;

public:
	static void setTextures(std::vector<CubeTexture*> *_pTextures) { pTextures = _pTextures; }
};


class CubeTexture
{
public:
	enum class CubeTextureColor : unsigned char { None, Yellow, Red, Blue, Green, White, Orenge, };

private:
	static const std::map<CubeTextureColor, std::string> nameMap;
	static const std::map<CubeTextureColor, unsigned int> colorBitMap;

	cocos2d::Texture2D *pTexture;
	cocos2d::Image * pImage;
	unsigned char cbit[16];
	std::string name;

	void createTexture(CubeTextureColor eColor);
public:
	CubeTexture(CubeTextureColor eColor);
	//	unsigned char * getBit() { return cbit; }
	cocos2d::Texture2D * getTexture() { return pTexture; }


};


class CubeBlockModel {
private:
	cocos2d::Sprite3D *pModel;
	std::vector<cocos2d::Mesh *>pMeshs;
	unsigned char id;

	void addPlaneMesh(const std::vector<unsigned short> &indices, CubeTexture *pTexture);
	void createModel(unsigned char id);
public:
	CubeBlockModel(	unsigned char id):pModel(nullptr), id(id){		createModel(id);	}
	void setEngine(cocos2d::Node *pNode); 


	cocos2d::Sprite3D * getModel() { return pModel; }
	void setBlock(cocos2d::Vec3 &localRight, cocos2d::Vec3 &localLook, int posID);

};

class CubeModel {
private:
	std::vector<CubeTexture*> textures;
	std::vector<CubeBlockModel *> blocks;
	cocos2d::Sprite3D * node;
	
public:
	CubeModel(int nBlockCount=27);
	~CubeModel();
	void setModel(model_coor*);
	void setModel(std::vector<BlockState> &v);
	void setEngine(cocos2d::Node *pNode); 


	cocos2d::Sprite3D * getModel() { return node; }

private:
	cocos2d::Vec3 getLocalAxisFromDir(AxisDir dir);
	cocos2d::Vec3 getLocalAxisFromDir(localAxisID dir);
};




//
//class TexTest {
//	static std::vector<CubeTexture*> textures;
//	cocos2d::Sprite3D* pModel;
//	cocos2d::Mesh *pMesh;
//public:
//	TexTest(int n);
//	void setEngine(cocos2d::Node *pNode);
//
//	void setTexture(int i);
//	void setPos(cocos2d::Vec3 &v);
//
//	static void setTex();
//};