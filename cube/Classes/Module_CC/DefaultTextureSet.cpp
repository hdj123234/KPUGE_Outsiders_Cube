#include"stdafx.h"

#include "DefaultTextureSet.h"
#include"cocos2d.h"


// ------------------------------------------ RawCCTexture ------------------------------------------ 

RawCCTexture::RawCCTexture(const std::string &name, unsigned int colorCode)
{

	memcpy(cbit, &colorCode, 4);
	memcpy(cbit + 4, &colorCode, 4);
	memcpy(cbit + 8, &colorCode, 4);
	memcpy(cbit + 12, &colorCode, 4);

	image = new cocos2d::Image();
	image->initWithRawData(cbit, 16, 2, 2, 32);
	//pImage->initWithRawData(cbit, 4, 1, 1, 8);
	texture = cocos2d::CCTextureCache::sharedTextureCache()->addImage(image, name);
}

RawCCTexture::~RawCCTexture()
{
	cocos2d::CCTextureCache::sharedTextureCache()->removeTexture(texture);
	image->autorelease();
}

// ------------------------------------------ DefaultTextureSet ------------------------------------------ 

DefaultTextureSet::DefaultTextureSet()
{

	//enum class CubeTextureColor : unsigned char { None, Yellow, Red, Blue, Green, White, Orenge, };
	//std::make_pair(CubeTexture::CubeTextureColor::None	,0xffffffff),
	//std::make_pair(CubeTexture::CubeTextureColor::Yellow,0xff00ffff),
	//std::make_pair(CubeTexture::CubeTextureColor::Red	,0xff0000ff),
	//std::make_pair(CubeTexture::CubeTextureColor::Blue	,0xffff0000),
	//std::make_pair(CubeTexture::CubeTextureColor::Green	,0xff00ff00),
	//std::make_pair(CubeTexture::CubeTextureColor::White	,0xffffffff),
	//std::make_pair(CubeTexture::CubeTextureColor::Orenge,0xff00a5ff),
	
	//ABGR
	rawCCTextures.push_back(new RawCCTexture("RawNone"		,0xff695E56));
	//
	rawCCTextures.push_back(new RawCCTexture("RawYellow"	, 0xff04f1fe));
	// 254 241 4
	rawCCTextures.push_back(new RawCCTexture("RawRed"		, 0xff221ceb));
	// 235 28 34
	rawCCTextures.push_back(new RawCCTexture("RawBlue"		,0xffa45401));
	// 1 84 164
	rawCCTextures.push_back(new RawCCTexture("RawGreen"		,0xff07fc8e));
	// 142 252 7
	rawCCTextures.push_back(new RawCCTexture("RawWhite"		,0xffe6e6e6));
	// 230 230 230
	rawCCTextures.push_back(new RawCCTexture("RawOrenge"	,0xff1e8ff7));
	// 247 143 30

	textures.reserve(rawCCTextures.size());
	for (auto p : rawCCTextures) textures.push_back(p->getTexture());
}

DefaultTextureSet::~DefaultTextureSet()
{
	for (auto p : rawCCTextures)
		delete p;
	rawCCTextures.clear();
	textures.clear();
}

std::vector<cocos2d::Texture2D*> DefaultTextureSet::getTextureSet_Rubiks()
{
	std::vector<cocos2d::Texture2D*> retval;
	retval.reserve(7);

	retval.push_back(rawCCTextures[0]->getTexture());
	retval.push_back(rawCCTextures[1]->getTexture());
	retval.push_back(rawCCTextures[2]->getTexture());
	retval.push_back(rawCCTextures[3]->getTexture());
	retval.push_back(rawCCTextures[4]->getTexture());
	retval.push_back(rawCCTextures[5]->getTexture());
	retval.push_back(rawCCTextures[6]->getTexture());

	return retval;
}
