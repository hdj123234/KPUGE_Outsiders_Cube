#pragma once

#include"interface\ICCResource.h"
#include"math\Vec2.h"


namespace cocos2d {
	class Label;
	class EventListenerTouchOneByOne;
	class Menu;
	class Scene;
	class Ref;
	class Sprite;
}

class ICCInputEvent;
class CCSceneMover;
struct INI_TitleItem_Select;
struct INI_TitleItem_Button;
class MyFunctor;
struct ACData_Label;
struct ACData_Button;
struct ACData_CheckBox;

typedef std::vector<ICCInputEvent *> InputMsgCollector;

class CCSimpleComponent : public ICCNode {
protected:
	cocos2d::Node *node;
	bool bEnable;

protected:
	CCSimpleComponent() {}

public:
	CCSimpleComponent(cocos2d::Node *node);
	virtual ~CCSimpleComponent();

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::SimpleType; }

	//override ICCNode
	virtual void disable();
	virtual void enable() ;
	virtual void show();
	virtual void hide();
	virtual void setCCParent(cocos2d::Node *parent);
	virtual cocos2d::Node * getCCNode() { return node; }
	virtual bool isEnable()const { return bEnable; }

};
class CCBasicComponent : public CCSimpleComponent {
private:
	bool bPosNorm;
	cocos2d::Vec2 pos;
	cocos2d::Vec2 scale;

public:
	CCBasicComponent(	cocos2d::Node *node,
						const cocos2d::Vec2 &pos= cocos2d::Vec2(0,0),
						bool bPosNorm=false,
						const cocos2d::Vec2 &scale = cocos2d::Vec2(1, 1)) 
		:CCSimpleComponent(node), 
		scale(scale), 		pos(pos), 		bPosNorm(bPosNorm)	{}

	virtual ~CCBasicComponent() {}

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::NormalType; }

	//override ICCNode
	virtual void setCCParent(cocos2d::Node *parent) ;

	void setPosition(const cocos2d::Vec2 &pos);

};

class CCTextLabel : public CCSimpleComponent {
private:
	cocos2d::Label *label;
public:
	CCTextLabel(cocos2d::Label *label);
	virtual ~CCTextLabel() {}

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::TextLabel; }

	void setLabel(const std::string &s);

};


class CCTextLabelAndBG : public CCSimpleComponent {
private:
	cocos2d::Label *label;
public:
	CCTextLabelAndBG(cocos2d::Label *label, cocos2d::Node *sprite);
	virtual ~CCTextLabelAndBG() {}

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::LabelAndBG; }
	
	void setLabel(const std::string &s);

};


class CCZButton : public ICCNode {
protected:
	cocos2d::Node *bg;
	cocos2d::Node *button_Normal;
	cocos2d::Node *button_Press;
	cocos2d::Node *currentButton;
	cocos2d::EventListenerTouchOneByOne *touchListener;
	cocos2d::Vec2 pos;
	bool bLeft;
	InputMsgCollector &collector;
	float mouseFix;
	bool bEnable;

	static bool bSelected;
public:
	CCZButton(cocos2d::Node *bg, cocos2d::Node *button_Normal, cocos2d::Node *button_Press,float width, bool bLeft, InputMsgCollector &collector);
	virtual ~CCZButton();

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::SimpleType; }

	//override ICCNode
	virtual void disable();
	virtual void enable() ;
	virtual void show();
	virtual void hide();
	virtual void setCCParent(cocos2d::Node *parent);
	virtual cocos2d::Node * getCCNode() { return currentButton; }
	virtual bool isEnable()const { return bEnable; }

	void createListener();

	static bool isSelected() { return bSelected; }
};

class CCTitleItem_Button : public CCSimpleComponent {
private:
	cocos2d::Menu *menu;
	CCSceneMover * sceneMover;
public:
	CCTitleItem_Button(const INI_TitleItem_Button & ini, MyFunctor &f);
	CCTitleItem_Button(const INI_TitleItem_Button & ini, cocos2d::Scene *targetScene);

	virtual ~CCTitleItem_Button();

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::TitleItem_Button; }

	void buildThis(const INI_TitleItem_Button & ini, MyFunctor &f);
};

class CCTitleItem_Select : public ICCNode {
public:
protected:
	typedef std::map<unsigned char, cocos2d::Node*> ItemMap;

	cocos2d::Menu *buttonL;
	cocos2d::Menu *buttonR;
	ItemMap items;
	ItemMap::iterator currentItem;
	ItemMap::iterator iter_End;
	ItemMap::iterator iter_Begin;
	bool bEnable;

public:
	CCTitleItem_Select(const INI_TitleItem_Select& ini);
	virtual ~CCTitleItem_Select();

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::TitleItem_Select; }

	//override ICCNode
	virtual void disable() {}//비활성화
	virtual void enable() {} //비활성화
	virtual void show() {}	 //비활성화
	virtual void hide() {}	 //비활성화
	virtual void setCCParent(cocos2d::Node *parent);
	virtual cocos2d::Node * getCCNode() { return nullptr; }	//비활성화
	virtual bool isEnable()const {return bEnable;	}

	void func_L(cocos2d::Ref * pSender);
	void func_R(cocos2d::Ref * pSender);
	

	unsigned char getSelectedItemID() { return currentItem->first; }
	
};

class CCImageLabel : public CCSimpleComponent {
public:
	CCImageLabel(const ACData_Label& data);
	virtual ~CCImageLabel() {}

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::ImageLabel; }
};

class CCSimpleButton : public CCSimpleComponent {
private:
public:
	CCSimpleButton(const ACData_Button& data, MyFunctor &functor);
	virtual ~CCSimpleButton() {}

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::ImageLabel; }
};


class CCCheckBox : public ICCNode{
private:
	bool bChecked;
	bool bVisible;
	cocos2d::Sprite *label;
	cocos2d::Sprite *checkBox_On;
	cocos2d::Sprite *checkBox_Off;
	cocos2d::EventListenerTouchOneByOne *listener;

public:
	CCCheckBox(const ACData_CheckBox& data);
	virtual ~CCCheckBox();

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::CheckBox; }

	virtual void disable() {}
	virtual void enable() {}
	virtual void show();
	virtual void hide();
	virtual void setCCParent(cocos2d::Node *parent) ;
	virtual cocos2d::Node * getCCNode() { return nullptr; }//disable
	virtual bool isEnable() const { return bVisible; }

	bool isChecked() { return bChecked; }
	
private:
	void setState();
};




