#pragma once

#include"interface\ICCResource.h"
#include"interface\ICCResourceBuilder.h"
#include<string>

class CCVolumnManager {
private:
	float masterVolumn;
	float bgmVolumn;
	float seVolumn;

	CCVolumnManager();
	~CCVolumnManager() {}

public:
	void setMasterVolumn(float f);
	float getMasterVolumn() { return masterVolumn; }

	void setBGMVolumn(float f);
	float getBGMVolumn() { return bgmVolumn; }

	void setSEVolumn(float f);
	float getSEVolumn() { return seVolumn; }

	static CCVolumnManager* getInstance() {
		static CCVolumnManager instance;
		return &instance;
	}
};

class CCAudio_BGM : public ICCAudio, public ICCResourceBuilder {
private:
	const std::string path;
	static std::string runPath;

public:
	CCAudio_BGM(const std::string& path);
	virtual ~CCAudio_BGM();

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::BGM; }

	//override ICCAudio
	virtual const std::string& getPath() { return path; }
	virtual void run() { run(true); }

	//override ICCResourceBuilder
	virtual ICCResource * buildResource() {return new CCAudio_BGM(*this); }

	virtual void run(bool bLoop );

	static std::string getRunPath() { return runPath; }
};

class CCAudio_SE : public ICCAudio, public ICCResourceBuilder {
private:
	const std::string path;
public:
	CCAudio_SE(const std::string& path);
	virtual ~CCAudio_SE();

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::SE; }

	//override ICCAudio
	virtual const std::string& getPath() { return path; }
	virtual void run();

	//override ICCResourceBuilder
	virtual ICCResource * buildResource() { return new CCAudio_SE(*this); }
};