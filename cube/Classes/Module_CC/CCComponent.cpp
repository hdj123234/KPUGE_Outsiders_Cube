#include"stdafx.h"

#include "CCComponent.h"

#include"cocos2d.h"
#include"CCInputEvent.h"
#include"..\MyFunctor.h"
#include"..\Types\InitializeData_Title.h"
#include"..\Types\AssistantComponentData.h"
#include"..\Types\Type_Component.h"

CCSimpleComponent::CCSimpleComponent(cocos2d::Node * node)
	:node(node) , bEnable(false)
{
	node->retain();
}

CCSimpleComponent::~CCSimpleComponent()
{
	node->autorelease();
}


void CCSimpleComponent::disable()
{
	node->pause();
	bEnable = false;
}

void CCSimpleComponent::enable()
{
	node->resume();
	bEnable = true;
}

void CCSimpleComponent::show()
{
	node->setVisible(true);
	bEnable = true;
}

void CCSimpleComponent::hide()
{
	node->setVisible(false);
	bEnable = false;
}

void CCSimpleComponent::setCCParent(cocos2d::Node * parent)
{
	parent->addChild(node);

}



void CCBasicComponent::setCCParent(cocos2d::Node * parent)
{
	parent->addChild(node);
	if (bPosNorm)
		node->setPositionNormalized(pos);
	else
		node->setPosition(pos);
	node->setScale(scale.x, scale.y);

}

void CCBasicComponent::setPosition(const cocos2d::Vec2 & position)
{
	pos = position;
	if (bPosNorm)
		node->setPositionNormalized(pos);
	else
		node->setPosition(pos);
}



CCTextLabel::CCTextLabel(cocos2d::Label * label)
	:CCSimpleComponent(label),
	label(label)
{
}

void CCTextLabel::setLabel(const std::string & s)
{
	label->setString(s);
}


//--------------------------- CCTextLabelAndBG ----------------------------------------------

CCTextLabelAndBG::CCTextLabelAndBG(cocos2d::Label * label, cocos2d::Node * sprite)
	:CCSimpleComponent(sprite),
	label(label)
{
	sprite->addChild(label);
	
}

void CCTextLabelAndBG::setLabel(const std::string & s)
{
	label->setString(s);
}

//--------------------------- CCZButton ----------------------------------------------

bool CCZButton::bSelected = false;


CCZButton::CCZButton(cocos2d::Node * bg, cocos2d::Node * button_Normal, cocos2d::Node * button_Press, float width, bool bLeft, InputMsgCollector &collector)
	:bg(bg), button_Normal(button_Normal), button_Press(button_Press), bLeft(bLeft), collector(collector),
	bEnable(false)
{
	using namespace cocos2d;
	assert(bg&&button_Normal&&button_Press);
	bg				->retain();
	button_Normal	->retain();
	button_Press	->retain();
	
	Vec2 c = Vec2(0.5, 0.5);
	//위치 및 앵커 설정
	Vec2 anchor;
	if (bLeft)
		pos = anchor = Vec2(0, 0.5);
	else
		pos = anchor = Vec2(1, 0.5);
	bg->setAnchorPoint(anchor);
	button_Normal->setAnchorPoint(c);
	button_Press->setAnchorPoint(c);
	bg->setPositionNormalized(pos);
	if (bLeft)
		pos = Vec2(0+width/2, 0.5);
	else
		pos = Vec2(1-width/2, 0.5);
	button_Normal->setPositionNormalized(pos);
	button_Press->setPositionNormalized(pos);

	button_Press->setVisible(false);
	createListener();
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, bg);

	currentButton = button_Normal;
}

CCZButton::~CCZButton()
{
	bg				->autorelease();
	button_Normal	->autorelease();
	button_Press	->autorelease();
}

void CCZButton::disable()
{
	touchListener->setEnabled(false);
	bEnable = false;
}

void CCZButton::enable()
{
	touchListener->setEnabled(true);

	bEnable = true;
}

void CCZButton::show()
{
	CCZButton::enable();
}

void CCZButton::hide()
{
	CCZButton::disable();
}

void CCZButton::setCCParent(cocos2d::Node * parent)
{
	parent->addChild(bg);
	parent->addChild(button_Normal);
	parent->addChild(button_Press);
}

void CCZButton::createListener()
{
	using namespace cocos2d;
	touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = [=](Touch *touch, Event *ev)->bool 
	{
		if (button_Normal->getBoundingBox().containsPoint(touch->getLocation()))
		{
			button_Normal->setVisible(false);
			button_Press->setVisible(true);
			pos.y = 0.5;
			button_Press->setPositionNormalized(pos);
			currentButton = button_Press;
			mouseFix = touch->getLocation().y / Director::getInstance()->getWinSize().height -0.5;
			bSelected = true;
			return true;
			button_Press->setRotation(0);

		}
		else
			return false;
	};
	touchListener->onTouchMoved	=[=](Touch *touch, Event *ev)
	{
		float pyTmp = pos.y;
		pos.y = touch->getLocation().y / Director::getInstance()->getWinSize().height- mouseFix;
		 
		float scroll = pos.y- pyTmp;
		collector.push_back(new CCInputEvent_ZButton(bLeft, scroll));
		auto p = button_Press->getAnchorPoint();
		button_Press->setAnchorPoint(Vec2(0.5,0.5));
		auto degree = (pos.y - 0.5) * 360;
		if (!bLeft)
			degree = -degree;
		button_Press->setRotation(degree);
		button_Press->setAnchorPoint(p);
		button_Press->setPositionNormalized(pos);
	
	};
	touchListener->onTouchEnded	=[=](Touch *touch, Event *ev)
	{
		pos.y = 0.5;
		button_Press->setPositionNormalized(pos);
		button_Normal->setVisible(true);
		button_Press->setVisible(false);
		bSelected = false;
		button_Press->setRotation(0);
	};
	touchListener->onTouchCancelled=[=](Touch *touch, Event *ev)
	{
		pos.y=0.5;
		
		button_Press->setVisible(false);
		button_Normal->setVisible(true);
		button_Normal->setPositionNormalized(pos);
		currentButton = button_Normal;

		bSelected = false;
		button_Press->setRotation(0);
	}; 
	touchListener->retain();
}

//--------------------------- CCTitleItem_Button ----------------------------------------------

CCTitleItem_Button::CCTitleItem_Button(const INI_TitleItem_Button & ini, MyFunctor &f)
	:sceneMover(nullptr)
{
	buildThis(ini, f);
}

CCTitleItem_Button::CCTitleItem_Button(const INI_TitleItem_Button & ini, cocos2d::Scene *targetScene)
	:sceneMover(new CCSceneMover(targetScene))
{
	MyFunctor f= MyFunctor(*sceneMover);
	buildThis(ini, f);
	//buildThis(ini, MyFunctor(*sceneMover));
}

CCTitleItem_Button::~CCTitleItem_Button()
{
	if (sceneMover) delete sceneMover;
}


void CCTitleItem_Button::buildThis(const INI_TitleItem_Button & ini, MyFunctor &f)
{
	auto menu = cocos2d::Menu::create();
	auto winSize = cocos2d::Director::getInstance()->getWinSize();
	auto item = cocos2d::MenuItemImage::create(ini.image1, ini.image2, f);
	item->setScale(ini.scaleX *winSize.width / item->getContentSize().width);
	item->setPositionNormalized(cocos2d::Vec2(0.0, 0.0));
	float anchorY;
	switch (ini.anchorY) {
	case Anchor_Y::C: anchorY = 0.5; break;
	case Anchor_Y::T: anchorY = 1; break;
	case Anchor_Y::B: anchorY = 0; break;
	}
	item->setAnchorPoint(cocos2d::Vec2(0.5, anchorY));
	menu->addChild(item);
	menu->setPositionNormalized(cocos2d::Vec2(ini.posX, ini.posY));
	
	menu->retain();
	node = menu;
}

//--------------------------- CCTitleItem_Select ----------------------------------------------

CCTitleItem_Select::CCTitleItem_Select(const INI_TitleItem_Select & ini)
	:bEnable(false)
{
	using namespace cocos2d;

	Menu *menu;
	MenuItemImage *item;
	auto winSize = cocos2d::Director::getInstance()->getWinSize();
	float anchorY;
	switch (ini.anchorY) {
	case Anchor_Y::C: anchorY = 0.5; break;
	case Anchor_Y::T: anchorY = 1; break;
	case Anchor_Y::B: anchorY = 0; break;
	}
	float buttonCenterDistance = ini.itemScaleX / 2 + ini.buttonGab + ini.buttonScaleX / 2;
	
	menu = cocos2d::Menu::create();
	item = MenuItemImage::create(ini.buttonL_Normal, ini.buttonL_Press, CC_CALLBACK_1(CCTitleItem_Select::func_L,this));
	item->setScale(ini.buttonScaleX *winSize.width / item->getContentSize().width);
	item->setPositionNormalized(cocos2d::Vec2(0.0, 0.0));
	item->setAnchorPoint(cocos2d::Vec2(0.5, anchorY));
	menu->addChild(item);
	menu->setPositionNormalized(cocos2d::Vec2(0.5 - buttonCenterDistance , ini.posY));
	buttonL = menu;

	menu = cocos2d::Menu::create();
	item = MenuItemImage::create(ini.buttonR_Normal, ini.buttonR_Press, CC_CALLBACK_1(CCTitleItem_Select::func_R, this));
	item->setScale(ini.buttonScaleX *winSize.width / item->getContentSize().width);
	item->setPositionNormalized(cocos2d::Vec2(0.0, 0.0));
	item->setAnchorPoint(cocos2d::Vec2(0.5, anchorY));
	menu->addChild(item);
	menu->setPositionNormalized(cocos2d::Vec2(0.5+ buttonCenterDistance, ini.posY));
	buttonR = menu;

	for (auto data : ini.items)
	{
		auto sprite = Sprite::create(data.second);
		items[data.first] = sprite;
		sprite->setScale(ini.itemScaleX *winSize.width / sprite->getContentSize().width);
		item->setAnchorPoint(cocos2d::Vec2(0.5, anchorY));
		sprite->setPositionNormalized(cocos2d::Vec2(0.5, ini.posY));
		sprite->retain();
	}
	currentItem = items.find( ini.defaultItemID);
	iter_Begin = items.begin();
	iter_End = items.end();


	buttonL->retain();
	buttonR->retain();
}

CCTitleItem_Select::~CCTitleItem_Select()
{
}

void CCTitleItem_Select::setCCParent(cocos2d::Node * parent)
{
	parent->addChild(buttonL);
	parent->addChild(buttonR);
	for (auto data : items)
	{
		parent->addChild(data.second);
		data.second->setVisible(false);
	}
	currentItem->second->setVisible(true);
}

void CCTitleItem_Select::func_L(cocos2d::Ref * pSender)
{
	currentItem->second->setVisible(false);

	--currentItem; 
	if ( currentItem == iter_End)
		--currentItem;
	currentItem->second->setVisible(true);

}


void CCTitleItem_Select::func_R(cocos2d::Ref * pSender)
{
	currentItem->second->setVisible(false);

	++currentItem; 
	if (currentItem == iter_End)
		currentItem = iter_Begin; 

	currentItem->second->setVisible(true);

}



CCImageLabel::CCImageLabel(const ACData_Label & data)
{
	using namespace cocos2d;

	cocos2d::Vec2 anchor;
	cocos2d::Vec2 pos;

	auto winSize = Director::getInstance()->getWinSize();
	
	node = Sprite::create(data.image);
	node->setPositionNormalized(data.pos);
	float size = data.scale *winSize.width / node->getContentSize().width;
	node->setScale(size);
	anchor = getAnchor(data.anchor_x, data.anchor_y);
	node->setAnchorPoint(anchor);
	node->setOpacity(data.opacity);

	node->retain();

}

CCSimpleButton::CCSimpleButton(const ACData_Button & data, MyFunctor &functor)
{
	using namespace cocos2d;

	cocos2d::Vec2 anchor;
	cocos2d::Vec2 pos;


	auto winSize = Director::getInstance()->getWinSize();
	auto menu = Menu::create();
	pos = data.pos;
	auto item = MenuItemImage::create(data.image1, data.image2, functor);
	menu->addChild(item);
	node = menu;
	node->setPositionNormalized(data.pos);
	anchor = getAnchor(data.anchor_x, data.anchor_y);
	item->setAnchorPoint(anchor);
	item->setScale(data.scale *winSize.width / item->getContentSize().width);
	node->retain();
}

CCCheckBox::CCCheckBox(const ACData_CheckBox & data)
	:bChecked(false),bVisible(false)
{
	using namespace cocos2d;

	cocos2d::Vec2 anchor;

	auto winSize = Director::getInstance()->getWinSize();

	label = Sprite::create(data.labelImage);
	label->setPositionNormalized(data.labelPos);
	anchor = getAnchor(data.labelAnchor_x, data.labelAnchor_y);
	label->setAnchorPoint(anchor);
	label->setScale(data.labelScale *winSize.width / label->getContentSize().width);
	label->retain();

	checkBox_Off = Sprite::create(data.checkBoxImage1);
	checkBox_Off->setPositionNormalized(data.checkBoxPos);
	anchor = getAnchor(data.checkBoxAnchor_x, data.checkBoxAnchor_y);
	checkBox_Off->setAnchorPoint(anchor);
	auto size = data.checkBoxScale*winSize.width / checkBox_Off->getContentSize().width;
	checkBox_Off->setScale(size);
	checkBox_Off->retain();

	checkBox_On = Sprite::create(data.checkBoxImage2);
	checkBox_On->setPositionNormalized(data.checkBoxPos);
	anchor = getAnchor(data.checkBoxAnchor_x, data.checkBoxAnchor_y);
	checkBox_On->setAnchorPoint(anchor);
	checkBox_On->setScale(size);
	checkBox_On->retain();


	listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = [=](Touch *touch, Event *ev)->bool 
	{
		if (checkBox_Off->getBoundingBox().containsPoint(touch->getLocation()))
		{
			bChecked = !bChecked;
			setState();
			return true;
		}
		else
			return false;
	};
	listener->retain();
	CCCheckBox::setState();
}

CCCheckBox::~CCCheckBox()
{
	label		->autorelease();
	checkBox_On	->autorelease();
	checkBox_Off->autorelease();
	listener	->autorelease();
}

void CCCheckBox::show()
{
	if (bVisible) return;
	bVisible = true;
	cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, label);
	CCCheckBox::setState();
}

void CCCheckBox::hide()
{

	if (!bVisible) return;
	bVisible = false;
	CCCheckBox::setState();
	cocos2d::Director::getInstance()->getEventDispatcher()->removeEventListener(listener);
}

void CCCheckBox::setCCParent(cocos2d::Node * parent)
{
	parent->addChild(label		);
	parent->addChild(checkBox_On	);
	parent->addChild(checkBox_Off);
}

void CCCheckBox::setState()
{
	if (bVisible)
	{
		label->setVisible(true);
		if (bChecked)
		{
			checkBox_On->setVisible(true);
			checkBox_Off->setVisible(false);
		}
		else
		{
			checkBox_On->setVisible(false);
			checkBox_Off->setVisible(true);

		}
	}
	else
	{

		label->setVisible(false);
		checkBox_On->setVisible(false);
		checkBox_Off->setVisible(false);
	}
}
