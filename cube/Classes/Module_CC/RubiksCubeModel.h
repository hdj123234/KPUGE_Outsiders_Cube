#pragma once
 
#include"interface\ITwistyPuzzleModel.h"
#include"..\IPickableObject.h"

namespace cocos2d {
	class Node;
	class Layer;
	class Sprite3D;
	class Texture2D;
	class Mesh;
	class Vec3;
	class Mat4;
}
class IModelRawData_Picking;
class IModelRawData;
class ICCOutputMsg;

typedef unsigned char localAxisID;

class RubiksCubeModel : public ITwistyPuzzleModel, public IPickableObject {
private:
	const IModelRawData_Picking *pickingData;
	unsigned char cubeSize;
	cocos2d::Node *sprite;
	std::map<unsigned char, std::vector<cocos2d::Mesh*>> meshMap;
	std::vector<cocos2d::Mesh*> hiddenMeshs;
	std::vector<cocos2d::Sprite3D *> models; 
	std::vector<cocos2d::Mat4 *> blockSpins;	//�ε��� = posID
	cocos2d::Mat4 &mat_idently;
	std::vector<cocos2d::Mat4>spinMatrixs;
	unsigned int nBlock;

	mutable PickingData::PickingTargetData pickingTargetData;

//	std::vector<cocos2d::Mesh *> meshs_Plane;
//	std::vector<cocos2d::Mesh *> meshs_HiddenPlane;

public:
	virtual void pure_virtual() {};
	RubiksCubeModel(const IModelRawData * renderingData, const IModelRawData_Picking * pickingData) ;
	~RubiksCubeModel();

	//override ITwistyPuzzleModel
	virtual void setState(ICCOutputMsg *pMsg);
	virtual void setTextureSet(ICCTextureSet*textureSet) ;
	virtual IPickableObject* getPickableObject() { return this; }
	virtual void reset() ;

	//override IPickableObject
	virtual const PickingData::PickingTargetData* getPickingTargetData() const;

	//override ICCResource
	virtual Type_CCResource getTypeResource()const { return Type_CCResource::TwistyPuzzleModel; }

	//override ICCNode
	virtual void disable() { hide(); }
	virtual void enable() { show(); }
	virtual void show();
	virtual void hide();
	virtual void setCCParent(cocos2d::Node *parent);
	virtual cocos2d::Node * getCCNode() { return sprite; }
	virtual bool isEnable()const { return true; }

private:
	void setBlock(cocos2d::Sprite3D *block, cocos2d::Vec3 localRight, cocos2d::Vec3 localLook, unsigned char posID);
	cocos2d::Vec3 getLocalAxisFromDir(localAxisID dir);
	std::vector<unsigned char> getSpinBlockIDs(unsigned char axisID, unsigned char lineID);
	void addSpinMatrix(unsigned char axisID, bool bCW, float angle);
};

 