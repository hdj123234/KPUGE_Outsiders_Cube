#include"stdafx.h"

#include "CCAudio.h"
#include"SimpleAudioEngine.h"

using namespace CocosDenshion;

CCVolumnManager::CCVolumnManager()
	:masterVolumn(1), bgmVolumn(1), seVolumn(1)
{
	bgmVolumn=SimpleAudioEngine::getInstance()->getBackgroundMusicVolume();
	seVolumn=SimpleAudioEngine::getInstance()->getEffectsVolume();
}

void CCVolumnManager::setMasterVolumn(float f)
{
	masterVolumn = f;

}

void CCVolumnManager::setBGMVolumn(float f)
{
	bgmVolumn = f;
	SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(masterVolumn*bgmVolumn);
}

void CCVolumnManager::setSEVolumn(float f)
{
	seVolumn = f;
	SimpleAudioEngine::getInstance()->setEffectsVolume(masterVolumn*seVolumn);
}



std::string CCAudio_BGM::runPath=std::string();

CCAudio_BGM::CCAudio_BGM(const std::string & path)
	:path(path)
{
	if(path!="")
		SimpleAudioEngine::getInstance()->preloadBackgroundMusic(path.c_str());
}

CCAudio_BGM::~CCAudio_BGM()
{
	if (path != "")
		SimpleAudioEngine::getInstance()->stopBackgroundMusic(path.c_str());
}


void CCAudio_BGM::run(bool bLoop)
{
	if (path == "")
	{
		runPath = "";
		SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	}
	if ( runPath!= path)
	{
		SimpleAudioEngine::getInstance()->playBackgroundMusic(path.c_str(), bLoop);
		runPath = path;
	}
}

CCAudio_SE::CCAudio_SE(const std::string & path)
	:path(path)
{
	if (path != "")
		SimpleAudioEngine::getInstance()->preloadEffect(path.c_str());

}

CCAudio_SE::~CCAudio_SE()
{
	if (path != "")
		SimpleAudioEngine::getInstance()->unloadEffect(path.c_str());
}

void CCAudio_SE::run()
{
	if (path == "") return;
	SimpleAudioEngine::getInstance()->playEffect(path.c_str());
}
