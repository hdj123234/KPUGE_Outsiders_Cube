#pragma once

#include"interface\ICCInputEvent.h"
#include"interface\ICCOutputMsg.h"

class ICCInputDummy : public ICCInputEvent {
public:
	virtual ~ICCInputDummy()  {}

	char type;
	int x;
	int y;
};

class ICCOutputDummy : public ICCOutputMsg {
public:
	virtual ~ICCOutputDummy() = 0 {}
};