#pragma once

enum class Type_CCOutputMsg : unsigned char {
	CubeState		,
	CameraState		,
	AssistantComponent	,
	SetTimer		,
	RunAudio		,
	Clear			,
	Back
};

class ICCOutputMsg {
public:
	virtual ~ICCOutputMsg()  {}

	virtual Type_CCOutputMsg getType() = 0;
};