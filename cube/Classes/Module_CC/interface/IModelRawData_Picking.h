#pragma once

#include"..\PickingData.h"

#include<vector>



class IModelRawData_Picking{
public:
	virtual ~IModelRawData_Picking()  {}

	virtual const PickingData::PickingTargetData& getInitialPickingTargetData() const =0;
	virtual PickingData::Vertex getPositionFromPosID(unsigned char id)const = 0;
};