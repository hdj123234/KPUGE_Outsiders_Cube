#pragma once

#include<vector>

namespace cocos2d {
	class Texture2D;

}

class ICCTextureSet {
public:
	virtual ~ICCTextureSet()  {}

	virtual std::vector<cocos2d::Texture2D*> getTextures()=0;
};
