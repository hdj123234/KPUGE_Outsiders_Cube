#pragma once

#include<string>

namespace cocos2d {
	class Node;
}


enum class Type_CCResource {
	TwistyPuzzleModel,
	SimpleType,
	NormalType,
	TextLabel,
	LabelAndBG,
	BGM,
	SE,
	TitleItem_Select,
	TitleItem_Button,
	ImageLabel,
	CheckBox,
};

class ICCResource {
public:
	virtual ~ICCResource()  {}

	virtual Type_CCResource getTypeResource()const=0;
};

class ICCNode : public ICCResource {
public:
	virtual ~ICCNode()  {}

	//override ICCResource
	virtual Type_CCResource getTypeResource()const = 0;

	virtual void disable() = 0;
	virtual void enable() = 0;
	virtual void show() = 0;
	virtual void hide() = 0;
	virtual void setCCParent(cocos2d::Node *parent) = 0;
	virtual cocos2d::Node * getCCNode() = 0;
	virtual bool isEnable() const =0;
};


class ICCAudio : public ICCResource {
public:
	virtual ~ICCAudio() {}

	//override ICCResource
	virtual Type_CCResource getTypeResource()const = 0;

	virtual const std::string& getPath() = 0;

	virtual void run() = 0;
};
