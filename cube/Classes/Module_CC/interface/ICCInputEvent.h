#pragma once

enum class Type_CCInputEvent : unsigned char {
	SelectObject,
	Touch,
	Exception,
	ZButton,
	Back,

	//���� ����
	Scroll,

};


class ICCInputEvent {
public:
	virtual ~ICCInputEvent() {}
	virtual Type_CCInputEvent getType() = 0;
};