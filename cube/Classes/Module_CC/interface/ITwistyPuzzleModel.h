#pragma once

#include"ICCResource.h"

#include"..\PickingData.h"

namespace cocos2d {
	class Node;

}

class ICCOutputMsg;
class ICCTextureSet;
class IPickableObject;

class ITwistyPuzzleModel : public ICCNode{
public:
	virtual ~ITwistyPuzzleModel()  {}

	//override ICCResource
	virtual Type_CCResource getTypeResource()const = 0;

	//override ICCNode
	virtual void disable() = 0;
	virtual void enable() = 0;
	virtual void show() = 0;
	virtual void hide() = 0;
	virtual void setCCParent(cocos2d::Node *parent) = 0;
	virtual cocos2d::Node * getCCNode() = 0;

	virtual void setState(ICCOutputMsg *pMsg) =0;
	virtual void setTextureSet(ICCTextureSet *) = 0;
	virtual IPickableObject* getPickableObject() = 0;
	virtual void reset() = 0;
};
