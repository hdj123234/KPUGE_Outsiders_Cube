#pragma once

#include"ICCResourceBuilder.h"

#include"math\Vec3.h"
class ITwistyPuzzleModel;

typedef cocos2d::Vec3 ModelPosition;
typedef unsigned char FaceDirectionID;

struct ModelBuildData {
	struct BlockData	{
		ModelPosition position;
		std::vector<FaceDirectionID> facesColorIDs;
		BlockData(	ModelPosition &position,
					const std::vector<unsigned char> &facesColorIDs)
			:position(position),
			facesColorIDs(facesColorIDs)		{}
	};
	std::vector<float> vertexPositions;
	std::vector<float> normals;
	std::vector<float> texcoords;
	std::map<FaceDirectionID, std::vector<unsigned short>>indices;
	std::vector<BlockData> blockDatas;
};


class IModelRawData : public ICCResourceBuilder {
public:
	virtual ~IModelRawData()  {}

	virtual ITwistyPuzzleModel * createPuzzleModel()const =0 ;
	virtual const ModelBuildData & getBuildData()const = 0;

	//override ICCResourceBuilder
	virtual ICCResource * buildResource() = 0;
};