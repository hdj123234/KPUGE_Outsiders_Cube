#pragma once

class ICCResource;

class ICCResourceBuilder {
public:
	virtual ~ICCResourceBuilder() {}

	virtual ICCResource * buildResource() = 0;
};