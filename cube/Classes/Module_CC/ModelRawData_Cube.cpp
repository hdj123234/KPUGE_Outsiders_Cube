
#include"stdafx.h"

#include"ModelRawData_Cube.h"
//#include"cocos2d.h"
#include"math\Vec3.h"
#include"RubiksCubeModel.h"
#include"DefaultTextureSet.h"
//


// ------------------------------------------ ModelRawData_Cube ------------------------------------------ 

float ModelRawData_Cube::cubeBlockHalfSize = 0.48f;
float ModelRawData_Cube::cubeBlockHalfOffset = 0.02f;

ModelRawData_Cube::ModelRawData_Cube(unsigned int size)
	:size(size)
{
	auto aabbHalfSize = cubeBlockHalfSize + cubeBlockHalfOffset;

	buildData.vertexPositions = {
		+cubeBlockHalfSize,+cubeBlockHalfSize,+cubeBlockHalfSize,
		+cubeBlockHalfSize,+cubeBlockHalfSize,-cubeBlockHalfSize,
		+cubeBlockHalfSize,-cubeBlockHalfSize,+cubeBlockHalfSize,
		+cubeBlockHalfSize,-cubeBlockHalfSize,-cubeBlockHalfSize,
		-cubeBlockHalfSize,+cubeBlockHalfSize,+cubeBlockHalfSize,
		-cubeBlockHalfSize,+cubeBlockHalfSize,-cubeBlockHalfSize,
		-cubeBlockHalfSize,-cubeBlockHalfSize,+cubeBlockHalfSize,
		-cubeBlockHalfSize,-cubeBlockHalfSize,-cubeBlockHalfSize,
	};
	aabbVertices = {
		cocos2d::Vec3(+aabbHalfSize,+aabbHalfSize,+aabbHalfSize),
		cocos2d::Vec3(+aabbHalfSize,+aabbHalfSize,-aabbHalfSize),
		cocos2d::Vec3(+aabbHalfSize,-aabbHalfSize,+aabbHalfSize),
		cocos2d::Vec3(+aabbHalfSize,-aabbHalfSize,-aabbHalfSize),
		cocos2d::Vec3(-aabbHalfSize,+aabbHalfSize,+aabbHalfSize),
		cocos2d::Vec3(-aabbHalfSize,+aabbHalfSize,-aabbHalfSize),
		cocos2d::Vec3(-aabbHalfSize,-aabbHalfSize,+aabbHalfSize),
		cocos2d::Vec3(-aabbHalfSize,-aabbHalfSize,-aabbHalfSize),
	};

	buildData.texcoords = {
		0.5f,0.5f,
		0.5f,0.5f,
		0.5f,0.5f,
		0.5f,0.5f,
		0.5f,0.5f,
		0.5f,0.5f,
		0.5f,0.5f,
		0.5f,0.5f,
	};

	//각 면을 구성하는 정점 4개의 인덱스를 반시계 순으로 입력
	faceIndices[CubeBlockFace::Front]	= std::array<unsigned short, 4>{0, 4, 6, 2};
	faceIndices[CubeBlockFace::Back]	= std::array<unsigned short, 4>{1, 3, 7, 5};
	faceIndices[CubeBlockFace::Up]		= std::array<unsigned short, 4>{0, 1, 5, 4};
	faceIndices[CubeBlockFace::Down]	= std::array<unsigned short, 4>{2, 6, 7, 3};
	faceIndices[CubeBlockFace::Left]	= std::array<unsigned short, 4>{4, 5, 7, 6};
	faceIndices[CubeBlockFace::Right]	= std::array<unsigned short, 4>{0, 2, 3, 1};

	initializeBlockDatas(); 
	initializeIndices();	 
	initializePickingTargetData();
}
ModelRawData_Cube::~ModelRawData_Cube()
{
}

ITwistyPuzzleModel * ModelRawData_Cube::createPuzzleModel() const
{
	RubiksCubeModel * pModel = new RubiksCubeModel(this,this);
	pModel->setTextureSet(DefaultTextureSet::getInstance());
	return pModel;
}

ICCResource * ModelRawData_Cube::buildResource()
{ 
	return  createPuzzleModel(); 
}

void ModelRawData_Cube::initializeBlockDatas()
{
	buildData.blockDatas.reserve(size*size);
	buildData.blockDatas.clear();
	//float center

	float blockDistance = (cubeBlockHalfSize + cubeBlockHalfOffset) * 2;
	//각 축의 -방향 모서리 블록의 중점 위치를 잡기 위한 계산
	//해당 위치는 입력된 사이즈보다 1칸 작은 큐브의 끝자락과 같음
	float startPos = -((blockDistance*(size - 1)) / 2);

	/*	면 ID
	0:은면
	1:Front = Red
	2:Up	= Yellow
	3:Right = Green
	4:Left	= Blue
	5:Back	= Orenge
	6:Down	= White
	*/
	enum eFaceID : unsigned char { None = 0, Front, Up, Right, Left, Back, Down };

	//루프 순서는 x z y
	/*	최 하단부터
	0 1 2
	3 4 5
	6 7 8
	*/
	//	unsigned int blockID=0;
	unsigned char cColorID[6];
	ModelPosition vTmp;
	unsigned int x, y, z;


	// 상,전,좌,우,하,뒤
	for (y = 0, vTmp.y = startPos; y < size; ++y, vTmp.y += blockDistance)
	{
		// Up(Y+)인덱스 : 0		Down(Y-)인덱스 : 4
		cColorID[0] = cColorID[4] = eFaceID::None;
		if (y == size - 1)	cColorID[0] = eFaceID::Up;
		if (y == 0)	cColorID[4] = eFaceID::Down;

		for (z = 0, vTmp.z = startPos; z < size; ++z, vTmp.z += blockDistance)
		{
			// Front(Z+)인덱스 : 1		Back(Z-)인덱스 : 5
			cColorID[1] = cColorID[5] = eFaceID::None;
			if (z == size - 1)	cColorID[1] = eFaceID::Front;
			if (z == 0)	cColorID[5] = eFaceID::Back;

			
			for (x = 0, vTmp.x = startPos; x < size; ++x, vTmp.x += blockDistance)
			{
				// Right(X+)인덱스 : 3		Left(X-)인덱스 : 2
				cColorID[3] = cColorID[2] = eFaceID::None;
				if (x == size - 1)	cColorID[3] = eFaceID::Right;
				if (x == 0)	cColorID[2] = eFaceID::Left;

				buildData.blockDatas.push_back(ModelBuildData::BlockData(vTmp, std::vector<unsigned char>(cColorID, cColorID + 6)));
				//				++blockID;
			}
		}
	}
}

void ModelRawData_Cube::initializeIndices()
{
	for (auto &data : faceIndices)
		initializeIndices(data.first, data.second);
}

void ModelRawData_Cube::initializeIndices(const CubeBlockFace face, const std::array<unsigned short, 4> &indices)
{
	buildData.indices[face] = std::vector<unsigned short>{ indices[0],indices[1], indices[3],indices[1],indices[2],indices[3], };
 
}

void ModelRawData_Cube::initializePickingTargetData()
{
	
	for (auto &blockData : buildData.blockDatas) 
	{
		//블록(메시)
		PickingData::Block block;
		auto &blockPos = blockData.position;
		for (auto &indices : faceIndices)
		{

			//폴리곤
			if(blockData.facesColorIDs[ indices.first]==0)
				continue;
			PickingData::Polygon polygon;
			for (int i = 0; i < 4; ++i)
			{
				
				polygon.push_back(aabbVertices[indices.second[i]] + blockPos);
			}
			block[indices.first]=std::move(polygon);
		}
		pickingTargetData.blocks.push_back(std::move(block));
	}
}
