#pragma once
#include"interface\ICCInputEvent.h"

#include"..\Types\Type_Component.h"


typedef  unsigned char AxisID;
enum AxisID_Cube {X,Y,Z};


enum class Type_Touch : unsigned char {
	Start,
	Move,
	End ,
	Cancle
};
 

enum class Type_Exception : unsigned char {
	PhoneCall,
	Debug,
};

class CCInputEvent_Touch : public ICCInputEvent{ 
private:
	Type_Touch touchType;
	unsigned int touchID;
	float posX			;
	float posY			;
public:
	CCInputEvent_Touch(	Type_Touch touchType, 
						unsigned int touchID,
						float posX,
						float posY)
		:touchType(touchType),
		touchID(touchID),
		posX(posX),
		posY(posY)		{}
	virtual ~CCInputEvent_Touch() {}

	virtual Type_CCInputEvent getType() { return Type_CCInputEvent::Touch; }
	
	Type_Touch getTouchType() { return touchType; }
	unsigned int getTouchID() { return touchID; }
	float getPosX() { return posX; }
	float getPosY() { return posY; }
};


class CCInputEvent_SelectObject : public ICCInputEvent {
private:
	Type_Touch touchType;
	Type_Component objectType;

public:
	CCInputEvent_SelectObject(Type_Touch touchType,Type_Component objectType)
		:touchType(touchType),
		objectType(objectType) {}
	virtual ~CCInputEvent_SelectObject() {}

	virtual Type_CCInputEvent getType() { return Type_CCInputEvent::SelectObject; }
	
	Type_Touch getTouchType() { return touchType; }
	Type_Component getObjectType() { return objectType; } 
};


class CCInputEvent_Exception : public ICCInputEvent {
private:
	Type_Exception exceptionType; 

public:
	CCInputEvent_Exception(Type_Exception exceptionType)
		:exceptionType(exceptionType)  {}
	virtual ~CCInputEvent_Exception() {}

	virtual Type_CCInputEvent getType() { return Type_CCInputEvent::Exception; }

	Type_Exception getExceptionType() { return exceptionType; }
};

class CCInputEvent_ZButton : public ICCInputEvent {
private:
	bool bLeft;
	float scroll;
public:
	CCInputEvent_ZButton(bool bLeft, float scroll) :bLeft(bLeft),scroll(scroll){}
	virtual ~CCInputEvent_ZButton() {}

	virtual Type_CCInputEvent getType() { return Type_CCInputEvent::ZButton; }

	float getScroll() { return scroll; }
	bool isLeft() { return bLeft; }
};

class CCInputEvent_Back : public ICCInputEvent {
private:
public:
	CCInputEvent_Back() {}
	virtual ~CCInputEvent_Back() {}

	virtual Type_CCInputEvent getType() { return Type_CCInputEvent::Back; }
};

class CCInputEvent_Scroll : public ICCInputEvent {
private:
	float scroll;
public:
	CCInputEvent_Scroll(float scroll)		:scroll(scroll) {}
	virtual ~CCInputEvent_Scroll() {}

	virtual Type_CCInputEvent getType() { return Type_CCInputEvent::Scroll; }

	float getScroll() { return scroll; }
};
