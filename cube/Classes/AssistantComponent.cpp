#include "stdafx.h"
#include "AssistantComponent.h"
#include "OutputMsg.h"

void TimerLabel::visit(OutputMsgCollector * p)
{
	if (bRun)
		p->addMsg(new OutputMsg_SetTimer(time));
}
