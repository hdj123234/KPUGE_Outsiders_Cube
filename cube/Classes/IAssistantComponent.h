#pragma once
#include "Utility.h"

enum class Type_Component : unsigned char;

class IAssistantComponent
{
public:
	virtual ~IAssistantComponent() {};
	virtual Type_Component getType() = 0;
	virtual void enable() = 0;
	virtual void disable() = 0;
	virtual bool isEnable() = 0;
	virtual void visible() = 0;
	virtual void invisible() = 0;
	virtual bool isVisible() = 0;
};
