#include "stdafx.h"
#include "NxNxNCubeBone.h"
#include"NxNxNCubeBlock.h"
#include "SpinStateNxNxNCube.h"
#include "OutputMsg.h"
//#include "Module_CC\Type_Component.h"
#include "Types\Type_Audio.h"

NxNxNCubeBone::NxNxNCubeBone()
{
}

void NxNxNCubeBone::initialize(int _cube_size)
{
	bWork = true;
	bSpinCompleteSound = false;
	cube_size = _cube_size;
	//state = NxNxNCubeBoneState::State_Rotate;
	int i = 0;
	vCubeBlock.resize(cube_size);
	for (int y_i = 0; y_i < cube_size; ++y_i) {
		unsigned char color[6] = { 0,0,0,0,0,0 };
		if (y_i == 0) color[4] = 5;
		if (y_i == cube_size - 1) color[0] = 1;
		vCubeBlock[y_i].resize(cube_size);
		for (int z_i = 0; z_i < cube_size; ++z_i) {
			if (z_i == 0) color[5] = 6;
			if (z_i == cube_size - 1) color[1] = 2;
			for (int x_i = 0; x_i < cube_size; ++x_i) {
				if (x_i == 0) color[2] = 3;
				if (x_i == cube_size - 1) color[3] = 4;
				vCubeBlock[y_i][z_i].push_back(new NxNxNCubeBlock(i++, x_i, y_i, z_i, color));
				color[2] = 0;
				color[3] = 0;
			}
			color[1] = 0;
			color[5] = 0;
		}
		color[0] = 0;
		color[4] = 0;
	}
}

void NxNxNCubeBone::reset()
{
	for (auto i : vCubeBlock) {
		for (auto j : i) {
			for (auto k : j) {
				delete k;
			}
		}
	}
	vCubeBlock.clear();
	for (auto i : spinStates)
		delete i;
	spinStates.clear();
	initialize(cube_size);
	state = NxNxNCubeBoneState::State_Rotate;
}



void NxNxNCubeBone::update(float dTime)
{
	spinUpdate(dTime);


	/*
	switch (state)
	{
	case NxNxNCubeBoneState::State_Stop:
	{
		//		if (input.dir != 0)
		state = NxNxNCubeBoneState::State_Rotate;
		break;
	}

	case NxNxNCubeBoneState::State_Rotate:
	{

		//		rotateCube(dir, input.number);
		state = NxNxNCubeBoneState::State_Replace;
		break;
	}
	case NxNxNCubeBoneState::State_Replace:
	{
		state = NxNxNCubeBoneState::State_Stop;
		break;
	}
	default:
		state = NxNxNCubeBoneState::State_Stop;
		break;
	}*/
}

std::vector<NxNxNCubeBlock*> NxNxNCubeBone::getPickableObjects()
{
	std::vector<NxNxNCubeBlock*> result;
	for (auto &i : vCubeBlock)
		for (auto &j : i)
			for (auto k : j)
				result.push_back(k);
	return result;
}

bool NxNxNCubeBone::rotateCube(RotateDir _dir, int _linenumber)
{
	if (_linenumber >= cube_size)
		return false;
	std::vector<NxNxNCubeBlock*> v_cube;
	switch (_dir)
	{
	case RotateDir::DIR_XCW:
	case RotateDir::DIR_XCCW:
		for (int y = 0; y < cube_size; ++y) {
			for (int z = 0; z < cube_size; ++z) {
				v_cube.push_back(vCubeBlock[y][z][_linenumber]);
			}
		}
		break;

	case RotateDir::DIR_YCW:
	case RotateDir::DIR_YCCW:
		for (int z = 0; z < cube_size; ++z) {
			for (int x = 0; x < cube_size; ++x) {
				v_cube.push_back(vCubeBlock[_linenumber][z][x]);
			}
		}
		break;

	case RotateDir::DIR_ZCW:
	case RotateDir::DIR_ZCCW:
		for (int y = 0; y < cube_size; ++y) {
			for (int x = 0; x < cube_size; ++x) {
				v_cube.push_back(vCubeBlock[y][_linenumber][x]);
			}
		}
		break;
	default:
		break;
	}

	for (auto &i : v_cube)
		i->rotateCube(cube_size, _dir);

	for (auto &i : v_cube) {
		vCubeBlock[i->getY()][i->getZ()][i->getX()] = i;
	}
	return true;
}

void NxNxNCubeBone::spinUpdate(float dTime)
{
	// 4차
	unsigned int firstIndex = 0;
	RotateDir firstRotateDir = RotateDir::DIR_NOTHING;
	unsigned char firstRotateLine = 255;

	// 스핀스테이트가 있다면 회전합니다.
	if (!spinStates.empty())
	{
		// spinStates를 순회돌며 이미 도는 중인 스테이트가 있는지 찾습니다.
		// 이미 도는 스테이트가 있다면 해당 스테이트의 축과 같은 스테이트만 돌려야 합니다.
		for (unsigned int i = 0; i < spinStates.size(); ++i) {
			if (spinStates[i]->isOn()) {
				// 만약 도는 스테이트를 발견했다면 해당 스테이트의 축과 라인을 저장합니다.
				auto data = static_cast<SpinStateNxNxNCube*>(spinStates[i]);
				firstRotateDir = static_cast<RotateDir>(data->getDir());
				firstRotateLine = data->getLine();
				firstIndex = i;
				// 하나라도 찾았으면 더 돌 필요는 없습니다.
				break;
			}
		}
		// 만약 하나도 찾지 못했다면 맨 앞의 스테이트를 On으로 돌리고 축과 라인을 저장합니다.
		if (firstRotateDir == RotateDir::DIR_NOTHING) {
			bSpinCompleteSound = true;
			auto data = static_cast<SpinStateNxNxNCube*>(spinStates[0]);
			data->on();
			firstRotateDir = static_cast<RotateDir>(data->getDir());
			firstRotateLine = data->getLine();
		}

		std::vector<unsigned int> overlap;
		bool isFirstDelete = false;
		// 뽑아낸 스테이트는 맨 처음으로 spin을 실행합니다.
		{
			bWork = true;
			auto data = static_cast<SpinStateNxNxNCube*>(spinStates[firstIndex]);
			data->on();
			bool bComplete = data->spin(dTime);
			if (bComplete) {
				// 회전에 성공하면 큐브를 실제로 회전시켜 결과를 저장합니다.
				// 각도가 0 이하면 제자리로 돌아온 마이너스 회전이니 제외합니다.
				if (data->getAngle() > 0)
					rotateCube(static_cast<RotateDir>(data->getDir()), data->getLine());
				// 180도 회전은 회전을 한 번 더 합니다.
				if (data->getAngle() >= 180)
					rotateCube(static_cast<RotateDir>(data->getDir()), data->getLine());

				// 모든 일은 마친 스테이트는 제거하고, 벡터가 깨지지 않게 관리합니다.
				delete spinStates[firstIndex];
				auto iter = spinStates.begin() + firstIndex;
				spinStates.erase(iter);
				isFirstDelete = true;
			}
			overlap.push_back(firstRotateLine);
		}


		// 저장된 스테이트의 다음 스테이트가 같은 축이라면 같이 돌려줍니다.
		for (unsigned int i = 0; i < spinStates.size(); )
		{
			// firstIndex에 해당되는 스테이트는 이미 위에서 spin을 실행했으니 제외합니다.
			if (!isFirstDelete && i == firstIndex) {
				++i;
				continue;
			}

			// 같은 축이 아니어도 제외합니다.
			auto data = static_cast<SpinStateNxNxNCube*>(spinStates[i]);
			if ((static_cast<unsigned char>(data->getDir()) - 1) / 2
				!= (static_cast<unsigned char>(firstRotateDir) - 1) / 2)
				return;

			// 이미 돌고있는 라인이면 종료합니다.
			for (auto i : overlap) {
				if (i == data->getLine())
					return;
			}


			// 축이 같을 경우 On으로 바꿔주고 회전시킵니다.
			data->on();
			bSpinCompleteSound = true;
			overlap.push_back(data->getLine());
			bWork = true;
			bool bComplete = data->spin(dTime);
			if (bComplete) {
				// 회전에 성공하면 큐브를 실제로 회전시켜 결과를 저장합니다.
				// 각도가 0 이하면 제자리로 돌아온 마이너스 회전이니 제외합니다.
				if (data->getAngle() > 0)
					rotateCube(static_cast<RotateDir>(data->getDir()), data->getLine());

				// 180도 회전은 회전을 한 번 더 합니다.
				if (data->getAngle() >= 180)
					rotateCube(static_cast<RotateDir>(data->getDir()), data->getLine());

				// 모든 일은 마친 스테이트는 제거하고, 벡터가 깨지지 않게 관리합니다.
				delete spinStates[i];
				auto iter = spinStates.begin() + i;
				spinStates.erase(iter);
			}
			else {
				// 아직 회전이 끝나지 않았다면 다음 스테이트로 넘어갑니다.
				++i;
			}
		}
	}
}



void NxNxNCubeBone::pushRotate(RotateDir _dir, int _linenumber, float _speed)
{
	// 5차
	if (!spinStates.empty())
	{
		// 축과 라인이 같은 스테이트가 있는지 찾기
		for (auto i : spinStates) {
			auto data = static_cast<SpinStateNxNxNCube*>(i);
			if ((static_cast<unsigned char>(_dir) - 1) / 2
				!= (static_cast<unsigned char>(data->getDir()) - 1) / 2)
				continue;
			if (_linenumber != data->getLine())
				continue;

			// 같은 방향에 같은 라인이라면 추가하지 않고 기존 스테이트 수정
			if (static_cast<unsigned char>(data->getDir())
				== static_cast<unsigned char>(_dir)) {
				data->changeSpeed(5);
				data->changeTargetAngle(180);
			}
			// 다른 방향이라면 반대로 돌도록 스테이트 수정
			else {
				data->changeSpeed(-3);
			}
			return;
		}

		// 축과 라인이 같은 스테이트가 없다면 추가하고 종료
		spinStates.push_back(new SpinStateNxNxNCube(static_cast<SpinDir>(_dir), _linenumber, _speed));
		return;
	}
	else {
		spinStates.push_back(new SpinStateNxNxNCube(static_cast<SpinDir>(_dir), _linenumber, _speed));
	}
}

void NxNxNCubeBone::pushRotateCompulsion(RotateDir _dir, int _linenumber, float _speed)
{
	if (!spinStates.empty())
	{
		auto endSpin = static_cast<SpinStateNxNxNCube*>(*(spinStates.end() - 1));

		if ((static_cast<unsigned char>(_dir) - 1) / 2
			== (static_cast<unsigned char>(endSpin->getDir()) - 1) / 2)
		{
			if (_linenumber == endSpin->getLine())
				if (endSpin->getTargetAngle() <= 90)
				{
					endSpin->changeTargetAngle(180);
					endSpin->scaleSpeed(2);
					return;
				}
		}
		//spinStates.push_back(new SpinStateNxNxNCube(static_cast<SpinDir>(_dir), _linenumber, _speed));
	}
	spinStates.push_back(new SpinStateNxNxNCube(static_cast<SpinDir>(_dir), _linenumber, _speed));
}

void NxNxNCubeBone::getBlockState(std::vector<BlockState> &r)
{
	for (auto &i : vCubeBlock)
		for (auto &j : i)
			for (auto k : j)
				r.push_back(k->getState(cube_size));
}

void NxNxNCubeBone::getSpinState(std::vector<CubeSpinState>& r)
{
	if (spinStates.empty())
		r.push_back(CubeSpinState());
	else {
		for (auto i : spinStates) {
			if (i->isOn())
				r.push_back(static_cast<SpinStateNxNxNCube*>(i)->makeSpinState());
		}
	}
}

void NxNxNCubeBone::setAllSpinSpeed(float _speed)
{
	for (auto i : spinStates) {
		i->changeSpeed(_speed);
	}
}

bool NxNxNCubeBone::checkClear()
{
	if (isSpinning()) return false;
	unsigned char colorResult[6] = {
		vCubeBlock[cube_size - 1][0][0]->getColor(0),
		vCubeBlock[0][cube_size - 1][0]->getColor(1),
		vCubeBlock[0][0][0]->getColor(2),
		vCubeBlock[0][0][cube_size - 1]->getColor(3),
		vCubeBlock[0][0][0]->getColor(4),
		vCubeBlock[0][0][0]->getColor(5)
	};
	for (auto &i : vCubeBlock)
		for (auto &j : i) {
			if (j[0]->getColor(2) != colorResult[2]) return false;
			if (j[cube_size - 1]->getColor(3) != colorResult[3]) return false;
		}
	for (auto &i : vCubeBlock[0])
		for (auto &j : i) {
			if (j->getColor(4) != colorResult[4]) return false;
		}
	for (auto &i : vCubeBlock[cube_size - 1])
		for (auto &j : i) {
			if (j->getColor(0) != colorResult[0]) return false;
		}
	for (auto &i : vCubeBlock) {
		for (auto &j : i[0])
			if (j->getColor(5) != colorResult[5]) return false;
		for (auto &j : i[cube_size - 1])
			if (j->getColor(1) != colorResult[1]) return false;
	}
	return true;
	/*
	AxisDir checkRight = vCubeBlock[0][0][0]->getLocalRight();
	AxisDir checkLook = vCubeBlock[0][0][0]->getLocalLook();

	for (auto &i : vCubeBlock)
		for (auto &j : i)
			for (auto k : j)
				if (checkRight != k->getLocalRight() || checkLook != k->getLocalLook())
					return false;
	return true;
	*/
}

void NxNxNCubeBone::visit(OutputMsgCollector * p)
{
	if (bWork) {
		std::vector<BlockState> blockVector;
		std::vector<CubeSpinState> spinVector;
		getBlockState(blockVector);
		getSpinState(spinVector);
		p->addMsg(new OutputMsg_CubeState(blockVector, spinVector));
		bWork = false;
	}
	if (bSpinCompleteSound) {
		p->addMsg(new OutputMsg_Audio(Type_Audio::SE_Spin));
		bSpinCompleteSound = false;
	}
}

NxNxNCubeBone::~NxNxNCubeBone()
{
	for (auto &i : vCubeBlock)
		for (auto &j : i)
			for (auto k : j)
			{
				delete k;
				k = nullptr;
			}
	vCubeBlock.clear();
}
