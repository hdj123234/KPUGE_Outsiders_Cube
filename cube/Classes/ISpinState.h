#pragma once


class ISpinState
{
public:
	virtual void pure_virtual() {};
	virtual ~ISpinState() {};

	virtual void on() = 0;
	virtual void off() = 0;
	virtual bool isOn() = 0;
	virtual bool spin(float dTime) = 0;
	virtual void changeSpeed(float _speed) = 0;
	virtual void scaleSpeed(float _speed) = 0;
	virtual float getTargetAngle() = 0;

	virtual float getAngle() = 0;
	virtual float getSpeed() = 0;
};
