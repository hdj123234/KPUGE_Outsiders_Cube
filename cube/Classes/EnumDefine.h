#pragma once

enum class Type_Puzzle : unsigned char {
	Square2x2x2Cube,
	RubiksCube,
	Square4x4x4Cube,
	Square5x5x5Cube,
	DEBUG = 255,
};

enum class Type_GameMode : unsigned char {
	Auto,
	Manual,
	Free
};
