#pragma once

class SceneMsg ;
class IUIManager ;

class IScene 
{
public:
	virtual void pure_virtual() = 0;
	virtual ~IScene() {};

	// 씬에서 엔진으로 전달해야 하는 메시지들을 반환
	virtual std::vector<SceneMsg*>  getSceneMsgs() = 0;

	// 경과 시간을 인자로 받아 매 프레임의 처리를 구현
	virtual void update(float dTime) = 0;

	// 엔진에서의 이벤트 처리를 위해 UI매니저의 인터페이스를 반환
	virtual IUIManager* getUIManager() = 0;
};
