#pragma once

#define INCLUDE_PATH_CUBE_R "..\CubeTest\CubeR.h"
#define INCLUDE_PATH_CUBE_Bone "..\CubeTest\CubeBone.h"
#define INCLUDE_PATH_CUBE_R_CPP "..\CubeTest\CubeR.cpp"
#define INCLUDE_PATH_CUBE_Bone_CPP "..\CubeTest\CubeBone.cpp"

#include INCLUDE_PATH_CUBE_R
#include INCLUDE_PATH_CUBE_Bone

struct Msg_CubeRotation {
	RotateDir dir;
	int line;

	
	static const Msg_CubeRotation cubeRotation_RCW ;
	static const Msg_CubeRotation cubeRotation_RCCW;
	static const Msg_CubeRotation cubeRotation_LCW ;
	static const Msg_CubeRotation cubeRotation_LCCW;
	static const Msg_CubeRotation cubeRotation_UCW ;
	static const Msg_CubeRotation cubeRotation_UCCW;
	static const Msg_CubeRotation cubeRotation_DCW ;
	static const Msg_CubeRotation cubeRotation_DCCW;
	static const Msg_CubeRotation cubeRotation_FCW ;
	static const Msg_CubeRotation cubeRotation_FCCW;
	static const Msg_CubeRotation cubeRotation_BCW ;
	static const Msg_CubeRotation cubeRotation_BCCW;
};
 

class Cube_RenderTest : public CubeBone
{
public:
	Cube_RenderTest() :CubeBone(3) {}
	bool rotateCube(Msg_CubeRotation rotation)
	{
		return CubeBone::rotateCube(rotation.dir, rotation.line);
	}
};
