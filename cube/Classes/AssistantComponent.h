#pragma once
#include "IAssistantComponent.h"
#include "OutputMsgCollector.h"

enum class Type_Component : unsigned char;

class AssistantComponent : public IAssistantComponent
{
	bool bActive;
	bool bVisible;
	Type_Component type;

public:
	virtual ~AssistantComponent() {};
	virtual Type_Component getType() { return type; }
	virtual void enable() { bActive = true; }
	virtual void disable() { bActive = false; }
	virtual bool isEnable() { return bActive; }
	virtual void visible() { bVisible = true; }
	virtual void invisible() { bVisible = false; }
	virtual bool isVisible() { return bVisible; }
};

class TimerLabel : public AssistantComponent, IOutputMsgTarget
{
	float time;
	bool bRun;
public:
	virtual void pure_virtual() {};
	TimerLabel() : time(0), bRun(false) {};
	void setTime(float _time) { time = _time; }
	void setZero() { time = 0; }
	float getTime() { return time; }
	void start() { bRun = true; setZero(); }
	void stop() { bRun = false; }
	void continueTimer() { bRun = true; }
	void update(float dTime) { if (bRun) time += dTime; }
	virtual void visit(OutputMsgCollector *p);
};

class Button : public AssistantComponent
{
public:
};