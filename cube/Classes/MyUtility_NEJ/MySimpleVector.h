#pragma once

#ifndef  M_PI
#define M_PI 3.14159265358979323846
#endif // ! M_PI

/*
	2017/12/07
*/

class MyVector2 {
public:
	float x, y;

public:
	MyVector2(float x, float y) :x(x), y(y)  {}

	operator float*() { return reinterpret_cast<float*>(&x); }
};

class MyVector3 {
public:
	float x, y, z;

public:
	MyVector3(float x, float y, float z) :x(x), y(y), z(z) {}

	operator float*() { return reinterpret_cast<float*>(&x); }
};

class MyVector4 {
public:
	float x, y, z, w;

public:
	MyVector4(float x, float y, float z, float w) :x(x), y(y), z(z),w(w) {}

	operator float*() { return reinterpret_cast<float*>(&x); }
};

inline float toRadian(float degree) { return degree * M_PI / 180 ; }
