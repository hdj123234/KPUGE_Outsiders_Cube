

#include "MyINI.h"

#include "MyUtility.h"
#ifndef HEADER_STL
#include<map>
#include<fstream>
#include<string>
#include<sstream>
#endif // !HEADER_STL

#include<cassert>

using namespace std;

//----------------------------------- MyINISection -------------------------------------------------------------------------------------


const std::string MyINISection::dummy ="";

void MyINISection::setParam(const std::string & rsParamName, const std::string & rsParam)
{ 
	properties.operator[](convertString(rsParamName)) = (rsParam);
}

const std::string & MyINISection::getParam(const std::string & rsParamName) const
{
	auto iter = properties.find(convertString(rsParamName));
	if (iter == properties.end())
		return dummy;
	return properties.at(convertString(rsParamName));
}

//const std::wstring MyINISection::getParam_wstring(const std::string & rsParamName) const
//{
//
//	return CUtility_String::convertToWString(properties.at(convertString(rsParamName)));
//}

const bool MyINISection::getParam_bool(const std::string & rsParamName) const
{
	auto iter = properties.find(convertString(rsParamName));
	if (iter == properties.end()) return false;
	return convertString(iter->second) =="true";
}

const MyVector4 MyINISection::getParam_Vector4(const std::string & rsParamName) const
{
	auto iter = properties.find(convertString(rsParamName));
	if (iter == properties.end()) return MyVector4(0, 0, 0,0);
	std::stringstream ss(iter->second);
	float f1, f2, f3, f4;
	char c;
	ss >> f1 >> c >> f2 >> c >> f3 >> c >> f4;

	return MyVector4(f1, f2, f3, f4);
}

const MyVector3 MyINISection::getParam_Vector3(const std::string & rsParamName) const
{
	auto iter = properties.find(convertString(rsParamName));
	if (iter == properties.end()) return MyVector3(0, 0,0);
	std::stringstream ss(iter->second);
	float f1, f2, f3;
	char c;
	ss >> f1 >> c >> f2 >> c >> f3;

	return MyVector3(f1, f2, f3);
}
 

const MyVector2 MyINISection::getParam_Vector2(const std::string & rsParamName) const
{
	auto iter = properties.find(convertString(rsParamName));
	if (iter == properties.end()) return MyVector2(0,0);
	std::stringstream ss(iter->second);
	float f1, f2;
	char c;
	ss >> f1 >> c >> f2;

	return MyVector2(f1, f2);
}
//
//const MyVector2 MyINISection::getParam_XMFLOAT2_ToRadian(const std::string & rsParamName) const
//{
//	using DirectX::XMFLOAT2;
//	std::stringstream ss(properties.at(convertString(rsParamName)));
//	float f1, f2;
//	char c;
//	ss >> f1 >> c >> f2;
//
//	return XMFLOAT2(DirectX::XMConvertToRadians( f1), DirectX::XMConvertToRadians(f2));
//}
// 

const float MyINISection::getParam_float(const std::string & rsParamName) const
{
//	 return std::stof(properties.at(convertString(rsParamName)));
	float retval;
	auto s = properties.at(convertString(rsParamName));
	std::stringstream(s) >> retval;
	return retval;
}


const short MyINISection::getParam_short(const std::string & rsParamName) const
{
//	return static_cast<short>(std::stoi(properties.at(convertString(rsParamName))));
	short retval;
	auto s = properties.at(convertString(rsParamName));
	std::stringstream(s) >> retval;
	return retval;
}

const unsigned int MyINISection::getParam_uint(const std::string & rsParamName) const
{
//	return static_cast<unsigned int>(std::stoul(properties.at(convertString(rsParamName))));
	unsigned int retval;
	auto s = properties.at(convertString(rsParamName));
	std::stringstream(s) >> retval;
	return retval;
}

MyINISection& MyINISection::setDefaultData(const MyINISection & rSection)
{
	for (auto &rData : rSection.properties)
		if (!properties.count(rData.first))
			properties.insert(rData);
	return *this;
}

const std::string MyINISection::convertString(const std::string & rs)
{
	return CUtility_String::convertToLower(CUtility_String::removeEscape_FrontBack(rs));
}

//----------------------------------- MyINIData -------------------------------------------------------------------------------------


MyINIData::MyINIData(const std::string & rsFileName)
{
	ifstream ifs(rsFileName);
	if(ifs.is_open())
	{
		loadData(ifs);
		ifs.close();
	}
	else
		assert(false);


}

MyINIData::MyINIData(std::istream & ifs)
{
	loadData(ifs);
}


void MyINIData::loadData(istream & ifs)
{
	using namespace std;

	MyINISection * pSection = nullptr;
	string s;

	while (!ifs.eof())
	{
		std::getline(ifs, s);
		if (!s.empty() && (s.back() == '\n' || s.back() == '\r'))
			s.pop_back();
		if (s[0] == '[')	//���� ó��
		{
			string sectionName(s.begin() + 1, s.end() - 1);
			std::transform(sectionName.begin(), sectionName.end(), sectionName.begin(), ::tolower);
			if (!sections.count(sectionName))
				sections.insert(make_pair(sectionName,MyINISection()));
			pSection = &sections.at(sectionName);
		}
		else
		{

			auto nIndex = s.find('=');
			if (nIndex == std::string::npos) continue;
			string sParamName;
			string sParamValue;
			char cEqual;
			stringstream ss(s);
			ss >> sParamName;
			ss >> cEqual;
			ss >> sParamValue;
			nIndex = sParamValue.find(';');
			if (nIndex != std::string::npos)
				sParamValue = std::string(sParamValue.begin(), sParamValue.begin() + nIndex);
			else if (!ss.eof())
			{
				string sTmp;
				std::getline(ss, sTmp, ';');
				sParamValue = sParamValue + ' ' + sTmp;
			}
			if (!pSection)
			{
				sections.insert(make_pair("", MyINISection()));
				pSection = &sections.at("");
			}
			(*pSection).setParam( sParamName, sParamValue);
		}
	}
	  
}

MyINISection MyINIData::getSection(const std::string & rsSectionName)const
{
	const std::string s = MyINISection::convertString(rsSectionName);
	if (sections.count(s))	return sections.at(s);
	else return MyINISection();
}

MyINISection & MyINIData::getSection(const std::string & rsSectionName)
{
	static MyINISection dummy;
	const std::string s = MyINISection::convertString(rsSectionName);
	if (sections.count(s))	return sections.at(s);
	else return dummy;

}

std::vector<MyINISection> MyINIData::getSection_Range(const std::string & rsSectionName, int nStartNum) const
{
	std::vector<MyINISection> retval;
	for (int i = nStartNum;; ++i)
	{
//		auto &rSection = getSection(rsSectionName + std::to_string(i));
		std::stringstream tmp;
		tmp<<i;
		const auto &rSection = getSection(rsSectionName + tmp.str());
		if (rSection.isEmpty())	break;
		retval.push_back(rSection);
	}
	return retval;
}

void MyINIData::setSection_DefaultData(const std::string & rsSectionName, const MyINISection & rParamiters)
{
	const std::string s = MyINISection::convertString(rsSectionName);
	if (!sections.count(s))	sections.insert(make_pair(s,rParamiters));
	else sections.at(s).setDefaultData(rParamiters);
}

MyINIData MyINIData::getDataFromRawData(const std::string & rawData)
{
	std::stringstream ss (rawData);

	return MyINIData(static_cast<std::istream&>(ss));
}
