#pragma once
#include<string>

/*
싱글톤 클래스

Debug.ini로 부터 정보를 읽음


*/
class MyDebugUtility {
private:
	std::string iniPath;
	
	MyDebugUtility();
	~MyDebugUtility() {}

public:
	std::string getINIPath() { return iniPath; }

public:
	static MyDebugUtility* getInstance()
	{
		static MyDebugUtility instance;
		return &instance;
	}

	
 };