
#ifndef HEADER_MYUTILITY
#define HEADER_MYUTILITY

#ifndef HEADER_STL
#include<string>
#include<sstream>
#include<fstream>
#include<iterator>
#include<algorithm>
#include<locale>
//#include<codecvt>
#endif // !HEADER_STL

//#ifndef HEADER_WINDOWS
//#include<Ws2tcpip.h>
//#include<winsock2.h>
//#include <Windows.h>
//#include <mmsystem.h>
//#endif // !HEADER_WINDOWS

#ifdef USE_DIRECTXTEX
#include<DirectXTex\DirectXTex.h>
#endif

template<class Type>
class CSingleton {
private:
	CSingleton(){}
	CSingleton(const CSingleton&){}
	~CSingleton(){}
public:
	static Type * getInstance()
	{
		static Type instance;
		return &instance;
	}
};


class CUtility_String {
public:
//	static const std::wstring convertToWString(const std::string &sString){	return  std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>().from_bytes(sString);}
//	static const std::string convertToString(const std::wstring &sWString) { return std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>().to_bytes(sWString); }

	static const std::string convertToLower(const std::string &rs)
	{
		std::string retval;
		retval.reserve(rs.size());
		std::transform(rs.begin(), rs.end(), std::back_inserter(retval), ::tolower);
		return retval;
	}
	static const std::string removeEscape_FrontBack(const std::string &rs)
	{
		auto iter_B = rs.begin();		auto iter_E = rs.end();
		auto iter_F = iter_B;
		while (iter_F != iter_E)
		{
			if (*iter_F != ' ')	break;
			++iter_F;
		}

		auto iter_RB = rs.rbegin();		auto iter_RE = rs.rend();
		auto riter_B = iter_RB;
		while (riter_B != iter_RE)
		{
			if (*riter_B != ' ')	break;
			++riter_B;
		}

		return std::string(iter_F, riter_B.base());
	}
};

class CUtility_File {
public:
	static const unsigned int getSize(const std::string &rsFileName)
	{
		std::ifstream inFile(rsFileName, std::ios::binary);
		if (!inFile.is_open())	return 0;

		inFile.seekg(0, std::ios::end);
		unsigned int  nResult = static_cast<unsigned int >(inFile.tellg());
		inFile.close();

		return nResult;
	}

};

class CUtility_Path {
public:
	static bool dividePath(const std::string &rsFullPath,
		std::string & rsPath,
		std::string & rsFileName)
	{
		rsPath.clear();
		rsFileName.clear();
		auto iter_reverse = std::find(rsFullPath.crbegin(), rsFullPath.crend(), '\\');
		if (iter_reverse != rsFullPath.crend())
		{
			auto iter = iter_reverse.base();
			std::copy(rsFullPath.cbegin(), iter, std::back_inserter(rsPath));
			std::copy(iter, rsFullPath.cend(), std::back_inserter(rsFileName));
		}
		else
		{
			iter_reverse = std::find(rsFullPath.crbegin(), rsFullPath.crend(), '/');
			auto iter = iter_reverse.base();
			std::copy(rsFullPath.cbegin(), iter, std::back_inserter(rsPath));
			std::copy(iter, rsFullPath.cend(), std::back_inserter(rsFileName));
		}
		return true;
	}

	static const std::string dividePath_FromDataFolder(const std::string &rsFullPath)
	{
		auto index = rsFullPath.find("Data\\");
		if (index == std::string::npos)
		{
			index = rsFullPath.find("Data/");
			if (index == std::string::npos)
				return std::string();
		}
		return std::string(rsFullPath.begin()+ index+5, rsFullPath.end());
	}

	static const std::string getFileNameOnly(const std::string &rsFullPath)
	{
		std::string sResult;
		auto iter_reverse = std::find(rsFullPath.crbegin(), rsFullPath.crend(), '\\');
		if (iter_reverse != rsFullPath.crend())
		{
			auto iter = iter_reverse.base();
			std::copy(iter, rsFullPath.cend(), std::back_inserter(sResult));
		}
		else
		{
			iter_reverse = std::find(rsFullPath.crbegin(), rsFullPath.crend(), '/');
			auto iter = iter_reverse.base();
			std::copy(iter, rsFullPath.cend(), std::back_inserter(sResult));
		}
		return sResult;
	}
	static const std::string getPathOnly(const std::string &rsFullPath)
	{
		auto iter_reverse = std::find(rsFullPath.crbegin(), rsFullPath.crend(), '\\');
		if (iter_reverse == rsFullPath.crend())
		{
			iter_reverse = std::find(rsFullPath.crbegin(), rsFullPath.crend(), '/');
		}
		auto iter = iter_reverse.base();
		return std::string(rsFullPath.cbegin(), iter);
	}
	static const std::pair<std::string, std::string> devideException(const std::string &rsPath)
	{
		auto iter_reverse = std::find(rsPath.crbegin(), rsPath.crend(), '.');
		auto iter = iter_reverse.base();
		return std::make_pair(std::string(rsPath.begin(), iter), std::string(iter+1, rsPath.end()));
	}
	static const std::string clipException(const std::string &rsPath)
	{
		auto iter_reverse = std::find(rsPath.crbegin(), rsPath.crend(), '.');
		auto iter = iter_reverse.base();
		return std::string(rsPath.begin(), iter-1);
	}
	static const std::string getException(const std::string &rsPath)
	{
		auto iter_reverse = std::find(rsPath.crbegin(), rsPath.crend(), '.');
		auto iter = iter_reverse.base();
		return std::string(iter + 1, rsPath.end());
	}
};




//
//#ifdef USE_DIRECTXTEX
//
//class CDirectXTexUitility {
//
//	static const D3DX11_IMAGE_INFO convertD3DX11Info(const DirectX::TexMetadata &rMetaData)
//	{
//		D3DX11_IMAGE_INFO info;
//		ZeroMemory(&info, sizeof(info));
//		info.ArraySize = rMetaData.arraySize;
//		info.Width				= rMetaData.width				;
//		info.Height				= rMetaData.height				;
//		info.Depth				= rMetaData.depth				;
//		info.ArraySize			= rMetaData.arraySize			;
//		info.MipLevels			= rMetaData.mipLevels			;
//		info.MiscFlags			= rMetaData.miscFlags			;
//		info.Format				= rMetaData.format				;
//		info.ResourceDimension	= static_cast<D3D11_RESOURCE_DIMENSION>(rMetaData.dimension );	//���� �� ���
//		//info.ImageFileFormat	= rMetaData.ImageFileFormat	;
//
//		return info;
//	}
//};
//
//#endif // USE_DIRECTXTEX



#endif