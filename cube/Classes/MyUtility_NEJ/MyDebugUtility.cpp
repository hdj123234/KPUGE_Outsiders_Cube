#include "MyDebugUtility.h"



//#ifdef _DEBUG
#include<fstream>

#include"MyINI.h"
//#endif

using namespace std;

MyDebugUtility::MyDebugUtility()
{
#if  defined(_DEBUG) & (defined(_WIN32) | defined(_WIN64))
	ifstream ifs("Debug.ini");
	if (ifs.is_open())
	{
		auto section = MyINIData(ifs).getSection();
		iniPath= section.getParam("iniPath");
		if (iniPath.empty())
		{
			iniPath = "";
			ifs.close();
			return;
		}
		char lastWord = iniPath.back();
		if (!(lastWord == '\\' || lastWord == '/'))
			iniPath = iniPath + '\\';
		ifs.close();
	}
#else
	iniPath = "";
#endif

}
