#pragma once

/*
	2017/11/07
*/

#ifndef HEADER_STL
#include<map>
#include <vector>
#include<string>
#endif // !HEADER_STL

#include"MySimpleVector.h"

class MyINISection {
private:
	static const std::string dummy;
private:
	std::map<const std::string, std::string > properties; 
public:
	MyINISection() {}
	~MyINISection() {}
	
	void setParam(const std::string &rsParamName, const std::string &rsParam);
	const bool isEmpty()const { return properties.empty(); }

	const std::string & getParam(const std::string &rsParamName)const;
	//const std::wstring getParam_wstring(const std::string &rsParamName)const;
	const bool getParam_bool(const std::string &rsParamName)const;
	const MyVector4 getParam_Vector4(const std::string &rsParamName)const;
	const MyVector3 getParam_Vector3(const std::string &rsParamName)const;
	const MyVector2 getParam_Vector2(const std::string &rsParamName)const;
//	const DirectX::XMFLOAT2 getParam_XMFLOAT2_ToRadian(const std::string &rsParamName)const;
//	const DirectX::XMFLOAT4X4 getParam_XMFLOAT4X4_FromPosAndScale(const std::string &rsParamName)const;
	const float getParam_float(const std::string &rsParamName)const;
//	const float getParam_float_ToRadian(const std::string &rsParamName)const;
	const short getParam_short(const std::string &rsParamName)const;
	const unsigned int getParam_uint(const std::string &rsParamName)const;
//	const std::string & getParam(const std::string &rsParamName)const { return SUPER::at(rsParamName); }

	MyINISection& setDefaultData(const MyINISection & rSection);
	void clear() { properties.clear(); }

	static const std::string convertString(const std::string &rs);
};


class MyINIData  {
private :
	std::map<const std::string, MyINISection> sections;
	void loadData(std::istream &ifs);

public:
//	MyINIData();
	MyINIData(const std::string &rsFileName);
	MyINIData(std::istream &ifs);
	//MyINIData(const std::stringstream &ss);
	~MyINIData() {}

	MyINISection getSection(const std::string &rsSectionName="")const;
	MyINISection & getSection(const std::string &rsSectionName="");
	std::vector<MyINISection> getSection_Range(const std::string &rsSectionName, int nStartNum=1)const;
//	std::vector<MyINISection> getSection_Range(const std::string &rsSectionName, int nDigit, int nStartNum=1)const;	//나중에 작성
	void setSection_DefaultData(const std::string &rsSectionName,const MyINISection & rParamiters);
	const bool isEmpty()const { return sections.empty(); }

	static MyINIData getDatas(const std::string &rsFileName){	return MyINIData(rsFileName);}
	static MyINIData getDataFromRawData(const std::string &rawData);
};

