#pragma once


namespace PickingData {
	struct PickingTargetData;
}

class IPickableObject
{
public:
	virtual void pure_virtual() = 0;
	virtual ~IPickableObject() {};
	virtual const PickingData::PickingTargetData* getPickingTargetData() const = 0;
};