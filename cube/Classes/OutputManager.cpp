#include "stdafx.h"
#include "OutputManager.h"
#include "OutputMsg.h"
//#include "Module_CC\interface\ICCOutputMsg.h"

#include"Module_CC\CCOutputMsg.h"
#include"Module_CC\CCOutputMsg_CubeState.h"

std::vector<ICCOutputMsg*> OutputManager::getCCOutputMsgs()
{
	//auto copydata = iccOutputMsg;
	//if (!iccOutputMsg.empty())
	//	iccOutputMsg.clear();
	//return copydata;

	return std::move(iccOutputMsg);
}

void OutputManager::setOutputMsgs(std::vector<IOutputMsg*> &_outputMsg)
{
	// 아웃풋 메시지 IOutputMsg 를 ICCOutputMsg로 컨버팅해서 벡터에 저장
	for (auto i : _outputMsg)
	{
		switch (i->getType())
		{
		case Type_OutputMsg::CubeState: {
			auto data = static_cast<OutputMsg_CubeState*>(i);
			iccOutputMsg.push_back(new CCOutputMsg_CubeState(data->getBlockState(), data->getSpinState()));
			break;
		}

		case Type_OutputMsg::CameraState: {
			auto data = static_cast<OutputMsg_CameraState*>(i);
			CCOutputMsg_CameraState* cameraState = new CCOutputMsg_CameraState();
			data->getMatrix().convertFloat16(cameraState->transform);
			iccOutputMsg.push_back(cameraState);
			break;
		}
		case Type_OutputMsg::RunAudio: {
			auto data = static_cast<OutputMsg_Audio*>(i);
			CCOutputMsg_RunAudio* runAudio = new CCOutputMsg_RunAudio(data->getAudioType());
			iccOutputMsg.push_back(runAudio);
			break;
		}
		case Type_OutputMsg::AssistantComponent: {
			auto data = static_cast<OutputMsg_AssistantComponent*>(i);
				iccOutputMsg.push_back(CCOutputMsg_Resource::createMsg(data->getComponentType(),data->isVisible(),data->isOn()));
			break;
		}
		case Type_OutputMsg::SetTimer: {
			auto data = static_cast<OutputMsg_SetTimer*>(i);
			iccOutputMsg.push_back(new CCOutputMsg_SetTimer(data->getTime()));
			break;
		}
		case Type_OutputMsg::Clear: {
			iccOutputMsg.push_back(new CCOutputMsg_Clear());
			break;
		case Type_OutputMsg::Back:
			iccOutputMsg.push_back(new CCOutputMsg_Back());
			break;
		}
		}

		// 지우기
		delete i;
		i = nullptr;
	}
	_outputMsg.clear();

	// ...

}
