#pragma once

class IPickableObject;

enum class Type_GameMode : unsigned char;
enum class Type_Puzzle : unsigned char;

class InitializeState
{
private:
	Type_GameMode gameMode;
	Type_Puzzle puzzleType;
	IPickableObject *pickableObjectPtr;
	InitializeState():pickableObjectPtr(nullptr) { }
	~InitializeState() {}
	
public:
	static InitializeState *getInstance() {
		static InitializeState retval;
		return &retval;
	}

	Type_GameMode getGameMode() { return gameMode; }
	Type_Puzzle getPuzzleType() { return puzzleType; }
	IPickableObject * getPickableObjectPtr() { return pickableObjectPtr; }

	void setState(Type_GameMode mode, Type_Puzzle puzzle) 
	{
		gameMode = mode;
		puzzleType = puzzle;
	}
	void setPickableObjectPtr(IPickableObject *ptr) { pickableObjectPtr = ptr; }
};
