#pragma once
#include "IOutputMsg.h"
class OutputMsgCollector;

class IOutputMsgTarget
{
public:
	virtual void visit(OutputMsgCollector *p) = 0;
	virtual ~IOutputMsgTarget() {};
};


class OutputMsgCollector
{
	std::vector<IOutputMsg*> outputMsgs;
public:
	virtual ~OutputMsgCollector() { for (auto i : outputMsgs) delete i; }
	void run(IOutputMsgTarget *p) { p->visit(this); }
	void addMsg(IOutputMsg* p) { outputMsgs.push_back(p); }
	std::vector<IOutputMsg*> getOutputMsg() { return std::move(outputMsgs); }
	void clear() { for (auto i : outputMsgs) delete i; }
};