#pragma once
#include "InGameData.h"

class IScene;
class InGameScene;
class InGameData;
class IPickableObject;
enum class Type_Puzzle : unsigned char;


class SceneInitializer
{
	void initializeScene(InGameScene* _scene);
public:
	virtual ~SceneInitializer() {};
//	void setInitializeState(InitializeState _initState) { initState = _initState; }
	void initializeScene(IScene* _scene);
	InGameData* createInGameData();
};
