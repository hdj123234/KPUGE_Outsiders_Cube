#include"stdafx.h"

#include "SpinStateNxNxNCube.h"

bool SpinStateNxNxNCube::spin(float dTime)
{
	angle += dTime * speed * 90;
	if (angle >= target_angle)
		return true;
	if (angle <= 0)
		return true;
	return false;
}
