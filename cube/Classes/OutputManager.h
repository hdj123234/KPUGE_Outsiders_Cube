#pragma once
#include <vector>

class ICCOutputMsg;
class IOutputMsg;

class OutputManager
{
	std::vector<ICCOutputMsg*> iccOutputMsg;

public:
	// CCOutputMsg 가 저장된 벡터를 반환합니다.
	std::vector<ICCOutputMsg*> getCCOutputMsgs();

	// OutputMsg를 CC용 메시지로 변환하여 저장합니다. 
	void setOutputMsgs(std::vector<IOutputMsg*> &_outputMsg);
};
