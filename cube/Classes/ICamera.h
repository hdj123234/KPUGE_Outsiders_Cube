#pragma once
#include "Utility.h"


class ICamera
{
public:
	virtual ~ICamera() {};
	virtual Matrix getMatrix() = 0;
};
