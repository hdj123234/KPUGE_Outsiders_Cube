#include"stdafx.h"

#include "ResourceManager.h"
#include"Enum_Resource.h"
#include"..\Module_CC\ModelRawData_Cube.h"
#include"..\Module_CC\CCAudio.h"
#include"..\Module_Manager\StartupManager.h"
#include"..\Module_Manager\Enum_Resource.h"
#include"..\Types\Type_Audio.h"
#include"..\Types\InitializeData.h"

ResourceManager::ResourceManager()
	: builderMap{
	std::make_pair(Enum_Resource::CubeModel_333,new ModelRawData_Cube(3)) ,
	std::make_pair(Enum_Resource::CubeModel_222,new ModelRawData_Cube(2)) ,
	std::make_pair(Enum_Resource::CubeModel_444,new ModelRawData_Cube(4)) ,
	std::make_pair(Enum_Resource::CubeModel_555,new ModelRawData_Cube(5)) ,
	}
{
	//Audio
	auto &rBGMList = getBGMList();
	auto enditer_BGMList = rBGMList.end();
	auto &rAudio = StartupManager::getInstance()->getInitializeData().audio;
	for (auto const &iter : rAudio.fileMap)
	{
		auto id = ResourceEnumerationManager::getID(iter.first);
		if(rBGMList.find(iter.first)== enditer_BGMList)
			builderMap[id] = new CCAudio_SE(iter.second);
		else 
			builderMap[id] = new CCAudio_BGM(iter.second);
	}

}

ResourceManager::~ResourceManager()
{
	for (auto data : builderMap) if (data.second)delete data.second;
}


bool ResourceManager::initialize()
{
	return true;
}

ICCResource * ResourceManager::getRequest(ID_Resource id)
{
	auto p = builderMap.find(id);

	if (p== builderMap.end()|| p->second==nullptr)	return nullptr;
	else return p->second->buildResource();
}
