#pragma once

class ICCResource;
typedef unsigned char ID_Resource;
class ICCResourceBuilder;


class ResourceManager {
private:
	std::map<ID_Resource, ICCResourceBuilder*> builderMap;
public:
	bool initialize();

	ICCResource * getRequest(ID_Resource id);


	static ResourceManager* getInstance() {
		static ResourceManager instance;
		return &instance;
	}

private:
	ResourceManager();
	~ResourceManager();

};