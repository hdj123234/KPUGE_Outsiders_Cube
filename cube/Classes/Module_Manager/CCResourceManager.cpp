#include"stdafx.h"

#include "CCResourceManager.h"
#include "ResourceManager.h"
#include"..\Module_CC\interface\ITwistyPuzzleModel.h"
#include"..\EnumDefine.h"
#include"Enum_Resource.h"
#include"..\Types\Type_Audio.h"
CCResourceManager::CCResourceManager()
{
}

CCResourceManager::~CCResourceManager()
{
}

ICCAudio* CCResourceManager::getAudio(Type_Audio type)
{
	auto iter = audioMap.find(type);
	if (iter == audioMap.end())
		audioMap[type] = static_cast<ICCAudio*>(ResourceManager::getInstance()->getRequest(ResourceEnumerationManager::getID(type)));
	
	return audioMap[type];
}



bool CCResourceManager::initialize()
{

	return true;
}

ITwistyPuzzleModel * CCResourceManager::getTwistyPuzzle(Type_Puzzle type)
{
	auto iter = puzzleModelMap.find(type);
	if (iter == puzzleModelMap.end())
		puzzleModelMap[type] = static_cast<ITwistyPuzzleModel*>(ResourceManager::getInstance()->getRequest(ResourceEnumerationManager::getID(type)));

	puzzleModelMap[type]->reset();
	return puzzleModelMap[type];
}

std::vector<ICCNode*> CCResourceManager::getInGameSceneComponents()
{
	return std::vector<ICCNode*>();
}
