#pragma once

class IScene;
class SceneInitializer;
struct InitializeData;
struct INI_Camera;
struct INI_Projection;

class StartupManager {

private:
//	INI_RenderTest ini_renderTest;
//	INI_Projection ini_projection;
//	std::map<unsigned char, INI_Camera> iniMap_Camera;
	InitializeData &initializeData;
	IScene * scene;
	SceneInitializer * sceneInitializer;


private:
	StartupManager();
	~StartupManager();

public:
	bool startup();
	
	IScene * getScene() { return scene; }
	const InitializeData& getInitializeData()const { return initializeData; }
	SceneInitializer* getSceneInitializer()const { return sceneInitializer; }
	const INI_Camera& getCamera(unsigned char id)const;
	const INI_Projection& getProjection()const;
//	const INI_RenderTest& getINI_RenderTest() { return ini_renderTest; }
//	const INI_Projection& getINI_Projection() { return ini_projection; }
//	const INI_Camera& getINI_Camera(unsigned char id) { return iniMap_Camera[id]; }


	static StartupManager* getInstance() {
		static StartupManager instance;
		return &instance;
	}

	std::string getRawDataFromCocos(const std::string &rsFileName);
};