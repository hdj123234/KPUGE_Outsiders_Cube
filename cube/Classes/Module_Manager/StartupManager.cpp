#include"stdafx.h"

#include "StartupManager.h"
#include"cocos2d.h"

#include"MyUtility_NEJ\MyDebugUtility.h"
#include"MyUtility_NEJ\MyIni.h"

#include "InGameScene.h"
#include"SceneInitializer.h"

#include"..\Types\Type_Component.h"
#include"..\Types\Type_Audio.h"
#include"Types\AssistantComponentData.h"

#include"math/Vec2.h"
#include"math/Vec3.h"
#include"math/Vec4.h"
#include<fstream>

#include"..\Types\InitializeData.h"
#include"Types\Type_Start.h"




StartupManager::StartupManager() 
	:scene(nullptr),
	sceneInitializer(nullptr),
	initializeData(*(new InitializeData()))
{
}

StartupManager::~StartupManager()
{
	if (scene)delete scene;
	if (sceneInitializer) delete sceneInitializer;
	delete &initializeData;
}

bool StartupManager::startup()
{
//	std::istream ifs(data.getBytes());
	
	;

	auto winSize = cocos2d::Director::getInstance()->getWinSize();
	initializeData.projection.aspectRatio = winSize.width / winSize.height;

	initializeData.cameraMap[0] = INI_Camera();	//디폴트 카메라

	initializeData.iniPath = MyDebugUtility::getInstance()->getINIPath();

	//ini로부터 루트 경로 읽기
	auto iniSection_Root = MyINIData::getDataFromRawData(getRawDataFromCocos( "cube.ini")).getSection();
	initializeData.iniPath = iniSection_Root.getParam("iniPath");
	if(!initializeData.iniPath.empty()&& initializeData.iniPath.back()=='/')
		initializeData.iniPath.push_back('/');
	initializeData.resourcePath =iniSection_Root.getParam("resourcePath");
	if(!initializeData.resourcePath.empty()&& initializeData.iniPath.back() == '/')
		initializeData.resourcePath.push_back('/');


	//ini로부터 배경색 읽기
	auto iniData_RenderTest = MyINIData::getDataFromRawData(getRawDataFromCocos(initializeData.iniPath + iniSection_Root.getParam("RenderINI")));

	MyINISection section_RenderTest = iniData_RenderTest.getSection();
	if (!section_RenderTest.isEmpty())
	{
		auto v4= section_RenderTest.getParam_Vector4("bgcolor");
		initializeData.renderTest.bgColor = cocos2d::Vec4(v4.x, v4.y, v4.z, v4.w);
		//		ini_renderTest.bCameraRotate = section_RenderTest.getParam_bool("cameraRotate");
		initializeData.renderTest.blockCount = section_RenderTest.getParam_uint("BlockCount");
	}
	//배경색 설정
	cocos2d::Color4F bgColor(initializeData.renderTest.bgColor.x
							, initializeData.renderTest.bgColor.y
							, initializeData.renderTest.bgColor.z
							, initializeData.renderTest.bgColor.w);
	cocos2d::Director::getInstance()->setClearColor(bgColor);

	//프로젝션
	MyINISection section_Projection = iniData_RenderTest.getSection("Projection");
	if (!section_Projection.isEmpty())
	{
		initializeData.projection.fov = section_Projection.getParam_float("FOV");
		initializeData.projection.nearZ = section_Projection.getParam_float("NearZ");
		initializeData.projection.farZ = section_Projection.getParam_float("FarZ");
	}

	//카메라
	auto sections_camera = iniData_RenderTest.getSection_Range("Camera");
	for (auto rSection : sections_camera)
	{
		INI_Camera camera;
		auto rotation = rSection.getParam_Vector2("rotationXY");
		camera.rotationXY[0] = rotation.x;
		camera.rotationXY[1] = rotation.y;
		camera.distance = rSection.getParam_float("Distance");
//		camera.distanceRate = rSection.getParam_float("DistanceRate");
		auto range = rSection.getParam_Vector2("DistanceRange");
		camera.distanceRange[0] = range.x;
		camera.distanceRange[1] = range.y;

		initializeData.cameraMap[static_cast<unsigned char>(getCubeType(rSection.getParam("Type")))] = camera;
	}


	//로고
	auto iniSection_Logo = MyINIData::getDataFromRawData(getRawDataFromCocos(initializeData.iniPath + iniSection_Root.getParam("LogoINI"))).getSection();
	if (!iniSection_Logo.isEmpty())
	{
		initializeData.logo.imageFile = iniSection_Logo.getParam("image");
		initializeData.logo.autoSkipTime= iniSection_Logo.getParam_float("autoSkipTime");

	}

	//타이틀
	auto iniData_Title = MyINIData::getDataFromRawData(getRawDataFromCocos(initializeData.iniPath + iniSection_Root.getParam("TitleINI")));
	if (!iniData_Title.isEmpty())
	{
		auto section_PuzzleType = iniData_Title.getSection("PuzzleType");
		{
			auto &section = section_PuzzleType;
			INI_TitleItem_Select state;
			state.buttonL_Normal = section.getParam("Button_L_Normal");
			state.buttonL_Press = section.getParam("Button_L_Press");
			state.buttonR_Normal = section.getParam("Button_R_Normal");
			state.buttonR_Press = section.getParam("Button_R_Press");
			state.buttonScaleX = section.getParam_float("ButtonScaleX");
			state.buttonGab = section.getParam_float("ButtonGab");
			state.itemScaleX= section.getParam_float("ItemScaleX");
			state.posY = section.getParam_float("PosY");
			auto s = section.getParam("AnchorY");
			state.anchorY = ComponentMaps::getAnchorY(s[0]);
			state.defaultItemID = static_cast<unsigned char>(StartOptionMaps::getPuzzleType(section.getParam("DefaultItem")));
			int n = 0;
			while (1)
			{
				++n;
				std::stringstream itemImage;
				std::stringstream itemID;
				itemID << "ItemID_"<<n;
				itemImage << "ItemImage_"<<n;
				auto str = section.getParam(itemID.str());
				if (str == "") break;
				state.items.insert(
					std::make_pair(static_cast<unsigned char>(StartOptionMaps::getPuzzleType(str)),
						section.getParam(itemImage.str()))
				);

			}
			initializeData.title.puzzle = state;
		}
		auto section_GameMode = iniData_Title.getSection("GameMode");
		{
			auto &section = section_GameMode;
			INI_TitleItem_Select state;
			state.buttonL_Normal = section.getParam("Button_L_Normal");
			state.buttonL_Press = section.getParam("Button_L_Press");
			state.buttonR_Normal = section.getParam("Button_R_Normal");
			state.buttonR_Press = section.getParam("Button_R_Press");
			state.buttonScaleX = section.getParam_float("ButtonScaleX");
			state.buttonGab = section.getParam_float("ButtonGab");
			state.itemScaleX = section.getParam_float("ItemScaleX");
			state.posY = section.getParam_float("PosY");
			auto s = section.getParam("AnchorY");
			state.anchorY = ComponentMaps::getAnchorY(s[0]);
			state.defaultItemID = static_cast<unsigned char>(StartOptionMaps::getModeType(section.getParam("DefaultItem")));
			int n = 0;
			while (1)
			{
				++n;
				std::stringstream itemImage;
				std::stringstream itemID;
				itemID << "ItemID_" << n;
				itemImage << "ItemImage_" << n;
				auto str = section.getParam(itemID.str());
				if (str == "") break;
				state.items.insert(
					std::make_pair(static_cast<unsigned char>(StartOptionMaps::getModeType(str)),
						section.getParam(itemImage.str()))
				);
			}
			initializeData.title.mode = state;
		}
		auto sections_Button = iniData_Title.getSection_Range("Button");
		for (auto &section : sections_Button)
//		for (auto i=1;;++i)
		{
//			auto section=iniData_Title.getSection("Button" + std::to_string(i));
			if (section.isEmpty())	break;
			
			INI_TitleItem_Button state;
			state.image1 = section.getParam("image1");
			state.image2 = section.getParam("image2");
			state.scaleX = section.getParam_float("scaleX");
			auto s = section.getParam("AnchorY");
			state.anchorY = ComponentMaps::getAnchorY(s[0]);
			state.posX = section.getParam_float("PosX");
			state.posY = section.getParam_float("PosY");

			initializeData.title.buttons.push_back(state);
		}
		auto v = iniData_Title.getSection("Buttons").getParam_Vector2("pos");
		initializeData.title.posNorm = cocos2d::Vec2(v.x, v.y);

		//BackButton
		{
			auto section= iniData_Title.getSection("BackButton");
			auto &data = initializeData.title.backButton;
			data.image = section.getParam("Image");
			auto s = section.getParam("anchor");
			data.anchorX = ComponentMaps::getAnchorX(s[0]);
			data.anchorY = ComponentMaps::getAnchorY(s[1]);
			auto v2 = section.getParam_Vector2("Pos");
			data.pos = cocos2d::Vec2(v2.x, v2.y);
			data.scaleX = section.getParam_float("ScaleX");
			data.opacity = section.getParam_float("Opacity");
			data.timeOut = section.getParam_float("TimeOut");
		}
		//TitleImage
		{
			auto section = iniData_Title.getSection("TitleImage");
			auto &data = initializeData.title.titleImage;
			data.image = section.getParam("Image");
			auto s = section.getParam("anchor");
			data.anchorX = ComponentMaps::getAnchorX(s[0]);
			data.anchorY = ComponentMaps::getAnchorY(s[1]);
			auto v2 = section.getParam_Vector2("Pos");
			data.pos = cocos2d::Vec2(v2.x, v2.y);
			data.scaleX = section.getParam_float("ScaleX");
		}

	}

	//AssistantComponent
	auto iniData_AC = MyINIData::getDataFromRawData(getRawDataFromCocos(initializeData.iniPath + iniSection_Root.getParam("AssistantComponentINI")));
	auto iniSections_AC = iniData_AC.getSection_Range("Component");
	auto iniSection_ZButton = iniData_AC.getSection("ZButton");
	float zButtonBGSize = iniSection_ZButton.getParam_float("size");
	float zButtonSize = iniSection_ZButton.getParam_float("ButtonSize");
	std::string zButtonBGImage= iniSection_ZButton.getParam("BGImage");
	float zButtonBGOpacity = iniSection_ZButton.getParam_float("BGOpacity");

	for (auto &data : iniSections_AC)
	{
		AssistantComponentData * tmp = nullptr;
		auto category = ComponentMaps::getComponentCategory(data.getParam("Type"));
		auto type = ComponentMaps::getComponentType(data.getParam("id"));
		switch (category)
		{
		case ComponentCategory::Button:
		{
			auto p = new ACData_Button();
			auto &rTarget = (*p);
			tmp = p;

			auto s = data.getParam("anchor");
			rTarget.image1 = data.getParam("Image1");
			rTarget.image2 = data.getParam("Image2");
			rTarget.anchor_x = ComponentMaps::getAnchorX(s[0]);
			rTarget.anchor_y = ComponentMaps::getAnchorY(s[1]);
			auto v2 = data.getParam_Vector2("pos");
			rTarget.pos =cocos2d::Vec2( v2.x,v2.y);
			rTarget.scale = data.getParam_float("scaleX");
			break;
		}
		case ComponentCategory::Label:
		{
			auto p = new ACData_Label();
			auto &rTarget = (*p);
			tmp = p;

			auto s = data.getParam("anchor");
			rTarget.image = data.getParam("Image");
			rTarget.anchor_x = ComponentMaps::getAnchorX(s[0]);
			rTarget.anchor_y = ComponentMaps::getAnchorY(s[1]);
			auto v2 = data.getParam_Vector2("pos");
			rTarget.pos = cocos2d::Vec2(v2.x, v2.y);
			rTarget.scale = data.getParam_float("scaleX");
			rTarget.opacity = data.getParam_float("Opacity");
			break;
		}
		case ComponentCategory::Text:
		{
			auto p = new ACData_Text();
			auto &rTarget = (*p);
			tmp = p;

			auto s = data.getParam("anchor");
			rTarget.anchor_x = ComponentMaps::getAnchorX(s[0]);
			rTarget.anchor_y = ComponentMaps::getAnchorY(s[1]);
			auto v2 = data.getParam_Vector2("pos");
			rTarget.pos = cocos2d::Vec2(v2.x, v2.y);
			v2 = data.getParam_Vector2("TextBoxSize");
			rTarget.textBoxSize= cocos2d::Vec2(v2.x, v2.y);
			rTarget.fontSize = data.getParam_float("fontSize");
			auto v3=data.getParam_Vector3("color");
			rTarget.color = cocos2d::Vec3(v3.x, v3.y, v3.z);
			rTarget.fontFile= data.getParam("FontFile");
			break;
		}
		case ComponentCategory::ClearLabel:
		{
			auto p = new ACData_ClearLabel();
			auto &rTarget = (*p);
			tmp = p;
			std::string s;
			rTarget.image = data.getParam("Image");
			s = data.getParam("anchor");
			rTarget.anchor_x = ComponentMaps::getAnchorX(s[0]);
			rTarget.anchor_y = ComponentMaps::getAnchorY(s[1]);

			s = data.getParam("textanchor");
			rTarget.textAnchor_x = ComponentMaps::getAnchorX(s[0]);
			rTarget.textAnchor_y = ComponentMaps::getAnchorY(s[1]);
			auto v2 = data.getParam_Vector2("pos");
			rTarget.pos = cocos2d::Vec2(v2.x, v2.y);
			v2 = data.getParam_Vector2("TextPos");
			rTarget.textPos = cocos2d::Vec2(v2.x,v2.y);
			v2 = data.getParam_Vector2("TextBoxSize");
			rTarget.textBoxSize = cocos2d::Vec2(v2.x, v2.y);
			rTarget.fontSize = data.getParam_float("fontSize");
			auto v3 = data.getParam_Vector3("textcolor");
			rTarget.color = cocos2d::Vec3(v3.x, v3.y, v3.z);
			rTarget.scale = data.getParam_float("scaleX");
			rTarget.fontFile = data.getParam("FontFile");
			rTarget.opacity = data.getParam_float("opacity");

			s = data.getParam("subImageAnchor");
			rTarget.subImage = data.getParam("subImage");
			rTarget.subImageAnchor_x = ComponentMaps::getAnchorX(s[0]);
			rTarget.subImageAnchor_y = ComponentMaps::getAnchorY(s[1]);
			v2 = data.getParam_Vector2("subImagePos");
			rTarget.subImagePos.x = v2.x;
			rTarget.subImagePos.y = v2.y;
			rTarget.subImageScale = data.getParam_float("subImageScaleX");

			break;
		}
		case ComponentCategory::ZButton:
		{
			auto p = new ACData_ZButton();
			auto &rTarget = (*p);
			tmp = p;

			rTarget.image1 = data.getParam("Image1");
			rTarget.image2 = data.getParam("Image2"); 
			rTarget.buttonOpacity = data.getParam_float("Opacity");
			rTarget.bg = zButtonBGImage;
			rTarget.scrollSizeX = zButtonBGSize;
			rTarget.bgOpacity = zButtonBGOpacity;
			rTarget.buttonSizeRate = zButtonSize;
			break;
		}
		case ComponentCategory::CheckBox:
		{
			auto p = new ACData_CheckBox();
			auto &rTarget = (*p);
			tmp = p;

			rTarget.labelImage = data.getParam("LabelImage");
			auto s = data.getParam("LabelAnchor");
			rTarget.labelAnchor_x= ComponentMaps::getAnchorX(s[0]);
			rTarget.labelAnchor_y = ComponentMaps::getAnchorY(s[1]);
			auto v2 = data.getParam_Vector2("LabelPos");
			rTarget.labelPos.x = v2.x;
			rTarget.labelPos.y = v2.y;
			rTarget.labelScale = data.getParam_float("LabelScaleX");

			rTarget.checkBoxImage1 = data.getParam("CheckBoxImage_Normal");
			rTarget.checkBoxImage2 = data.getParam("CheckBoxImage_Check");
			s = data.getParam("CheckBoxAnchor");
			rTarget.checkBoxAnchor_x = ComponentMaps::getAnchorX(s[0]);
			rTarget.checkBoxAnchor_y = ComponentMaps::getAnchorY(s[1]);
			v2 = data.getParam_Vector2("CheckBoxPos");
			rTarget.checkBoxPos.x = v2.x;
			rTarget.checkBoxPos.y = v2.y;
			rTarget.checkBoxScale = data.getParam_float("CheckBoxScaleX");
			break;
		}
		}
		if (tmp)
		{
			initializeData.assistantComponents.push_back(tmp);

			tmp->category = category;
			tmp->type = type;
		}
	}
	//Audio
	auto iniSections_Audio = MyINIData::getDataFromRawData(getRawDataFromCocos(initializeData.iniPath + iniSection_Root.getParam("AudioINI"))).getSection();
	auto audioParamNameMap = std::move(getParamNameMap());
	auto nType = static_cast<unsigned char>(Type_Audio::_LastFlag);
	for (unsigned char i = 0; i < nType; ++i)
	{
		auto type = static_cast<Type_Audio>(i);
		auto paramName = audioParamNameMap[type];
		auto path = iniSections_Audio.getParam(paramName);
		initializeData.audio.fileMap[type] = path;
	}

	//크레딧
	auto iniData_Credit = MyINIData::getDataFromRawData(getRawDataFromCocos(initializeData.iniPath + iniSection_Root.getParam("CreditINI")));
	if (!iniData_Credit.isEmpty())
	{
		auto section_Image = iniData_Credit.getSection("Image");
		initializeData.credit.imageFile = section_Image.getParam("Image");
		initializeData.credit.imageScaleX= section_Image.getParam_float("ScaleX");
		initializeData.credit.scrollTime= section_Image.getParam_float("ScrollTime");
		
		auto section_Button = iniData_Credit.getSection("Button");
		initializeData.credit.buttonImage1 = section_Button.getParam("Image1");
		initializeData.credit.buttonImage2 = section_Button.getParam("Image2");
		auto s = section_Button.getParam("Anchor");
		initializeData.credit.buttonAnchor_x = ComponentMaps::getAnchorX(s[0]);
		initializeData.credit.buttonAnchor_y = ComponentMaps::getAnchorY(s[1]);
		auto v2 = section_Button.getParam_Vector2("pos");
		initializeData.credit.buttonPos = cocos2d::Vec2(v2.x, v2.y);
		initializeData.credit.buttonScaleX = section_Button.getParam_float("ScaleX");

	}


	scene = new InGameScene();
	sceneInitializer = new SceneInitializer();

	return true;
}

const INI_Camera & StartupManager::getCamera(unsigned char id) const
{
	auto iter = initializeData.cameraMap.find(id);
	if (iter == initializeData.cameraMap.end())
		return initializeData.cameraMap[0];
	else
		return iter->second;

}

const INI_Projection & StartupManager::getProjection() const
{
	return initializeData.projection;
}


std::string StartupManager::getRawDataFromCocos(const std::string & rsFileName)
{
	auto data = cocos2d::FileUtils::getInstance()->getDataFromFile(rsFileName);
	return std::string(data.getBytes(), data.getBytes() + data.getSize());
}
