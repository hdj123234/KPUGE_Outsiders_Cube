#pragma once

class ITwistyPuzzleModel;
enum class Type_Puzzle :unsigned char;
enum class Type_Audio:unsigned char;
enum Enum_Resource : unsigned char;
class ICCNode;
class ICCAudio;

class CCResourceManager {
private:
	std::map<Type_Puzzle, ITwistyPuzzleModel*> puzzleModelMap;
	std::map<Type_Audio, ICCAudio*> audioMap;

public:
	bool initialize();

	ITwistyPuzzleModel * getTwistyPuzzle(Type_Puzzle type);
	std::vector<ICCNode *> getInGameSceneComponents();
	ICCAudio* getAudio(Type_Audio type);


	static CCResourceManager* getInstance() {
		static CCResourceManager instance;
		return &instance;
	}

private:
	CCResourceManager();
	~CCResourceManager();


};