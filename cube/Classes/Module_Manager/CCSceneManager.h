#pragma once

#include"..\MyFunctor.h"

class InGameScene_CC;
class TinyScene_CC;
class InitializeState;
class CCTitleItem_Select;
class CCSimpleComponent;
namespace cocos2d {
	class Scene;
	class Ref;
}

class GameStarter : public IMyFunctor {
private:
	cocos2d::Scene *inGameScene;
	CCTitleItem_Select *modeSelector;
	CCTitleItem_Select *puzzleSelector;

public:
	GameStarter(cocos2d::Scene *inGameScene,
		CCTitleItem_Select *modeSelector,
		CCTitleItem_Select *puzzleSelector)
		:inGameScene(inGameScene),
		modeSelector(modeSelector),
		puzzleSelector(puzzleSelector) {}
	~GameStarter() {}
	void operator()(cocos2d::Ref*) { operator() (); }
	void operator()();
};

class CCSceneManager {
private:
	struct CreditScrollData {
		float creditPosY;
		float creditPosY_Max;
		float creditPosY_Tick;
	};
	struct BackButtonData {
		float timeCount;
		float timeOut;
		bool bOnce;
		CCSimpleComponent *backButtonLabel;
	};
private:
	InGameScene_CC * inGameScene;
	cocos2d::Scene * inGameScene_CC;
	cocos2d::Scene *startScene;
	cocos2d::Scene *titleScene;
	cocos2d::Scene *creditScene;
	GameStarter * gameStarter;
	CreditScrollData creditScrollData;
	BackButtonData titleBackButtonData;

//	InitializeState & sceneInitializeState;

//	std::vector<CCSceneMover> sceneMovers;

private:
	CCSceneManager();
	~CCSceneManager();

public:
	bool initialize();

	cocos2d::Scene * getInGameScene();
	cocos2d::Scene * getStartScene() { return startScene;  	}


	static CCSceneManager* getInstance() {
		static CCSceneManager instance;
		return &instance;
	}

};