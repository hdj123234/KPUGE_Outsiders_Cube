
#include"stdafx.h"

#include "CCSceneManager.h" 

//USING_NS_CC;

#include"MyUtility_NEJ\MyDebugUtility.h"
#include"MyUtility_NEJ\MyINI.h"

#include"StartupManager.h"

#include"..\CC_Scene\InGameScene_CC.h"
#include"..\CC_Scene\TinyScene_CC.h"
//#include "cocos2d.h"

#include"..\InitializeState.h"
#include"..\EnumDefine.h"

#include"..\Module_CC\CCComponent.h"
#include"..\Types\Type_Audio.h"
#include"..\Types\Type_Component.h"
#include"CCResourceManager.h"
#include"..\Types\InitializeData.h"
#include"..\MyFunctor.h"
#include"InitializeState.h"

 

void GameStarter::operator()()
{
	InitializeState::getInstance()->setState(
		static_cast<Type_GameMode>(modeSelector->getSelectedItemID()),
		static_cast<Type_Puzzle>(puzzleSelector->getSelectedItemID())
	);
	cocos2d::Director::getInstance()->pushScene(inGameScene);

}



CCSceneManager::CCSceneManager() 
	: inGameScene(nullptr), startScene(nullptr), titleScene(nullptr), creditScene(nullptr)
	//,
	//sceneInitializeState(*(new InitializeState()))
{
	auto sceneInitializeState = InitializeState::getInstance();
	sceneInitializeState->setState(Type_GameMode::Auto, Type_Puzzle::RubiksCube);

	
	//sceneInitializeState.gameMode=Type_GameMode::Auto;
	//sceneInitializeState.puzzleType = Type_Puzzle::RubiksCube;
	//sceneInitializeState.pickableObjectPtr=nullptr;
}

CCSceneManager::~CCSceneManager()
{
//	delete &sceneInitializeState;
}

bool CCSceneManager::initialize()
{
	using namespace cocos2d;
	auto winSize = Director::getInstance()->getWinSize();

	//if (!inGameScene) inGameScene = static_cast<InGameScene_CC*>(InGameScene_CC::createScene(&sceneInitializeState));
	if (!inGameScene) inGameScene = static_cast<InGameScene_CC*>(InGameScene_CC::createScene());
	inGameScene->retain();
	inGameScene_CC = inGameScene;

	auto &ini = StartupManager::getInstance()->getInitializeData();
	//로고씬
	if (!startScene) {
		auto &iniData = ini.logo;
		TinySceneData sceneData;

		//리스너
		auto touchListener = cocos2d::EventListenerTouchOneByOne::create();
		touchListener->retain();
		touchListener->onTouchBegan = 
			[=](const cocos2d::Touch* touch, cocos2d::Event * ev)->bool
		{
			cocos2d::Director::getInstance()->replaceScene(titleScene);
			return true;	
		};
		sceneData.listeners.push_back(touchListener);
		//노드
		auto sprite = cocos2d::Sprite::create(ini.resourcePath+ iniData.imageFile);
		sprite->setPositionNormalized(Vec2(0.5, 0.5));
		auto logoSize = sprite->getContentSize();
		auto winSize = Director::getInstance()->getWinSize();
		float scale = logoSize.width/ winSize.width>logoSize.height / winSize.height ? winSize.width / logoSize.width : winSize.height / logoSize.height;
		sprite->setScale(scale);
		sceneData.nodes.push_back(new CCSimpleComponent( sprite));

		ScheduleData scheduleData;
		scheduleData.bOnce = true;
		scheduleData.callback = [=](float) {cocos2d::Director::getInstance()->replaceScene(titleScene); };
		scheduleData.delay = iniData.autoSkipTime;
		scheduleData.key = "LogoFunc";
		sceneData.schedules.push_back(scheduleData);
		sceneData.bgm = CCResourceManager::getInstance()->getAudio(Type_Audio::Logo_BGM);
		startScene = TinyScene_CC::createScene(sceneData);
	}
	//크레딧
	if (!creditScene) {
		auto &iniData = ini.credit;
		TinySceneData sceneData;

		//노드
		auto sprite = cocos2d::Sprite::create(ini.resourcePath + iniData.imageFile);
		sprite->setAnchorPoint(Vec2(0.5, 1));
		sprite->setPositionNormalized(Vec2(0.5,0));
		auto creditSize = sprite->getContentSize();
		auto winSize = Director::getInstance()->getWinSize();
		auto imageScale = iniData.imageScaleX* winSize.width / creditSize.width;
		sprite->setScale(imageScale);
		sceneData.nodes.push_back(new CCSimpleComponent(sprite));
		creditScrollData.creditPosY_Max = creditSize.height*imageScale / winSize.height+1;
		creditScrollData.creditPosY = creditScrollData.creditPosY_Max + 1;
		creditScrollData.creditPosY_Tick = creditScrollData.creditPosY_Max / iniData.scrollTime *0.016;
		auto buttonItem = MenuItemImage::create(ini.resourcePath + iniData.buttonImage1, ini.resourcePath + iniData.buttonImage2, CCSceneMover(nullptr));
		auto button = cocos2d::Menu::create();
		button->addChild(buttonItem);
		buttonItem->setAnchorPoint(getAnchor(iniData.buttonAnchor_x, iniData.buttonAnchor_y));
		buttonItem->setScale(iniData.buttonScaleX* winSize.width / buttonItem->getContentSize().width);
		button->setPositionNormalized(iniData.buttonPos);
		sceneData.nodes.push_back(new CCSimpleComponent(button));

		//스케쥴
		{
			ScheduleData scheduleData;
			scheduleData.bOnce = true;
			scheduleData.callback = [=](float)
			{
				auto &data = creditScrollData;
				auto image = sprite;
				data.creditPosY = 0;
				image->setPositionNormalized(Vec2(0.5, data.creditPosY));
			};
			scheduleData.interval = 0;
			scheduleData.delay = 0;

			scheduleData.key = "CreditInitFunc";
			sceneData.schedules.push_back(scheduleData);
		}

		{
			ScheduleData scheduleData;
			scheduleData.bOnce = false;
			scheduleData.callback = [=](float)
			{
				auto &data = creditScrollData;
				auto image = sprite;
				if (data.creditPosY > data.creditPosY_Max)
					data.creditPosY = 0;
				data.creditPosY += data.creditPosY_Tick;
				image->setPositionNormalized(Vec2(0.5, data.creditPosY));
			};
			scheduleData.interval = 0;
			scheduleData.repeat = CC_REPEAT_FOREVER;
			scheduleData.delay = 0.016;

			scheduleData.key = "CreditFunc";
			sceneData.schedules.push_back(scheduleData);
		}
		//리스너
		auto backButtonListener = EventListenerKeyboard::create();
		backButtonListener->onKeyPressed = [=](EventKeyboard::KeyCode key, Event *ev) {
			if (key == EventKeyboard::KeyCode::KEY_ESCAPE)
			{
				Director::getInstance()->popScene();
			}
		};
		backButtonListener->retain();
		sceneData.listeners.push_back(backButtonListener);
		//sceneData.bgm = CCResourceManager::getInstance()->getAudio(Type_Audio::Logo_BGM);
		creditScene = TinyScene_CC::createScene(sceneData);
	}

	//타이틀씬
	if (!titleScene) {
		auto &iniData = ini.title;
		TinySceneData sceneData;
		//타이틀 이미지
		{
			auto &data = iniData.titleImage;
			auto sprite = Sprite::create(data.image);
			sprite->setPositionNormalized(data.pos);
			sprite->setScale(data.scaleX*winSize.width / sprite->getContentSize().width);
			auto anchor = getAnchor(data.anchorX, data.anchorY);
			sprite->setAnchorPoint(anchor);
			titleBackButtonData.backButtonLabel = new CCSimpleComponent(sprite);
			sceneData.nodes.push_back(titleBackButtonData.backButtonLabel);
		}

			//게임모드
		auto modeSelector = new CCTitleItem_Select(iniData.mode);
		auto puzzleSelector = new CCTitleItem_Select(iniData.puzzle);

		sceneData.nodes.push_back(modeSelector);
		sceneData.nodes.push_back(puzzleSelector);

		auto nButton = iniData.buttons.size();
//		sceneMovers.reserve(nButton);
			//버튼
		for (int i=0;i<nButton;++i)
		{
			auto &data = iniData.buttons[i];
			cocos2d::Scene * moveTarget=nullptr;
			switch (i+1)
			{
			case 1:
				moveTarget = inGameScene;
				break;
			case 2:
				moveTarget = creditScene;
				break;
			}
			if (i == 0)
			{
				gameStarter = new GameStarter(inGameScene, modeSelector, puzzleSelector);
				MyFunctor f = MyFunctor(*gameStarter);
				sceneData.nodes.push_back(new CCTitleItem_Button(data, f));
			}
			else
				sceneData.nodes.push_back(new CCTitleItem_Button(data, moveTarget));
		}
		//백버튼 레이블
		{
			auto &data = iniData.backButton;
			auto sprite = Sprite::create(data.image);
			sprite->setScale(data.scaleX*winSize.width / sprite->getContentSize().width);
			sprite->setPositionNormalized(data.pos);
			auto anchor = getAnchor(data.anchorX, data.anchorY);
			sprite->setAnchorPoint(anchor);
			sprite->setOpacity(data.opacity);
			sprite->setVisible(false);
			titleBackButtonData.backButtonLabel = new CCSimpleComponent(sprite);
			sceneData.nodes.push_back(titleBackButtonData.backButtonLabel);
			titleBackButtonData.timeOut = data.timeOut;
		}
		//리스너
		auto backButtonListener = EventListenerKeyboard::create();
		backButtonListener->retain();
		backButtonListener->onKeyPressed = [=](EventKeyboard::KeyCode key, Event *ev) {
			if (key == EventKeyboard::KeyCode::KEY_ESCAPE)
			{
				auto &data = titleBackButtonData;
				if (!data.bOnce)
				{
					data.bOnce = true;
					data.timeCount = 0;
					data.backButtonLabel->show();
				}
				else
				{
					if (data.timeCount < data.timeOut)
						Director::getInstance()->popScene();
					else
						data.timeCount = 0;
				}
			}
		};
		sceneData.listeners.push_back(backButtonListener);
		//스케줄
		ScheduleData scheduleData;
		scheduleData.bOnce = false;
		scheduleData.callback = [=](float f)
		{
			auto &data = titleBackButtonData;
			if (data.timeCount > data.timeOut)
			{
				data.backButtonLabel->hide();
				data.bOnce = false;
				data.timeCount = 0;
			}
			else if (data.bOnce)
				data.timeCount += f;
		};
		scheduleData.interval = 0;
		scheduleData.repeat = CC_REPEAT_FOREVER;
		scheduleData.delay = 0;

		scheduleData.key = "CreditFunc";
		sceneData.schedules.push_back(scheduleData);

		sceneData.bgm = CCResourceManager::getInstance()->getAudio(Type_Audio::Title_BGM);
	
	
		titleScene = TinyScene_CC::createScene(sceneData);
	}
	 
	return true;
}

cocos2d::Scene * CCSceneManager::getInGameScene()
{
	//추후 삭제
//	return SceneTestScene::create();

	return inGameScene; 
}
