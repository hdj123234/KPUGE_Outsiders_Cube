#pragma once

#include"EnumDefine.h"
#include"..\Types\Type_Audio.h"

enum Enum_Resource : unsigned char {
	CubeModel_222,
	CubeModel_333,
	CubeModel_444,
	CubeModel_555,

	BGM_Main	,
	BGM2		,
	BGM3		,
	SE_Spin,
	SE_Spin1,
	SE_Spin2,
	SE_Spin3,
	SE_Spin4,
	SE_Spin5,
	SE_Spin6,
	SE_Spin7,
	SE_Clear,
	Logo_BGM,
	Title_BGM,
};


class ResourceEnumerationManager {
public:
	static ID_Resource getID(Type_Puzzle type) 
	{
		static std::map<Type_Puzzle, ID_Resource> idMap_PuzzleType{
			std::make_pair(Type_Puzzle::RubiksCube,Enum_Resource::CubeModel_333),
			std::make_pair(Type_Puzzle::Square2x2x2Cube,Enum_Resource::CubeModel_222),
			std::make_pair(Type_Puzzle::Square4x4x4Cube,Enum_Resource::CubeModel_444),
			std::make_pair(Type_Puzzle::Square5x5x5Cube,Enum_Resource::CubeModel_555),
		};
		return idMap_PuzzleType[type]; 
	}
	static ID_Resource getID(Type_Audio type) {
		static std::map<Type_Audio, ID_Resource> idMap_Audio{
			std::make_pair(Type_Audio::BGM_Main ,Enum_Resource::BGM_Main),
			std::make_pair(Type_Audio::BGM2,Enum_Resource::BGM2),
			std::make_pair(Type_Audio::BGM3,Enum_Resource::BGM3),
			std::make_pair(Type_Audio::SE_Spin, Enum_Resource::SE_Spin),
			std::make_pair(Type_Audio::SE_Spin1, Enum_Resource::SE_Spin1),
			std::make_pair(Type_Audio::SE_Spin2, Enum_Resource::SE_Spin2),
			std::make_pair(Type_Audio::SE_Spin3, Enum_Resource::SE_Spin3),
			std::make_pair(Type_Audio::SE_Spin4, Enum_Resource::SE_Spin4),
			std::make_pair(Type_Audio::SE_Spin5, Enum_Resource::SE_Spin5),
			std::make_pair(Type_Audio::SE_Spin6, Enum_Resource::SE_Spin6),
			std::make_pair(Type_Audio::SE_Spin7, Enum_Resource::SE_Spin7),
			std::make_pair(Type_Audio::SE_Clear,Enum_Resource::SE_Clear),
			std::make_pair(Type_Audio::Logo_BGM,Enum_Resource::Logo_BGM),
			std::make_pair(Type_Audio::Title_BGM,Enum_Resource::Title_BGM),
		};
		return idMap_Audio[type];
	}
};
