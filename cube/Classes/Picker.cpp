#include "stdafx.h"
#include "Picker.h"
#include"ISpace.h"
#include"ICamera.h"
#include"IPickableObject.h"
#include"Module_CC\PickingData.h"
#include"Module_Manager\StartupManager.h"

#include"MyUtility_NEJ\MySimpleVector.h"
#include"math/Vec3.h"
#include"math/Mat4.h"
#include"Types\InitializeData_Camera.h"

void Picker::setSpace(ISpace* _space)
{
	space = _space;
}

PickingResult Picker::pick(float posX, float posY)
{
	using namespace cocos2d;
	// 피킹
	//~!@#$%
	
	//1 레이 생성
	//계산 편이를 위해 룩벡터를 실제 보는방향으로 변경
	Ray ray;
	auto cameraMatrix = space->getCamera()->getMatrix();
	auto proj = StartupManager::getInstance()->getProjection();
	float fov = proj.fov;
	float aspectRatio = proj.aspectRatio;
	ray.startPoint = Vec3(cameraMatrix.m[12], cameraMatrix.m[13], cameraMatrix.m[14]);
	auto cameraR = Vec3(&cameraMatrix.m[0]);
	auto cameraU = Vec3(&cameraMatrix.m[4]);
	auto cameraL = Vec3(&cameraMatrix.m[8]);
	auto cameraPos = Vec3(&cameraMatrix.m[12]);
	auto camera= Mat4(	cameraMatrix.m[0], cameraMatrix.m[4], cameraMatrix.m[8], 0,
						cameraMatrix.m[1], cameraMatrix.m[5],cameraMatrix.m[9], 0,
						cameraMatrix.m[2], cameraMatrix.m[6], cameraMatrix.m[10], 0,
						-Vec3::dot(cameraR, cameraPos), 
						-Vec3::dot(cameraU, cameraPos), 
						-Vec3::dot(cameraL, cameraPos), 1);
//	auto cameraD = XMMatrixLookAtRH(XMVectorSet(cameraPos.x, cameraPos.y, cameraPos.z,1), XMVectorZero(), XMVectorSet(cameraU.x, cameraU.y, cameraU.z,0));
	auto uncamera = camera.getInversed();
	uncamera.transpose();
//	auto uncameraD= XMMatrixInverse(NULL, cameraD);
	Mat4 unproj;
	Mat4::createPerspective((proj.fov), proj.aspectRatio, proj.nearZ, proj.farZ,&unproj);
//	auto unprojD = DirectX::XMMatrixPerspectiveFovRH(XMConvertToRadians(proj.fov), proj.aspectRatio, proj.nearZ, proj.farZ);
	unproj.inverse();
//	unprojD= XMMatrixInverse(NULL, unprojD);

//	unproj.transpose();
	Vec4 posProjection(posX * 2 - 1, posY * 2 - 1,1,1);
//	posProjection = unproj*posProjection;
//	auto posT = XMVector4Transform( XMVectorSet(posProjection.x, posProjection.y, posProjection.z, posProjection.w), unprojD) ;
//	auto posT = XMVector4Transform( XMVectorSet(posProjection.x, posProjection.y, posProjection.z, posProjection.w),uncameraD) ;
	unproj.transformVector(&posProjection);
	posProjection.w = 1;
	uncamera.transformVector( &posProjection);

	auto pos = Vec3(posProjection.x, posProjection.y, posProjection.z);
	ray.dir = (pos - ray.startPoint).getNormalized();

	//2 대상 객체 세팅
//	auto puzzleType=space->getPuzzleType();

	std::vector<std::pair<float, Target>> targets;

	//
	auto obj=space->getObjects()[0];
	auto pickingData = obj->getPickingTargetData();
	auto size = pickingData->blocks.size();
	targets.reserve(size);
	for (auto i=0U;i<size;++i)
	{
		for (auto data : pickingData->blocks[i])
		{
			//타겟 폴리곤 설정
			Target tmp;
			tmp.posID = i;
			tmp.faceID= ID_Face(data.first+1);
			auto numberOfVertex = data.second.size();
			Vec3 center=Vec3(0,0,0) ;
			for (auto v : data.second)
			{
				tmp.polygon.push_back(v);
				center += v;
			}

			//거리 계산
			center= center/numberOfVertex;
			float distance_square=(ray.startPoint - center).lengthSquared();

			
			targets.push_back(std::make_pair(distance_square, tmp));
		}
	}

	//3 피킹 실행 및 결과 반환
	std::vector<std::pair<int, float>> targetIDs;
	for (int i = 0; i < targets.size(); ++i)
	{
		PickingResult result;
		result.bSuccess = false;
		if (isCollided(ray, targets[i].second))
		{
			targetIDs.push_back(std::make_pair(i, targets[i].first));
		}
	}
	PickingResult result;
	result.bSuccess = false;
	if (targetIDs.size() == 0) return result;

	std::sort(targetIDs.begin(), targetIDs.end(),
		[](const std::pair<int, float> &obj1, const  std::pair<int, float> &obj2)
	{
		return obj1.second < obj2.second;
	});

	result.bSuccess = true;
	result.result.faceID = ID_Face(targets[targetIDs[0].first].second.faceID);
	result.result.posID = targets[targetIDs[0].first].second.posID;

	return result;
}

bool Picker::isCollided(Ray & ray, Target & target)
{
	using namespace cocos2d;
	Vec4 plane = getPlaneFromPoints(target.polygon[0],target.polygon[1],target.polygon[2]);
	auto rayDir = ray.dir;
//
	//면의 방향과 카메라 방향이 마주보고 있지 않을경우
	if (Vec3::dot(-rayDir, Vec3(plane.x, plane.y, plane.z)) < 0)
		return false;
//
	auto rayPoint1 = ray.startPoint;
//	//나중에 최적화
	auto rayPoint2 = rayPoint1 + 100 * rayDir;	//임의의 길이(1000)를 통해 3차원 상 선분의 끝점 지정
//	DirectX::XMFLOAT3 bNan;
	auto collition = getCollistionPoint(plane, rayPoint1, rayPoint2);
	if (!collition.bCollide)	return false;
	Vec3 intersectionPoint = collition.point;
//
//	//해당 충돌점이 평면상의 폴리곤 안에 있는지 확인
//	//폴리곤을 구성하는 인접한 두개의 점과 충돌지점으로 만들어진 각을 통해 계산
//	//모든 각도의 합이 360도(2파이)일 경우 폴리곤 안의 점
	float fDegree = 0;
	auto nNumberOfVertex = target.polygon.size();
	Vec3 v0;
	Vec3 v1 = v0 = target.polygon[0] - intersectionPoint;
	Vec3 v2;
	Vec3 *pv1 = &v1;
	Vec3 *pv2 = &v2;
	Vec3 *pvTmp;
	for (auto i = 1U; i<nNumberOfVertex; ++i)
	{
		*pv2 = target.polygon[i] - intersectionPoint;
		fDegree += getAngleBetweenVectors(v1, v2);
		pvTmp = pv1;		pv1 = pv2;		pv2 = pvTmp;
	}
	v1 = target.polygon.back() - intersectionPoint;
	fDegree += getAngleBetweenVectors(v0, v1);
	fDegree -= (M_PI * 2);
//
	//fDegree의 절대값을 통해 0의 오차범위에 포함되는지 확인
	auto tmp = std::abs(fDegree);
	if (tmp <= 0.000086)
	{
//		cocos2d::log("Picking Successed");
//		cocos2d::log((std::string("Pos ID = ") + std::to_string(target.posID)).c_str());
//		cocos2d::log((std::string("Face ID = ") + std::to_string(target.faceID)).c_str());
//		cocos2d::log(" ");
		return true;
	}
	else
		return false;
}

cocos2d::Vec4 Picker::getPlaneFromPoints(cocos2d::Vec3 p1, cocos2d::Vec3 p2, cocos2d::Vec3 p3)
{
	using namespace cocos2d;
	//getPlaneFromPoints 함수
	//출처 : MSDN
	//XMVECTOR Result;
	//XMVECTOR N;
	//XMVECTOR D;

	//XMVECTOR V21 = XMVectorSubtract(Point1, Point2);
	//XMVECTOR V31 = XMVectorSubtract(Point1, Point3);

	//N = XMVector3Cross(V21, V31);
	//N = XMVector3Normalize(N);

	//D = XMPlaneDotNormal(N, Point1);

	//Result.x = N.x;
	//Result.y = N.y;
	//Result.z = N.z;
	//Result.w = -D.w;

	//return Result;

	Vec3 v21 = p1-p2;
	Vec3 v31 = p1-p3;
	Vec3 n;
	Vec3::cross(v21, v31,&n);
	float d = Vec3::dot(n, p1);

	return Vec4(n.x,n.y,n.z,-d);
}

Picker::CollisionPoint Picker::getCollistionPoint(cocos2d::Vec4 plane, cocos2d::Vec3 p1, cocos2d::Vec3 p2)
{
	//Dir은 P1-P0로 가정
	//(A,B,C)*Dir == 일 경우 평면의 법선벡터와 선분은 직교 => 충돌하지 않음

	//Ax+By+Cz +D = 0   --- (1)
	//P=(x,y,z)=P0+tDir --- (2)
	// 두 식을 연립하여 매개변수 t를 계산
	//t = (-D -(A,B,C)*P0)/(A,B,C)*Dir)
	//t를 (2)에 대입하여 접점을 구함
	//Dir은 P1-P0 이므로 0<t<1 이라면 해당 선분과 충돌

	using namespace cocos2d;

	Picker::CollisionPoint retval;
	retval.bCollide = false;

	Vec3 planeNormal = Vec3(plane.x, plane.y, plane.z);
	Vec3 dir = p2 - p1;

	//선분과 평면의 직교여부를 확인
	float fTmp = Vec3::dot(planeNormal, dir);
	if (fTmp == 0)		return retval;

	//t를 계산
	float t = (-plane.w - Vec3::dot(planeNormal, p1)) / fTmp;

	//선분의 범위 안에 없을 때
	if(!(t>=0&&t<=1))		return retval;
	retval.bCollide = true;
	//교차점 계산
	retval.point = p1 + t*dir;

	return retval;
}

float Picker::getAngleBetweenVectors(cocos2d::Vec3 v1, cocos2d::Vec3 v2)
{
	//A*B = |A|*|B|*cos@
	//@ =acos( A*B / |A|*|B|)
	auto f = std::acos(cocos2d::Vec3::dot(v1, v2) /( v1.length()*v2.length()));
	if (std::isnan(f))
		return 0;
	return  f;
}
