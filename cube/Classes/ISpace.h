#pragma once

class IPickableObject;
class ICamera;


enum class Type_Puzzle : unsigned char;


class ISpace
{
public:
	virtual ~ISpace() {};
	virtual std::vector<IPickableObject*> getObjects() = 0;
	virtual ICamera* getCamera() = 0;
	virtual Type_Puzzle getPuzzleType() = 0;
};
