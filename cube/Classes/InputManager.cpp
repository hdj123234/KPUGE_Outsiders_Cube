#include "stdafx.h"

#include "InputManager.h"

#include "InputEvent.h"
#include"Module_CC\CCInputEvent.h"
#include"Picker.h"
#include<sstream>

InputManager::InputManager()
	:picker(*(new Picker()))
{
	// 반드시 CW 순서대로 입력
	vFaceRotationX.push_back(ID_Face::Face_Up); vFaceRotationX.push_back(ID_Face::Face_Back);
	vFaceRotationX.push_back(ID_Face::Face_Down); vFaceRotationX.push_back(ID_Face::Face_Front);

	vFaceRotationY.push_back(ID_Face::Face_Front); vFaceRotationY.push_back(ID_Face::Face_Left);
	vFaceRotationY.push_back(ID_Face::Face_Back); vFaceRotationY.push_back(ID_Face::Face_Right);

	vFaceRotationZ.push_back(ID_Face::Face_Up); vFaceRotationZ.push_back(ID_Face::Face_Right);
	vFaceRotationZ.push_back(ID_Face::Face_Down); vFaceRotationZ.push_back(ID_Face::Face_Left);
}

InputManager::~InputManager()
{
	for (auto i : inputEvent)
		delete i;
	inputEvent.clear();

	delete &picker;
}

void InputManager::setCCInputEvents(ICCInputEvent* _inputEvent)
{
	_inputEvent;
	// 코코스의 인풋이벤트를 인게임에서 쓸 인풋이벤트로 변환합니다.
	// ICCInputEvent 를 IInputEvent 로 변환하는 작업
	// ....

//	IInputEvent* input = new InputEvent_Spin();
//	inputEvent.push_back(input);

	switch (_inputEvent->getType())
	{
	case Type_CCInputEvent::Touch:
	{
		auto pEvent = static_cast<CCInputEvent_Touch *>(_inputEvent);

		// 터치의 상태가 Start 라면 벡터에 집어넣습니다.
		if (pEvent->getTouchType() == Type_Touch::Start) {
			inputEvent.push_back(new InputEvent_TypeOnly(Type_InputEvent::Touch));

			auto cPick = picker.pick(pEvent->getPosX(), pEvent->getPosY());
			InputState inputState;
			if (cPick.bSuccess)
				inputState = InputState::PuzzleSpin;
			else
				inputState = InputState::CameraMove;
			vTouch.push_back(TouchType(pEvent->getTouchID(), cPick, inputState));
		}

		TouchType *isFind = nullptr;
		for (auto &i : vTouch) {
			if (i.id == pEvent->getTouchID()) {
				isFind = &i;
				break;
			}
		}
		if (isFind == nullptr) break;
		inputEventProcessing(_inputEvent, isFind);
		break;
	}

	case Type_CCInputEvent::SelectObject:
	{
		auto pEvent = static_cast<CCInputEvent_SelectObject *>(_inputEvent);
		switch (pEvent->getTouchType())
		{
		case Type_Touch::Start:
		{
			//if (selectObject == Type_Component::ZRotationButton1)
			//	inputState = InputState::CameraMoveZL;
			//if (selectObject == Type_Component::ZRotationButton2)
			//	inputState = InputState::CameraMoveZR;
			switch (pEvent->getObjectType())
			{
			case Type_Component::OptionButton:
				inputEvent.push_back(new InputEvent_TypeOnly(Type_InputEvent::SelectOption));
				break;
			case Type_Component::StartButton:
				inputEvent.push_back(new InputEvent_TypeOnly(Type_InputEvent::Start));
				break;
			case Type_Component::AutoSuffleButton:
				inputEvent.push_back(new InputEvent_TypeOnly(Type_InputEvent::AutoSuffle));
				break;
			case Type_Component::Retry:
				inputEvent.push_back(new InputEvent_TypeOnly(Type_InputEvent::Retry));
				break;
			case Type_Component::Exit:
				inputEvent.push_back(new InputEvent_TypeOnly(Type_InputEvent::Exit));
				break;
			}
			break;
		}
		case Type_Touch::End:
		{
			// if (pEvent->getObjectType() != selectObject) break;
			break;
		}
		}
		break;
	}
	case Type_CCInputEvent::Exception:
		inputEvent.push_back(new InputEvent_TypeOnly(Type_InputEvent::Exception));
		break;

	case Type_CCInputEvent::ZButton:
	{
		auto pEvent = static_cast<CCInputEvent_ZButton *>(_inputEvent);
		inputEvent.push_back(new InputEvent_RotationCamera_AxisZ((pEvent->isLeft() ? -1 : 1) * pEvent->getScroll() * 360));
		break;
	}

	case Type_CCInputEvent::Back:
	{
		inputEvent.push_back(new InputEvent_Back());
		break;
	}

	// 더미코드
	case Type_CCInputEvent::Scroll:
	{
		auto pEvent = static_cast<CCInputEvent_Scroll *>(_inputEvent);
		inputEvent.push_back(new InputEvent_ZoomCamera(1 - pEvent->getScroll() / 10));
		break;
	}
	}

	for (unsigned int i = 0; i < vTouch.size(); ++i) {
		if (vTouch[i].inputState == InputState::Nothing) {
			auto iter = vTouch.begin() + i--;
			vTouch.erase(iter);
		}
	}
	delete _inputEvent;
}

std::vector<IInputEvent*> InputManager::getInputEvents()
{
	//auto result = inputEvent;
	//if (!inputEvent.empty()) {
	//	inputEvent.clear();
	//}
	//return result;
	return std::move(inputEvent);
}


void InputManager::inputEventProcessing(ICCInputEvent * _inputEvent, TouchType* touchType)
{
	auto pEvent = static_cast<CCInputEvent_Touch *>(_inputEvent);
	if (touchType->inputState == InputState::PuzzleSpin)
	{
		switch (pEvent->getTouchType())
		{
		case Type_Touch::Start:
		{
			touchType->posX = pEvent->getPosX();
			touchType->posY = pEvent->getPosY();
			break;
		}
		case Type_Touch::Cancle:
		case Type_Touch::End:
		{
			touchType->posX = -1;
			touchType->posY = -1;
			touchType->inputState = InputState::Nothing;
			break;
		}
		case Type_Touch::Move:
		{
			auto cpick = picker.pick(pEvent->getPosX(), pEvent->getPosY());
			if (cpick.bSuccess == false) break;
			if (cpick.result.posID == touchType->pickingResult.result.posID
				&& cpick.result.faceID == touchType->pickingResult.result.faceID) break;
			//if (abs(pEvent->getPosX() - posX) < 0.05
			//	|| abs(pEvent->getPosY() - posY) < 0.05) break;
			auto pSpin = createSpinEvent(touchType->pickingResult.result, cpick.result);
			if (pSpin == nullptr) break;

			inputEvent.push_back(pSpin);

			touchType->pickingResult = cpick;
			touchType->posX = pEvent->getPosX();
			touchType->posY = pEvent->getPosY();
			touchType->inputState = InputState::Nothing;
			break;
		}
		}
	}
	else if (touchType->inputState == InputState::CameraMove)
	{
		bool isZoomMode = false;
		for (auto &i : vTouch) {
			if (i.id == touchType->id) continue;
			if (i.inputState == InputState::CameraMove ||
				i.inputState == InputState::CameraZoom) {
				isZoomMode = true;
				i.inputState = InputState::CameraZoom;
				touchType->inputState = InputState::CameraZoom;
			}
		}
		if (!isZoomMode) {
			auto pEvent = static_cast<CCInputEvent_Touch *>(_inputEvent);
			switch (pEvent->getTouchType())
			{
			case Type_Touch::Start:
			{
				touchType->posX = pEvent->getPosX();
				touchType->posY = pEvent->getPosY();
				break;
			}
			case Type_Touch::Cancle:
			case Type_Touch::End:
			{
				touchType->posX = -1;
				touchType->posY = -1;
				touchType->inputState = InputState::Nothing;
				break;
			}
			case Type_Touch::Move:
			{
				auto rateX = pEvent->getPosX() - touchType->posX;
				auto rateY = pEvent->getPosY() - touchType->posY;
				auto dist = sqrt((rateX*rateX) + (rateY*rateY));
				if (std::abs(rateX) < 0.0001 && std::abs(rateY) < 0.0001) break;
				inputEvent.push_back(new InputEvent_RotationCamera(-rateX * 500, rateY * 500, dist * 400));
				//inputEvent.push_back(new InputEvent_RotationCamera(-rateX * 500, 0, 2));
				touchType->posX = pEvent->getPosX();
				touchType->posY = pEvent->getPosY();
				break;
			}
			}
		}
	}

	// 인풋상태가 CameraMove에서 바뀔 수 있으므로 반드시 CameraMove보다 밑에 있어야 합니다.
	if (touchType->inputState == InputState::CameraZoom)
	{
		if (vTouch.size() < 2)
			for (auto &i : vTouch)
				if (i.inputState == InputState::CameraZoom)
					i.inputState = InputState::CameraMove;

		auto pEvent = static_cast<CCInputEvent_Touch *>(_inputEvent);
		switch (pEvent->getTouchType())
		{
		case Type_Touch::Start:
			touchType->posX = pEvent->getPosX();
			touchType->posY = pEvent->getPosY();
			break;
		case Type_Touch::Cancle:
		case Type_Touch::End:
		{
			touchType->inputState = InputState::Nothing;
			int num = 0;
			for (auto &i : vTouch)
				if (i.inputState == InputState::CameraZoom)
					num++;
			if (num < 2) {
				for (auto &i : vTouch)
					if (i.inputState == InputState::CameraZoom)
						i.inputState = InputState::CameraMove;
			}
			break;
		}
		case Type_Touch::Move:
		{
			if (std::abs(pEvent->getPosX() - touchType->posX) < 0.0001 &&
				std::abs(pEvent->getPosY() - touchType->posY) < 0.0001) break;

			// 좌, 우, 상, 하
			float pre[4] = { 1,0,0,1 };
			float cur[4] = { 1,0,0,1 };
			for (auto &i : vTouch) {
				if (i.inputState == InputState::CameraZoom) {
					if (i.posX < pre[0]) pre[0] = i.posX;
					if (i.posX > pre[1]) pre[1] = i.posX;
					if (i.posY > pre[2]) pre[2] = i.posY;
					if (i.posY < pre[3]) pre[3] = i.posY;
				}
			}

			touchType->posX = pEvent->getPosX();
			touchType->posY = pEvent->getPosY();

			for (auto &i : vTouch) {
				if (i.inputState == InputState::CameraZoom) {
					if (i.posX < cur[0]) cur[0] = i.posX;
					if (i.posX > cur[1]) cur[1] = i.posX;
					if (i.posY > cur[2]) cur[2] = i.posY;
					if (i.posY < cur[3]) cur[3] = i.posY;
				}
			}

			float result = pre[1] - pre[0] - cur[1] + cur[0] + pre[2] - pre[3] - cur[2] + cur[3];
			inputEvent.push_back(new InputEvent_ZoomCamera(1 + (result)));

			break;
		}
		}


	}

}

InputEvent_Spin* InputManager::createSpinEvent(_PickingResult r1, _PickingResult r2)
{
	AxisID axisID = 255;
	unsigned char line;
	bool bCW = false;

	bool isOver = false;

	if (r1.posID == r2.posID && r1.faceID == r2.faceID) return nullptr;

	// 같은 블럭의 피킹면 변화로 인한 회전
	if (r1.posID == r2.posID) {
		std::vector<int> rotateAxis(3, 255);
		std::vector<std::vector<ID_Face>*> vFaceAxis = {
			&vFaceRotationX, &vFaceRotationY, &vFaceRotationZ
		};

		for (unsigned int iter = 0; iter < vFaceAxis.size(); ++iter) {
			for (unsigned int i = 0; i < vFaceAxis[iter]->size(); ++i)
				if (vFaceAxis[iter]->at(i) == r1.faceID) {
					for (unsigned int j = 0; j < vFaceAxis[iter]->size(); ++j) {
						if (vFaceAxis[iter]->at(j) == r2.faceID) {
							rotateAxis[iter] = abs(static_cast<int>(i - j));
							if (i - j == -1 || i - j == 3)
								bCW = true;
							break;
						}
					}
				}
		}

		for (unsigned int i = 0; i < rotateAxis.size(); ++i)
			if (rotateAxis[i] == 1 || rotateAxis[i] == 3) {
				switch (i) {
				case 0:	// X회전
					axisID = AxisID_Cube::X;
					line = r1.posID % cube_size;
					break;
				case 1:	// Y회전
					axisID = AxisID_Cube::Y;
					line = r1.posID / cube_size_square;
					break;
				case 2:	// Z회전
					axisID = AxisID_Cube::Z;
					line = r1.posID % cube_size_square / cube_size;
					break;
				default:
					break;
				}
				return new InputEvent_Spin(axisID, line, bCW);
			}
	}

	// 피킹 블럭의 변화로 인한 회전
	else {
		bool isPlus = false;

		// RotationX
		if (r1.posID % cube_size == r2.posID % cube_size) {
			for (unsigned int i = 0; i < vFaceRotationX.size(); ++i)
				if (vFaceRotationX[i] == r1.faceID)
				{
					line = r1.posID % cube_size;
					axisID = AxisID_Cube::X;
					break;
				}
		}
		else
			if (r1.posID % cube_size < r2.posID % cube_size)
				isPlus = true;

		// RotationY
		if (r1.posID / cube_size_square == r2.posID / cube_size_square) {
			for (unsigned int i = 0; i < vFaceRotationY.size(); ++i)
				if (vFaceRotationY[i] == r1.faceID)
				{
					line = r1.posID / cube_size_square;
					axisID = AxisID_Cube::Y;
					break;
				}
		}
		else
			if (r1.posID / cube_size_square < r2.posID / cube_size_square)
				isPlus = true;

		// RotationZ
		if (r1.posID % cube_size_square / cube_size == r2.posID % cube_size_square / cube_size) {
			for (unsigned int i = 0; i < vFaceRotationZ.size(); ++i)
				if (vFaceRotationZ[i] == r1.faceID)
				{
					line = r1.posID % cube_size_square / cube_size;
					axisID = AxisID_Cube::Z;
					break;
				}
		}
		else
			if (r1.posID % cube_size_square / cube_size < r2.posID % cube_size_square / cube_size)
				isPlus = true;

		if (axisID == 255)
			return nullptr;

		switch (axisID)
		{
		case AxisID_Cube::X:
			if (r1.faceID == ID_Face::Face_Front || r1.faceID == ID_Face::Face_Down)
				bCW = isPlus;
			else
				bCW = !isPlus;
			break;
		case AxisID_Cube::Y:
			if (r1.faceID == ID_Face::Face_Right || r1.faceID == ID_Face::Face_Back)
				bCW = isPlus;
			else
				bCW = !isPlus;
			break;
		case AxisID_Cube::Z:
			if (r1.faceID == ID_Face::Face_Up || r1.faceID == ID_Face::Face_Left)
				bCW = isPlus;
			else
				bCW = !isPlus;
			break;
		}

		return new InputEvent_Spin(axisID, line, bCW);
	}

	return nullptr;
};