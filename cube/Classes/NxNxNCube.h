#pragma once
#include "OutputMsgCollector.h"
#include "IPickableObject.h"
#include"BlockState.h"

enum class Type_Puzzle : unsigned char;
class NxNxNCubeBone;
class IOutputMsg;

class NxNxNCube : public ITwistyPuzzle, IOutputMsgTarget, IPickableObject
{
	NxNxNCubeBone *cubeBone;
	int cubeSize;
	IPickableObject *p;

public:
	virtual void pure_virtual() {};
	NxNxNCube(int _cubeSize, IPickableObject *_p = nullptr);
	~NxNxNCube();

	virtual void initialize();
	virtual void reset();
	virtual void recvInputEvent(IInputEvent *inputEvent);
	virtual IPickableObject* getPickableObjects();
	virtual bool checkClear();
	virtual Type_Puzzle getPuzzleType();

	virtual const PickingData::PickingTargetData* getPickingTargetData() const { return p->getPickingTargetData(); };

	virtual void setRotate(float dTime);
	virtual void getBlockState(std::vector<BlockState> &r);
	virtual void getSpinState(std::vector<CubeSpinState> &r);
	virtual void update(float dTime);
	virtual void shuffle(unsigned int _num, float _speed);
	virtual bool isSpinning();
	virtual void skipShuffling(float _speed);
	virtual unsigned int getCubeSize() { return cubeSize; }

	virtual IOutputMsgTarget* getTarget() { return this; }
	virtual void visit(OutputMsgCollector *p);
};