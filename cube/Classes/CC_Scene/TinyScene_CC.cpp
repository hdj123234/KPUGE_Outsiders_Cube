
#include"stdafx.h"

#include "TinyScene_CC.h"
#include "SimpleAudioEngine.h"

#include"MyUtility_NEJ\MyDebugUtility.h"
#include"MyUtility_NEJ\MyINI.h"
//#include"SceneTestScene.h"

#include"..\Module_CC\interface\ICCResource.h"
#include"AdmobController.h"

USING_NS_CC;


//----------------------------------------------- TinyScene_CC -----------------------------------------------

cocos2d::Scene * TinyScene_CC::createScene(TinySceneData & sceneData)
{
	auto scene = TinyScene_CC::create();
	scene->sceneData = sceneData;
	scene->initialize();
	scene->retain();
	return scene;
}

bool TinyScene_CC::init()
{
	
	if (!Scene::init())
	{
		return false;
	}

	return true;
}

void TinyScene_CC::onEnter()
{
	Scene::onEnter();

	for (auto listener : sceneData.listeners)
	{
		listener->setEnabled(true);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	}
	if (sceneData.bgm)
		sceneData.bgm->run();

	for (auto schedule : sceneData.schedules)
	{
		if (schedule.bOnce)
			this->scheduleOnce(schedule.callback, schedule.delay, schedule.key);
		else
			this->schedule(schedule.callback, schedule.interval, schedule.repeat, schedule.delay, schedule.key);
	}


	this->schedule(schedule_selector(TinyScene_CC::func));
}
 
void TinyScene_CC::onExit()
{
	for (auto listener : sceneData.listeners)
		_eventDispatcher->removeEventListener(listener);
	using namespace CocosDenshion;

//	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	
	Scene::onExit();
}

void TinyScene_CC::initialize()
{
	for (auto node : sceneData.nodes)
		node->setCCParent(this);

}

TinyScene_CC::~TinyScene_CC()
{
	for (auto data : sceneData.nodes)
		if (data) delete data;
}

void TinyScene_CC::func(float f)
{
	auto instance = AdmobController::getInstance();
	instance->func_admob(f);
}
