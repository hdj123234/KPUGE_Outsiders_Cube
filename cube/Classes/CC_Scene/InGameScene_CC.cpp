
#include"stdafx.h"

#include "InGameScene_CC.h"
#include "SimpleAudioEngine.h"

#include"..\IScene.h"
#include"..\IUIManager.h"
#include"..\Module_CC\interface\ICCOutputMsg.h"
#include"..\Module_CC\interface\ITwistyPuzzleModel.h"
#include"..\Module_CC\CCOutputMsg.h"
#include"..\Module_CC\CCInputEvent.h"
#include"..\Module_CC\CCComponent.h"


#include"..\Module_Manager\StartupManager.h"
#include"..\Types\InitializeData.h"
#include"..\Module_Manager\CCResourceManager.h"
#include"..\Module_Manager\CCSceneManager.h"
#include"..\InitializeState.h"
#include"..\SceneInitializer.h"
#include"..\EnumDefine.h"
#include"..\Module_CC\CCAudio.h"
#include<string>
#include<sstream>
#include<iomanip>

#include"AdmobController.h"

USING_NS_CC;



Functor_Selection::ID_Selected Functor_Selection::id_Selected = Functor_Selection::Nothing;

//----------------------------------------------- TinyScene_CC -----------------------------------------------


InGameScene_CC::InGameScene_CC()
	: scene				(nullptr), 
	uiManager			(nullptr),
	puzzleModel			(nullptr),
	touchListener		(nullptr),
	camera_3D			(nullptr)
{
}

InGameScene_CC::~InGameScene_CC()
{
	for (auto data : assistantComponents)
		if(data) delete data;
}

//cocos2d::Scene * InGameScene_CC::createScene(InitializeState *initializeState)
cocos2d::Scene * InGameScene_CC::createScene()
{
	auto retval = InGameScene_CC::create();
	return retval;
}

bool InGameScene_CC::init()
{
	if (!Scene::init())
	{
		return false;
	}
	functors.reserve(255);

	auto resourceManager = CCResourceManager::getInstance();
	assistantComponents = resourceManager->getInGameSceneComponents();

	winSize = Director::getInstance()->getWinSize();
	
//	ModelRawData_Cube *modelData = new ModelRawData_Cube(3);
//	model = modelData->createPuzzleModel();
//	model->setCCParent(this);
//	model->getCCNode()->setCameraMask(static_cast<unsigned short>(cocos2d::CameraFlag::USER2));

	
	//3D 카메라 생성
	auto &projection = StartupManager::getInstance()->getInitializeData().projection;
	camera_3D = cocos2d::Camera::createPerspective(projection.fov, projection.aspectRatio, projection.nearZ, projection.farZ);
	camera_3D->setPosition3D(Vec3(0,0,1));
	camera_3D->lookAt(Vec3(0, 0, 0));
	camera_3D->setCameraFlag(cocos2d::CameraFlag::USER2);
	addChild(camera_3D);
	
	//AssistantComponent 생성
	assistantComponents.resize(255);
	for (auto &data : assistantComponents) data = nullptr;
	menu = cocos2d::Menu::create();
	auto &assistantComponentINIs = StartupManager::getInstance()->getInitializeData().assistantComponents;
	auto nAssistantComponent = assistantComponentINIs.size();
	//for (auto i=0;i<nAssistantComponent;++i)
	for(auto &data : assistantComponentINIs)	createAssistantComponent(*data);
	this->addChild(menu);
	menu->retain();

	//씬 및 UI 세팅
	auto startupManager = StartupManager::getInstance();
	scene = startupManager->getScene(); 


	return true;

}

void InGameScene_CC::onEnter()
{
	Scene::onEnter();

	//리스너 생성 및 등록
	touchListener = EventListenerTouchAllAtOnce::create();
	touchListener->onTouchesBegan = [=](const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev)->bool
	{		return inputEvent(TouchType::Begin, touches, ev);	};
	touchListener->onTouchesMoved = [=](const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev)
	{		inputEvent(TouchType::Move, touches, ev);	};
	touchListener->onTouchesEnded = [=](const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev)
	{		inputEvent(TouchType::End, touches, ev);	};
	touchListener->onTouchesCancelled = [=](const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev)
	{		inputEvent(TouchType::Cancle, touches, ev);	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	mouseListener=EventListenerMouse::create();
	mouseListener->onMouseScroll = [=](EventMouse* ev) {
		auto f = ev->getScrollY();
		uiManager->setCCInputEvent( new CCInputEvent_Scroll(ev->getScrollY()));
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(mouseListener, this);

	backButtonListener = EventListenerKeyboard::create();
	backButtonListener->onKeyPressed = [=](EventKeyboard::KeyCode key, Event *ev) {
		if (key == EventKeyboard::KeyCode::KEY_ESCAPE)
		{
			if (bRun)
				selectOption();
			else
				returnOption();

		}
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(backButtonListener, this);

	resetScene();
	bRun = true;
	//스케줄 설정
	this->schedule(schedule_selector(InGameScene_CC::func_update));
}

void InGameScene_CC::onExit()
{
	//리스너 해제
	_eventDispatcher->removeEventListener(touchListener);
	_eventDispatcher->removeEventListener(mouseListener);
	_eventDispatcher->removeEventListener(backButtonListener);

	Scene::onExit();
}

void InGameScene_CC::resetScene()
{
	auto initializeState = InitializeState::getInstance();

	//퍼즐 객체를 받아옴
	auto resourceManager = CCResourceManager::getInstance();

	puzzleModel = resourceManager->getTwistyPuzzle(initializeState->getPuzzleType());
	puzzleModel->setCCParent(this);
	puzzleModel->getCCNode()->setCameraMask(static_cast<unsigned short>(cocos2d::CameraFlag::USER2));



	//씬 초기화
	auto sceneInitializer = StartupManager::getInstance()->getSceneInitializer();

//	InitializeState state= *initializeState;
//	state.pickableObjectPtr = puzzleModel->getPickableObject();
	initializeState->setPickableObjectPtr(puzzleModel->getPickableObject());


	//sceneInitializer->setInitializeState(state);
	sceneInitializer->initializeScene(scene);

	//AssistantComponent 초기화
	for (auto data : assistantComponents)if(data) data->hide();

	//UI매니저 갱신
	uiManager = scene->getUIManager();

	//타이머 초기화
	timer = 0;
	static_cast<CCTextLabel*>(assistantComponents[static_cast<unsigned char>(Type_Component::TimerLable)])->setLabel("");

}

void InGameScene_CC::func_update(float dTime)
{
	auto instance = AdmobController::getInstance();
	instance->func_admob(dTime);
	
	//winSize = Director::getInstance()->getWinSize();
//	std::stringstream ss;
//	ss << "X: " << winSize.x << "   Y: " << winSize.y;
//	const char* c = ss.str().c_str();
	//cocos2d::log("X = %f Y : %f", winSize.x, winSize.y);

	auto selectedID = Functor_Selection::getSelectedID();
	Functor_Selection::clear();

	if (!bRun) {
		if (selectedID == static_cast<unsigned char>(Type_Component::Option_Return))
			returnOption();
		else if (selectedID == static_cast<unsigned char>(Type_Component::Option_Title))
			moveToTitle();

		if(static_cast<CCCheckBox*>(assistantComponents[static_cast<unsigned char>(Type_Component::Option_MuteBGM)])->isChecked())
			CCVolumnManager::getInstance()->setBGMVolumn(0);
		else
			CCVolumnManager::getInstance()->setBGMVolumn(1);
		if (static_cast<CCCheckBox*>(assistantComponents[static_cast<unsigned char>(Type_Component::Option_MuteSE)])->isChecked())
			CCVolumnManager::getInstance()->setSEVolumn(0);
		else
			CCVolumnManager::getInstance()->setSEVolumn(1);
		return;
	}
	
	if (selectedID == static_cast<unsigned char>(Type_Component::Exit))
		moveToTitle();
	else if (selectedID == static_cast<unsigned char>(Type_Component::Retry))
	{
		this->removeChild(puzzleModel->getCCNode());
		Director::getInstance()->popScene();
		Director::getInstance()->pushScene(CCSceneManager::getInstance()->getInGameScene());
		return;
	}
	else if (selectedID == static_cast<unsigned char>(Type_Component::OptionButton))
		selectOption();


	else if (selectedID != Functor_Selection::Nothing)
		uiManager->setCCInputEvent(new CCInputEvent_SelectObject(Type_Touch::Start, static_cast<Type_Component>( selectedID)));
	
	//uiManager->setCCInputEvent(new CCInputEvent_Exception(Type_Exception::Debug));


	scene->update(dTime);
	for(auto p : uiManager->getCCOutputMsgs())	handleOutputMsg(p);
	for(auto p : scene->getSceneMsgs())			handleSceneMsg(p);
	//테스트코드
	//static_cast<CCTextLabel*>(assistantComponents[static_cast<unsigned char>(Type_Component::TimerLable)])->setLabel(getStringFromTime(timer += dTime));
}



void InGameScene_CC::handleOutputMsg(ICCOutputMsg * outputMsg)
{
	switch (outputMsg->getType()) {
	case Type_CCOutputMsg::CubeState:
		//작업예정
		puzzleModel->setState(outputMsg);
		break;
	case Type_CCOutputMsg::CameraState:
	{
		auto state = static_cast<CCOutputMsg_CameraState *>(outputMsg);
		//		auto camera = getDefaultCamera();
		camera_3D->setPosition3D(Vec3(state->transform[12], state->transform[13], state->transform[14]));
		camera_3D->lookAt(Vec3(0, 0, 0), Vec3(state->transform[4], state->transform[5], state->transform[6]));
		//		camera->setAdditionalTransform(Mat4(state->transform));
		break;
	}
	case Type_CCOutputMsg::SetTimer:
		timer = static_cast<CCOutputMsg_SetTimer *>(outputMsg)->getTime();
		static_cast<CCTextLabel*>(assistantComponents[static_cast<unsigned char>(Type_Component::TimerLable)])->setLabel(getStringFromTime(timer));
		break;
	case Type_CCOutputMsg::Clear:
//		clear();
		break;
	case Type_CCOutputMsg::AssistantComponent:
	{
		
		auto state = static_cast<CCOutputMsg_Resource *>(outputMsg);
		auto type = state->getObjectType();
		auto target = assistantComponents[static_cast<unsigned char>(type)];
		if (!target) break;
		if (state->isVisible())
		{
			if(target)
				target->show();
			if (type == Type_Component::TimerLable)
			{

			}
			else if (type == Type_Component::ClearLable)
				static_cast<CCTextLabelAndBG *>(target)->setLabel(getStringFromTime(timer));
		
		}
		else {

			//테스트코드
			//if (state->getObjectType() == Type_Component::TimerLable) break;
			target->hide();
		}
		if (state->isEnable())
		{
			target->enable();
		}
		else
		{

			target->disable();
		}
		break;
	}
	case Type_CCOutputMsg::RunAudio:
	{
		auto msg = static_cast<CCOutputMsg_RunAudio *>(outputMsg);
		CCResourceManager::getInstance()->getAudio(msg->getAudio())->run();
		break;
	}
	}

	delete outputMsg;
}

void InGameScene_CC::handleSceneMsg(SceneMsg * sceneMsg)
{
	delete sceneMsg;
}

bool InGameScene_CC::inputEvent(TouchType type, const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev)
{
	if (!bRun)return false;

	if (CCZButton::isSelected())
	{
		auto tmp = std::move(msgCollector);
		for (auto p : tmp)
		{
			uiManager->setCCInputEvent(p);
		}
		return false;
	}

	auto winSize = Director::getInstance()->getWinSize();

	static std::map<TouchType, Type_Touch> typeMap = {
		std::make_pair(TouchType::Begin	,Type_Touch::Start),
		std::make_pair(TouchType::Move	,Type_Touch::Move),
		std::make_pair(TouchType::End	,Type_Touch::End),
		std::make_pair(TouchType::Cancle,Type_Touch::Cancle),
	};

	for (auto touch : touches)
	{
		auto touchPoint = this->convertToWorldSpace(touch->getLocation());

		auto eve = new CCInputEvent_Touch(typeMap[type],
			touch->getID(),
			static_cast<float>(touchPoint.x) / winSize.width,
			static_cast<float>(touchPoint.y) / winSize.height);

		uiManager->setCCInputEvent(eve);

	}



	return true;
}

void InGameScene_CC::clear()
{
	removeChild(puzzleModel->getCCNode());
	Director::getInstance()->popScene();
}

void InGameScene_CC::createAssistantComponent(const AssistantComponentData & ini)
{
	using namespace cocos2d;
	auto path = StartupManager::getInstance()->getInitializeData().resourcePath;
	Node *node=nullptr;
	auto id = static_cast<unsigned char>(ini.type);
	
	cocos2d::Vec2 anchor;
	cocos2d::Vec2 pos; 

	//ini로부터 선행정보 읽어옴



	switch (ini.category)
	{
	case ComponentCategory::Button:
	{
		auto &data = static_cast<const ACData_Button&>(ini);
		functors.push_back(Functor_Selection(id));
//		pos = data.pos;
//		node = MenuItemImage::create(path+ data.image1, path+ data.image2, functors.back());
//		menu->addChild(node);
//		pos.x -= 0.5;
//		pos.y -= 0.5;
//		assistantComponents[id]=(new CCSimpleComponent(node));
//		node->setPositionNormalized(pos);
//		anchor = getAnchor(data.anchor_x, data.anchor_y);
//		node->setAnchorPoint(anchor);
//		node->setScale(data.scale *winSize.x / node->getContentSize().width);

		MyFunctor f(functors.back());

		assistantComponents[id] = (new CCSimpleButton(data, f));
		assistantComponents[id]->setCCParent(this);
		break;
	}
	case ComponentCategory::ClearLabel:
	{
		auto &data = static_cast<const ACData_ClearLabel&>(ini);
		node = Sprite::create(data.image);
		node->setPositionNormalized(data.pos);
		float bgSize = data.scale *winSize.x / node->getContentSize().width;
		node->setScale(bgSize);
		anchor = getAnchor(data.anchor_x, data.anchor_y);
		node->setAnchorPoint(anchor);
		node->setOpacity(data.opacity);
		TextHAlignment hAlign;
		switch (data.anchor_x){
		case Anchor_X::C: hAlign = TextHAlignment::CENTER; break;
		case Anchor_X::L: hAlign = TextHAlignment::LEFT; break;
		case Anchor_X::R: hAlign = TextHAlignment::RIGHT; break;
		}
		TextVAlignment vAlign;
		switch (data.anchor_y) {
		case Anchor_Y::C: vAlign = TextVAlignment::CENTER; break;
		case Anchor_Y::T: vAlign = TextVAlignment::TOP; break;
		case Anchor_Y::B: vAlign = TextVAlignment::BOTTOM; break;
		}
		Size textBoxSize;
		textBoxSize.width = winSize.x * data.textBoxSize.x;
		textBoxSize.height = winSize.y * data.textBoxSize.y;
		auto label =  Label::createWithTTF("", path + data.fontFile, data.fontSize, textBoxSize, hAlign, vAlign);
		label->setPositionNormalized(data.textPos);
		anchor = getAnchor(data.textAnchor_x, data.textAnchor_y);
		label->setAnchorPoint(anchor);
		//label->setScale(data.scale *winSize.x / node->getContentSize().width);
		cocos2d::Color3B color(data.color.x, data.color.y, data.color.z);
		
		label->setColor(color);
		label->setCascadeOpacityEnabled(false);
		label->setScale(1.0f / bgSize);
		
		auto subImage= Sprite::create(data.subImage);
		subImage->setPositionNormalized(data.subImagePos);
		subImage->setScale(data.subImageScale *winSize.x / subImage->getContentSize().width/ bgSize);
		anchor = getAnchor(data.subImageAnchor_x, data.subImageAnchor_y);
		subImage->setAnchorPoint(anchor);
		subImage->setCascadeOpacityEnabled(false);
		node->addChild(subImage);
		assistantComponents[id]=new CCTextLabelAndBG(label,node);
		assistantComponents[id]->setCCParent(this);
		
		break;
	}
	case ComponentCategory::Text:
	{
		//폰트 크기 및 범위조정 등 추후 작업
		auto &data = static_cast<const ACData_Text&>(ini);
		pos = data.pos;
		Label* label;

		TextHAlignment hAlign;
		switch (data.anchor_x) {
		case Anchor_X::C: hAlign = TextHAlignment::CENTER; break;
		case Anchor_X::L: hAlign = TextHAlignment::LEFT; break;
		case Anchor_X::R: hAlign = TextHAlignment::RIGHT; break;
		}
		TextVAlignment vAlign;
		switch (data.anchor_y) {
		case Anchor_Y::C: vAlign = TextVAlignment::CENTER; break;
		case Anchor_Y::T: vAlign = TextVAlignment::TOP; break;
		case Anchor_Y::B: vAlign = TextVAlignment::BOTTOM; break;
		}
		Size textBoxSize;
		textBoxSize.width = winSize.x * data.textBoxSize.x;
		textBoxSize.height = winSize.y * data.textBoxSize.y;
		node = label = Label::createWithTTF("", path + data.fontFile, data.fontSize,textBoxSize, hAlign, vAlign);
		cocos2d::Color3B color(data.color.x, data.color.y, data.color.z);
		
		label->setColor(color);
		this->addChild(label);
		assistantComponents[id]=new CCTextLabel(label);
		node->setPositionNormalized(pos);
		anchor = getAnchor(data.anchor_x, data.anchor_y);
		node->setAnchorPoint(anchor);
//		node->setScale(data.scale *winSize.x / node->getContentSize().width);

		break;
	}
	case ComponentCategory::ZButton:
	{
		auto &data = static_cast<const ACData_ZButton&>(ini);
		bool bLeft = ini.type == Type_Component::ZRotationButton1;

		//bg
		auto bg = Sprite::create(path + data.bg);
		bg->setOpacity(data.bgOpacity);
		bg->setScaleX(data.scrollSizeX*winSize.x / bg->getContentSize().width);
		bg->setScaleY(winSize.y / bg->getContentSize().height);


		auto frame1 = Sprite::create(path + data.image1);
		frame1->setOpacity(data.buttonOpacity);
		frame1->setScale(data.scrollSizeX*winSize.x / frame1->getContentSize().width);

		auto frame2 = Sprite::create(path + data.image2);
		frame2->setOpacity(data.buttonOpacity);
		frame2->setScale(data.scrollSizeX*winSize.x / frame2->getContentSize().width);

		this->addChild(bg);
		this->addChild(frame1);
		this->addChild(frame2);
		frame2->setVisible(false);

		assistantComponents[id] = new CCZButton(bg, frame1, frame2, data.scrollSizeX, bLeft, msgCollector);
//		node->setPositionNormalized(pos);
//		node->setAnchorPoint(anchor);
		//		node->setScale(data.scale *winSize.x / node->getContentSize().width);

		break;
	}
	case ComponentCategory::Label:
	{
		auto &data = static_cast<const ACData_Label&>(ini);
		assistantComponents[id] = new CCImageLabel(data);
		assistantComponents[id]->setCCParent(this);
		break;
	}
	case ComponentCategory::CheckBox:
	{
		auto &data = static_cast<const ACData_CheckBox&>(ini);
		assistantComponents[id] = new CCCheckBox(data);
		assistantComponents[id]->setCCParent(this);
		break;
	}
	} 

}

void InGameScene_CC::selectOption()
{
	if (bRun)
	{
		bRun = false;
		auto nSize = assistantComponents.size();
		assistantComponentsState.resize(assistantComponents.size());

		for (auto i=0U;i<nSize;++i)
			if (assistantComponents[i]) {
				assistantComponentsState[i] = assistantComponents[i]->isEnable();
				assistantComponents[i]->disable();
			}

		assistantComponents[static_cast<unsigned char>(Type_Component::Option_BG)]		->show();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_Return)]	->show();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_MuteBGM)]	->show();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_MuteSE)]	->show();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_Title)]	->show();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_BG)]		->enable();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_Return)]	->enable();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_MuteBGM)]	->enable();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_MuteSE)]	->enable();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_Title)]	->enable();
	}
}

void InGameScene_CC::returnOption()
{
	if (!bRun)
	{
		bRun = true;

		auto nSize = assistantComponents.size();
		
		for (auto i = 0U; i<nSize; ++i)			
			if (assistantComponents[i]&& assistantComponentsState[i])
				assistantComponents[i]->enable();

		assistantComponents[static_cast<unsigned char>(Type_Component::Option_BG)]		->hide();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_Return)]	->hide();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_MuteBGM)]	->hide();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_MuteSE)]	->hide();
		assistantComponents[static_cast<unsigned char>(Type_Component::Option_Title)]	->hide();
	}
}

void InGameScene_CC::moveToTitle()
{
	this->removeChild(puzzleModel->getCCNode());
	Director::getInstance()->popScene();
	return;
}


std::string InGameScene_CC::getStringFromTime(float f)
{
	using namespace std;
	stringstream ss;
	if (f > 9999.99) return "9999.99s";
	ss << setprecision(2) << fixed << f << " s";
	return ss.str();
}
