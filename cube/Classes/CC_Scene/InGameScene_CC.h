#pragma once

#include "cocos2d.h"
#include"..\MyFunctor.h"
//전방선언
class IScene;
class IUIManager;
class ITwistyPuzzleModel;
class ICCOutputMsg;
class ICCInputEvent;
class SceneMsg;
class InitializeState;
class ICCNode; 
struct AssistantComponentData;
enum class Anchor_X:unsigned char ;
enum class Anchor_Y:unsigned char ;

typedef std::vector<ICCInputEvent *> InputMsgCollector;

class Functor_Selection :public IMyFunctor{
private:
	typedef unsigned char ID_Selected;

	static ID_Selected id_Selected;

	ID_Selected myID;

public:
	static const ID_Selected Nothing = 255;

	Functor_Selection(ID_Selected myID) :myID(myID) {}
	void operator()(cocos2d::Ref *) { id_Selected = myID; }
	void operator()() { id_Selected = myID; }
	
	static ID_Selected getSelectedID() { return id_Selected; }
	static ID_Selected clear() { return id_Selected= Nothing; }
};


class InGameScene_CC : public cocos2d::Scene {
private:
	enum TouchType { Begin, Move, End, Cancle };

private: 
	IScene *scene;
	IUIManager *uiManager;
	ITwistyPuzzleModel *puzzleModel;
	cocos2d::EventListenerTouchAllAtOnce *touchListener;
	cocos2d::Camera *camera_3D;
	std::vector<ICCNode *> assistantComponents;
	std::vector<bool> assistantComponentsState;
	std::vector<Functor_Selection> functors;
	cocos2d::Vec2 winSize;
	Node *menu;
	float timer;
	InputMsgCollector msgCollector;
	bool bRun;

	//테스트용
	cocos2d::EventListenerMouse *mouseListener;
	cocos2d::EventListenerKeyboard *backButtonListener;

public:
	//static cocos2d::Scene* createScene(InitializeState *initializeState);
	static cocos2d::Scene* createScene();

	// implement the "static create()" method manually
	CREATE_FUNC(InGameScene_CC);

	//override cocos2d::Scene
	virtual bool init();
	virtual void onEnter();
	virtual void onExit();

	void resetScene();
	void func_update(float dTime);
	void selectOption();

private:
	InGameScene_CC();
	~InGameScene_CC();


	void handleOutputMsg(ICCOutputMsg * outputMsg);
	void handleSceneMsg(SceneMsg *sceneMsg);
	bool inputEvent(TouchType type, const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev);
	void clear();

	void createAssistantComponent(const AssistantComponentData &ini);
	void returnOption();
	void moveToTitle();

	static std::string getStringFromTime(float f);
	
	//static cocos2d::Vec2 getAnchor(Anchor_X x, Anchor_Y y);
};
 