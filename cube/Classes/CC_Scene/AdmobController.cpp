#include"stdafx.h"

#include"AdmobController.h"



#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)|| (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "../../cocos2d/cocos/platform/android/jni/JniHelper.h"
#include "FirebaseHelper.h"

#include "firebase/admob/types.h"
#include "firebase/admob.h"
#include "firebase/app.h"
#include "firebase/future.h"
#include "firebase/admob/banner_view.h"



#endif



void AdmobController::initialize()
	{


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		// Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713

		// Initialize Firebase for Android.
		firebase::App* app = firebase::App::Create(
			firebase::AppOptions(), cocos2d::JniHelper::getEnv(), cocos2d::JniHelper::getActivity());
		// Initialize AdMob.
		firebase::admob::Initialize(*app, "ca-app-pub-2372302032847332");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		// Initialize Firebase for iOS.
		firebase::App* app = firebase::App::Create(firebase::AppOptions());
		// Initialize AdMob.
		firebase::admob::Initialize(*app, "ca-app-pub-2372302032847332");
#endif
		// Initialize AdMob.
		//	firebase::admob::Initialize(*app);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)||(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
// Android ad unit IDs.
	const char* kBannerAdUnit = "ca-app-pub-2372302032847332/9984872327";
#elif (CC_TARGET_PLATFORM ==CC_PLATFORM_IOS)
// iOS ad unit IDs.
	const char* kBannerAdUnit = "ca-app-pub-2372302032847332/9984872327";
	#endif
	
	// Create and initialize banner view.
	banner_view = new firebase::admob::BannerView();
	ad_size.ad_size_type = firebase::admob::kAdSizeStandard;
	ad_size.width = 320;
	ad_size.height = 50;
	banner_view->Initialize(getAdParent(), kBannerAdUnit, ad_size);
	// Schedule updates so that the Cocos2d-x update() method gets called.
	//		this->scheduleUpdate();
	//		return true;
	
	//update
	// Check that the banner has been initialized.
	
	
#endif
	}

	void AdmobController::func_admob(float f)
	{
	
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)||(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			if (banner_view->InitializeLastResult().status() ==
				firebase::kFutureStatusComplete) {
				// Check that the banner hasn't started loading.
				if (banner_view->LoadAdLastResult().status() ==
					firebase::kFutureStatusInvalid) {
					// Make the banner visible and load an ad.
					CCLOG("Loading a banner.");
					banner_view->Show();
					banner_view->MoveTo(firebase::admob::BannerView::Position::kPositionTop);
					firebase::admob::AdRequest my_ad_request = {};
					banner_view->LoadAd(my_ad_request);
				}
			}
		#endif
	}
