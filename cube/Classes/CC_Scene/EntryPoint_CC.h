#pragma once
#ifndef _ENTRY_POINT_CC_H_
#define _ENTRY_POINT_CC_H_

#include "cocos2d.h"
 

class EntryPoint_CC : public cocos2d::Scene {
private: 
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	virtual void onEnter();

	// implement the "static create()" method manually
	CREATE_FUNC(EntryPoint_CC);

};

#endif // !_ENTRY_POINT_CC_H_
