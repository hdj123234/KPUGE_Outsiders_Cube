#pragma once

#include "cocos2d.h"

class CubeModel;
class ITwistyPuzzleModel;
class IScene;
class IUIManager;
class SceneMsg;
class ICCOutputMsg;

enum TouchEventType { Begin, Move, End, Cancle };

class SceneTestScene : public cocos2d::Scene
{
	static int n;
private:
	IScene * pScene;
	IUIManager *pUIManager;
	cocos2d::Camera * camera;
	cocos2d::EventListenerTouchAllAtOnce *pListener;

	ITwistyPuzzleModel *pModel;
	cocos2d::Vec3 cameraPos;
	bool bCameraRotate;
	int blockCount;

public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	// a selector callback
//    void menuCloseCallback(cocos2d::Ref* pSender);

	// implement the "static create()" method manually
	CREATE_FUNC(SceneTestScene);


private:
	void func_tick(float f);

	bool inputEvent(TouchEventType type, const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev);
	bool checkTouch( const cocos2d::Touch* touch, cocos2d::Event * ev);
	void handleSceneMsg(const std::vector<SceneMsg *>& sceneMsgs);
	void handleOutputMsg(ICCOutputMsg *msg);
};
 