
#include"stdafx.h"

#include "SceneTestScene.h"
#include "SimpleAudioEngine.h"
 
#include<string>
#include<map>

#include"MyUtility_NEJ\MyDebugUtility.h"
#include"MyUtility_NEJ\MyINI.h"

#include"Cube_Adapter.h"
#include"Module_CC\CubeModel.h"

#include"..\Module_Manager\StartupManager.h"
#include"IScene.h"
#include"IUIManager.h"

//#include"Module_CC\ICCIODummy.h"

#include"..\Module_CC\CCInputEvent.h"
#include"..\Module_CC\CCOutputMsg.h"
#include"..\Module_CC\CCOutputMsg_CubeState.h"

#include"..\\InGameScene.h"
#include"..\\UIManager.h"
#include"..\\SceneInitializer.h"
#include"ITwistyPuzzle.h"
#include"..\module_Manager\ResourceManager.h"
#include"..\EnumDefine.h"

#include"Module_CC\RubiksCubeModel.h"
#include"Module_CC\ModelRawData_Cube.h"

USING_NS_CC;

int SceneTestScene::n;

Scene* SceneTestScene::createScene()
{
	
	
    auto retval =  SceneTestScene::create();
	return retval;
}

// on "init" you need to initialize your instance
bool SceneTestScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

	//assert(StartupManager::getInstance()->startup());
	


	////ini로부터 배경색 읽기
	//std::string sColor;
	//MyINISection section = MyINIData( MyDebugUtility::getInstance()->getINIPath()+"RenderTest.ini").getSection();
	//if (section.isEmpty())
	//{
	//	sColor = "black";
		cameraPos = Vec3(0,5,20);
		bCameraRotate = true;
		blockCount = 27;
	//}
	//else
	//{
	//	sColor = section.getParam("bgcolor");
	//	auto v = section.getParam_Vector3("cameraPos");
	//	cameraPos = Vec3(v.x,v.y,v.z);
	//	bCameraRotate = section.getParam_bool("cameraRotate");
	//	blockCount = section.getParam_uint("BlockCount");
	//}
	

	ModelRawData_Cube *modelData= new ModelRawData_Cube(3);
	pModel = modelData->createPuzzleModel();

//	pModel = new CubeModel(blockCount);
	pModel->setCCParent(this);
//	cameraPos = Vec3(0, 0, 0);
	auto winSize = Director::getInstance()->getWinSize();
//	camera = getDefaultCamera();
	auto &ini_projection= StartupManager::getInstance()->getInitializeData().projection;
	camera = cocos2d::Camera::createPerspective(ini_projection.fov, ini_projection.aspectRatio, ini_projection.nearZ, ini_projection.farZ);
	//auto v = section.getParam_Vector3("cameraPos");
//	pModel->getCCNode()->setVisible(false);
	pModel->getCCNode()->setCameraMask(static_cast<unsigned short>(cocos2d::CameraFlag::USER2));

	camera->initPerspective(ini_projection.fov, ini_projection.aspectRatio, ini_projection.nearZ, ini_projection.farZ);
	camera->setPosition3D(cameraPos);
	camera->lookAt(Vec3(0, 0, 0)); 
	camera->setCameraFlag(cocos2d::CameraFlag::USER2);
	addChild(camera);


	auto uiCamera = getDefaultCamera();

//	uiCamera->setCameraFlag(cocos2d::CameraFlag::DEFAULT);
	//cocos2d::CameraFlag
	auto layer = Node::create();
		auto label = Sprite::create(std::string("HelloWorld.png"));
		//auto label = cocos2d::LabelTTF::create("A", "arial", 80);
		label->setPositionNormalized(Vec2( 0.5,0.5));
		auto s = label->getContentSize();

	pListener = EventListenerTouchAllAtOnce::create();
	pListener->onTouchesBegan = [=](const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev)->bool 
	{		return inputEvent(TouchEventType::Begin, touches, ev);	};
	pListener->onTouchesMoved = [=](const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev)
	{		inputEvent(TouchEventType::Move , touches, ev);	};	
	pListener->onTouchesEnded = [=](const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev)
	{		inputEvent(TouchEventType::End, touches, ev);	};	
	pListener->onTouchesCancelled = [=](const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev)
	{		inputEvent(TouchEventType::Cancle, touches, ev);	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(pListener, this);
//	_eventDispatcher->addEventListenerWithSceneGraphPriority(pListener->clone(), label);

	this->schedule(schedule_selector(SceneTestScene::func_tick)); 

	SceneInitializer initializer;
	InitializeState initializeState;
	initializeState.gameMode = Type_GameMode::Auto;
	initializeState.puzzleType = Type_Puzzle::RubiksCube;
	initializeState.pickableObjectPtr = pModel->getPickableObject();

	initializer.setInitializeState(initializeState);

	pScene = StartupManager::getInstance()->getScene();
	initializer.initializeScene(pScene);

	pUIManager = pScene->getUIManager();
	assert(pScene&&pUIManager);

	return true;
}

void SceneTestScene::func_tick(float f)
{
	//if (bCameraRotate)
	//{

	//	Mat4 r;
	//	cocos2d::Mat4::createRotationY(0.01f, &r);
	//	r.transformPoint(&cameraPos);
	//	auto camera = getDefaultCamera();
	//	camera->setPosition3D(cameraPos);
	//	camera->lookAt(Vec3(0, 0, 0));
	//}
	pScene->update(f);
	handleSceneMsg(pScene->getSceneMsgs());
	for(auto data :pUIManager->getCCOutputMsgs())
		handleOutputMsg(data);
}

bool SceneTestScene::inputEvent(TouchEventType type, const std::vector<cocos2d::Touch*> touches, cocos2d::Event * ev)
{
//	if(touches.size()==1&& checkTouch(touches[0],ev))



	auto winSize = Director::getInstance()->getWinSizeInPixels();
	//작업예정
//	static ICCInputDummy input;
	static std::map<TouchEventType, Type_Touch> typeMap = {
		std::make_pair(TouchEventType::Begin	,Type_Touch::Start	),
		std::make_pair(TouchEventType::Move		,Type_Touch::Move	),
		std::make_pair(TouchEventType::End		,Type_Touch::End	),
		std::make_pair(TouchEventType::Cancle	,Type_Touch::Cancle	),
	};

	for (auto touch : touches)
	{ 
		auto touchPoint = this->convertToWorldSpace(touch->getLocation());
		
		auto eve = new CCInputEvent_Touch(	typeMap[type], 
											touch->getID(),
											static_cast<float>(touchPoint.x)/winSize.width, 
											static_cast<float>(touchPoint.y)/winSize.height);

		pUIManager->setCCInputEvent(eve);

	}
	


	return true;
}

bool SceneTestScene::checkTouch(const cocos2d::Touch * touch, cocos2d::Event * ev)
{
	auto target = ev->getCurrentTarget();
	auto touchPoint = this->convertToWorldSpace(touch->getLocation());
	auto boundingBox = target->getBoundingBox();
	if (boundingBox.containsPoint(touchPoint))
	{
		cocos2d::log((std::string("touch, Tag : ") + std::to_string(target->getTag())).c_str());
		return true;

	}
	return false;
}

//추후작업
void SceneTestScene::handleSceneMsg(const std::vector<SceneMsg*>& sceneMsgs)
{
}

void SceneTestScene::handleOutputMsg(ICCOutputMsg * msg)
{
	switch (msg->getType()) {
	case Type_CCOutputMsg::CubeState:
		//작업예정
		pModel->setState(static_cast<CCOutputMsg_CubeState *>(msg));
		break;
	case Type_CCOutputMsg::CameraState:
	{
		auto state = static_cast<CCOutputMsg_CameraState *>(msg);
//		auto camera = getDefaultCamera();
		camera->setPosition3D(Vec3(state->transform[12], state->transform[13], state->transform[14]));
		camera->lookAt(Vec3(0, 0, 0),Vec3(state->transform[4], state->transform[5], state->transform[6]));
//		camera->setAdditionalTransform(Mat4(state->transform));
		break;
	}
	}
}
 