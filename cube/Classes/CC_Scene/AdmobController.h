#pragma once
#ifndef _ADMOB_CONTROLLER_H_
#define _ADMOB_CONTROLLER_H_

//#define CC_TARGET_PLATFORM CC_PLATFORM_ANDROID
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)||(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "firebase/admob/types.h"
#include "firebase/admob/banner_view.h"

#endif


class AdmobController
{
private:
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)||(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	firebase::admob::BannerView* banner_view;
	firebase::admob::AdSize ad_size;
#endif
public:
	static AdmobController* getInstance() {
		static AdmobController instance;
		return &instance;
	}

	void initialize();
	void func_admob(float f);

};


#endif // !_ENTRY_POINT_CC_H_
