
#include"stdafx.h"

#include "EntryPoint_CC.h"
#include "SimpleAudioEngine.h"

#include"MyUtility_NEJ\MyDebugUtility.h"
#include"MyUtility_NEJ\MyINI.h"

#include"Module_Manager\StartupManager.h"
#include"Module_Manager\CCSceneManager.h"
#include"Module_Manager\ResourceManager.h"
#include"AdmobController.h"



USING_NS_CC;

cocos2d::Scene * EntryPoint_CC::createScene()
{
	return EntryPoint_CC::create();
}

bool EntryPoint_CC::init()
{
	if (!Scene::init())
	{
		return false;
	} 
	AdmobController::getInstance()->initialize();
	if (!StartupManager::getInstance()->startup())return false;
	if (!ResourceManager::getInstance()->initialize())return false;
	if (!CCSceneManager::getInstance()->initialize() )return false;

	return true;
}

void EntryPoint_CC::onEnter()
{
	Scene::onEnter();

	

	Director::getInstance()->replaceScene(CCSceneManager::getInstance()->getStartScene());
	//Director::getInstance()->replaceScene(CCSceneManager::getInstance()->getInGameScene());
}
