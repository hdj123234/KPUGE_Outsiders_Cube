#pragma once

#include "cocos2d.h"

#include<vector>
#include<functional>

class ICCNode;
class ICCAudio;

struct ScheduleData {
	bool bOnce;
	std::function<void(float)> callback;
	float interval;
	unsigned int repeat;
	float delay;
	std::string key;
};

struct TinySceneData {
	std::vector<cocos2d::EventListener *> listeners;
	std::vector<ICCNode*> nodes;
	std::vector<ScheduleData> schedules;
	ICCAudio * bgm;
	TinySceneData() :bgm(nullptr) {}
};


class TinyScene_CC : public cocos2d::Scene {
private:
	TinySceneData sceneData;
public:
	static cocos2d::Scene* createScene(TinySceneData & sceneData);

	virtual bool init();
	virtual void onEnter();
	virtual void onExit();

	void initialize();

	// implement the "static create()" method manually
	CREATE_FUNC(TinyScene_CC);

private:
	virtual ~TinyScene_CC();

	void func(float f);
};


//
//class LogoScene : public cocos2d::Scene
//{
//private:
//	cocos2d::EventListenerTouchOneByOne *touchListener;
//public:
//	static cocos2d::Scene* createScene();
//
//	virtual bool init();
//	virtual void onEnter();
//	virtual void onExit();
//
//
//	void gotoNextScene(float f);
//
//	// implement the "static create()" method manually
//	CREATE_FUNC(LogoScene);
//
//};
// 
//
//
//class TitleScene : public cocos2d::Scene
//{
//public:
//	static cocos2d::Scene* createScene();
//
//	virtual bool init();
//
//	CREATE_FUNC(TitleScene);
//	 
//};
// 