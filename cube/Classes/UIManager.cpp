#include "stdafx.h"
#include "UIManager.h"
#include "InputManager.h"
#include "OutputManager.h"

UIManager::UIManager()
{
	inputManager = new InputManager();
	outputManager = new OutputManager();
}

UIManager::~UIManager()
{
	delete inputManager;
	inputManager = nullptr;
	delete outputManager;
	outputManager = nullptr;
}

void UIManager::setCCInputEvent(ICCInputEvent * _iccInputEvent)
{
	inputManager->setCCInputEvents(_iccInputEvent);
}

std::vector<IInputEvent*> UIManager::getInputEvents()
{
	return inputManager->getInputEvents();
}

std::vector<ICCOutputMsg*> UIManager::getCCOutputMsgs()
{
	return outputManager->getCCOutputMsgs();
}

void UIManager::setOutputMsgs(std::vector<IOutputMsg *> _outputMsg)
{
	outputManager->setOutputMsgs(_outputMsg);
}
