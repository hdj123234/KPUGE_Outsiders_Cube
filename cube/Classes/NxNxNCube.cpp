#include"stdafx.h"

#include "ITwistyPuzzle.h"
#include "EnumDefine.h"
#include "NxNxNCube.h"
#include "NxNxNCubeBone.h"
#include "InputEvent.h"
#include <random>


NxNxNCube::NxNxNCube(int _cubeSize, IPickableObject *_p)
	: cubeSize(_cubeSize), p(_p)
{
	cubeBone = new NxNxNCubeBone();
}

NxNxNCube::~NxNxNCube()
{
	delete cubeBone;
}

void NxNxNCube::initialize()
{
	cubeBone->initialize(cubeSize);
}

void NxNxNCube::reset()
{
	cubeBone->reset();
}

void NxNxNCube::recvInputEvent(IInputEvent * inputEvent)
{
	switch (inputEvent->getType())
	{
	case Type_InputEvent::Spin:
		auto data = static_cast<InputEvent_Spin*>(inputEvent);

		// InputEvent_Spin의 데이터를 RotateDir로 바꿉니다.
		auto nRotateDir = ((data->getAxisID() * 2) + 1) + ((data->isCW() - 1)*-1);

		cubeBone->pushRotate(static_cast<RotateDir>(nRotateDir), data->getLineID(), 3);
		break;

	}
}

IPickableObject* NxNxNCube::getPickableObjects()
{
	return this;
}

void NxNxNCube::setRotate(float dTime)
{
}

void NxNxNCube::getBlockState(std::vector<BlockState>& r)
{
	cubeBone->getBlockState(r);
}

void NxNxNCube::getSpinState(std::vector<CubeSpinState>& r)
{
	cubeBone->getSpinState(r);
}

void NxNxNCube::update(float dTime)
{
	cubeBone->update(dTime);
}

void NxNxNCube::shuffle(unsigned int _num,float _speed)
{
	unsigned char preDir = 255;
	unsigned char preLine = 255;
	unsigned char curDir = 255;
	unsigned char curLine = 255;

	float cubeSpeed;
	if (_speed == 0)
		cubeSpeed = cubeSize*1.1 + 0.4;
	else
		cubeSpeed = _speed;

	std::random_device rd;
	std::uniform_int_distribution<int> dist1(1, 6);
	std::uniform_int_distribution<int> dist2(0, cubeSize - 1);

	bool isSkiped = false;
	for (unsigned int i = 0; i < _num; ++i) {
		preDir = curDir;
		preLine = curLine;
		curDir = dist1(rd);
		curLine = dist2(rd);

		// 이전 회전방향 & 라인의 반대방향으로 도는거면 다시 랜덤을 돌린다.
		if (preLine == curLine &&
			(preDir - 1) % 2 != (curDir - 1) % 2) {
			//(preDir - 1) / 2 == (curDir - 1) / 2) {
			i--;
			continue;
		}

		// 회전 방향이 같기만 하다면 회전 횟수 1회 추가 
		if ((preDir - 1) / 2 == (curDir - 1) / 2) {
			if (!isSkiped) {
				i--;
				isSkiped = true;
				cubeBone->pushRotateCompulsion(static_cast<RotateDir>(curDir), curLine, cubeSpeed);
			}
			continue;
		}
		/* 이전 회전방향과 겹친다면 다시 랜덤을 돌린다.
		if ((preDir - 1) / 2 == (curDir - 1) / 2) {
			i--;
			continue;
		}
		*/
		cubeBone->pushRotateCompulsion(static_cast<RotateDir>(curDir), curLine, cubeSpeed);
		isSkiped = false;
	}
}

bool NxNxNCube::isSpinning()
{
	return cubeBone->isSpinning();
}

void NxNxNCube::skipShuffling(float _speed)
{
	cubeBone->setAllSpinSpeed(_speed);
}

void NxNxNCube::visit(OutputMsgCollector * p)
{
	cubeBone->visit(p);
}

bool NxNxNCube::checkClear()
{
	return cubeBone->checkClear();
}

Type_Puzzle NxNxNCube::getPuzzleType()
{
	return Type_Puzzle(static_cast<int>(Type_Puzzle::Square2x2x2Cube) + (cubeSize - 2));
}
