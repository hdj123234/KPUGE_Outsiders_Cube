#pragma once
#include "Utility.h"

enum class Type_InputEvent : unsigned char {
	Touch,

	Spin,
	RotationCamera,
	RotationCamera_AxisZ,
	ZoomCamera,
	StopRotationCamera,

	SelectOption,
	ExitOption,
	Start,
	AutoSuffle,
	Retry,
	Exit,
	Exception,
	ExitPause,

	Back,
};

enum class type_Exception : unsigned char {
	PhoneCall,
	etc
};

class IInputEvent
{
public:
	virtual void pure_virtual() = 0;
	virtual ~IInputEvent() {};
	virtual Type_InputEvent getType() = 0;
};
