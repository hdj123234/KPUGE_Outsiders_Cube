#include"stdafx.h"

#include "InGameScene.h"
#include "IUIManager.h"
#include "UIManager.h"
#include "InGameData.h"


InGameScene::InGameScene() : uiManager(nullptr), inGameData(nullptr)
{
}

InGameScene::~InGameScene()
{
	if (uiManager != nullptr) {
		delete uiManager;
		uiManager = nullptr;
	}
	if (inGameData != nullptr) {
		delete inGameData;
		inGameData = nullptr;
	}
}

std::vector<SceneMsg *>InGameScene::getSceneMsgs()
{
	// 
	auto retval = std::vector<SceneMsg *>();
	
	return retval;
}

void InGameScene::update(float dTime)
{
	auto vInput = uiManager->getInputEvents();
	inGameData->handleInputData(vInput);

	inGameData->update(dTime);

	std::vector<IOutputMsg*> outputMsgs;
	inGameData->getOutputMsgs(outputMsgs);
	uiManager->setOutputMsgs(outputMsgs);
}
