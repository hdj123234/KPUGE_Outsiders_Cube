#pragma once
#include <vector>
#include "Picker.h"

class ICCInputEvent;
class IInputEvent;
class SceneInitializer;
class InputEvent_Spin;

enum class InputState { Nothing, PuzzleSpin, CameraMove, CameraMoveZL, CameraMoveZR, CameraZoom };
enum class Type_Component : unsigned char;


struct TouchType {
	unsigned int id;
	PickingResult pickingResult;
	InputState inputState;
	float posX, posY;
	TouchType(unsigned int _id, PickingResult _pickingResult, InputState _inputState)
		:id(_id), pickingResult(_pickingResult), posX(-1), posY(-1), inputState(_inputState) {};
};

class InputManager
{
	friend SceneInitializer;

	std::vector<IInputEvent*> inputEvent;
	Picker &picker;

	unsigned int cube_size;
	unsigned int cube_size_square;

	std::vector<TouchType> vTouch;

	std::vector<ID_Face> vFaceRotationX, vFaceRotationY, vFaceRotationZ;
	void inputEventProcessing(ICCInputEvent* _inputEvent, TouchType* touchType);
	InputEvent_Spin* createSpinEvent(_PickingResult r1, _PickingResult r2);
public:
	InputManager();
	~InputManager();

	// 
	void setCubeSize(unsigned int _cube_size) {
		cube_size = _cube_size;
		cube_size_square = _cube_size*_cube_size;
	}
	void setCCInputEvents(ICCInputEvent* _inputEvent);

	// 인풋 이벤트가 저장된 벡터를 반환합니다.
	std::vector<IInputEvent*> getInputEvents();
};
