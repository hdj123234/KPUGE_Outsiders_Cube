#pragma once

enum class Type_OutputMsg : unsigned char
{
	CubeState,
	CameraState,
	RunAudio,
	AssistantComponent,
	SetTimer,
	Clear,
	Exit,
	Back,
};

class IOutputMsg
{
public:
	virtual void pure_virtual() = 0;
	virtual ~IOutputMsg() {};
	virtual Type_OutputMsg getType() = 0;
};