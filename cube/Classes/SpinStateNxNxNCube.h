#pragma once
#include "ISpinState.h"
#include "SpinStateNxNxNCube.h"
#include"BlockState.h"

enum class SpinDir : unsigned char {
	DIR_NOTHING = 0,
	DIR_XCW,
	DIR_XCCW,
	DIR_YCW,
	DIR_YCCW,
	DIR_ZCW,
	DIR_ZCCW
};

class SpinStateNxNxNCube : public ISpinState
{
	bool bOn;
	SpinDir dir;
	unsigned char line;
	float angle;
	float target_angle;
	float speed;
public:
	SpinStateNxNxNCube(SpinDir _dir, unsigned char _line, float _speed)
		: bOn(false), dir(_dir), line(_line), angle(0), target_angle(90), speed(_speed) {};
	SpinStateNxNxNCube(SpinDir _dir, unsigned char _line, float _angle, float _speed)
		: bOn(false), dir(_dir), line(_line), angle(_angle), target_angle(90), speed(_speed) {};
	virtual void on() { bOn = true; }
	virtual void off() { bOn = false; }
	virtual bool isOn() { return bOn; }
	virtual bool spin(float dTime);
	virtual void changeSpeed(float _speed) { speed = _speed; }
	virtual void scaleSpeed(float _speed) { speed *= _speed; }
	SpinDir getDir() { return dir; }
	unsigned char getLine() { return line; }
	virtual float getAngle() { return angle; }
	virtual float getSpeed() { return speed; }
	virtual float getTargetAngle() { return target_angle; }
	void changeTargetAngle(float _angle) { target_angle = _angle; }
	CubeSpinState makeSpinState() {
		unsigned char axisID = (static_cast<unsigned char>(dir) - 1) / 2 + 1;
		bool bCW = (static_cast<unsigned char>(dir) % 2) == 0;
		return CubeSpinState(axisID, line, bCW, angle);
	}
};
