#pragma once

enum class Type_Component : unsigned char {
	TimerLable,	// 타이머가 출력되는 레이블 입니다.
	OptionButton,	// 옵션 버튼입니다.
	OptionLable,	// 옵션 버튼을 누르면 나오는 옵션 레이블 입니다.
	ZRotationButton1,	// Z로테이션을 하기 위한 첫 번째 Z버튼 입니다.
	ZRotationButton2,	// Z로테이션을 하기 위한 두 번째 Z버튼 입니다.
	StartButton,	// 게임 시작 버튼입니다.
	AutoSuffleButton,	// 매뉴얼 모드에서의 오토셔플 버튼입니다.
	ManualSuffleButton,	// 매뉴얼 모드에서의 매뉴얼셔플 버튼입니다.
	ClearLable,	// 클리어 화면의 레이블 입니다.
	Retry,	// 클리어 레이블의 재시도 버튼입니다. 옵션레이블에서도 사용될 예정입니다.
	Exit,	// 클리어 레이블의 나가기 버튼입니다. 옵션레이블에서도 사용될 예정입니다.
	Pause,	// 일시정지 상태의 레이블 입니다.
	CheckScore,	// 미정
	StartButton_Manual,


	Option_BG,
	Option_Return,
	Option_MuteBGM,
	Option_MuteSE,
	Option_Title,

	Dummy = 255,
};
enum class ComponentCategory : unsigned char {
	Button,
	Label,
	Text,
	ClearLabel,
	ZButton,
	CheckBox
};

enum class Anchor_X :unsigned char { R, L, C };
enum class Anchor_Y :unsigned char { T, B, C };

class ComponentMaps {
public:
	static Type_Component getComponentType(const std::string & idString)
	{
		if (idString == "Timer") return (Type_Component::TimerLable);
		else if (idString == "OptionButton") return Type_Component::OptionButton;
		else if (idString == "ZRotation1") return Type_Component::ZRotationButton1;
		else if (idString == "ZRotation2") return Type_Component::ZRotationButton2;
		else if (idString == "Start(Auto)") return Type_Component::StartButton;
		else if (idString == "AutoSuffle") return Type_Component::AutoSuffleButton;
		else if (idString == "ManualSuffle") return Type_Component::ManualSuffleButton;
		else if (idString == "Start(Manual)") return Type_Component::StartButton_Manual;
		else if (idString == "Clear") return Type_Component::ClearLable;
		else if (idString == "Restart") return Type_Component::Retry;
		else if (idString == "ToTitle") return Type_Component::Exit;
		else if (idString == "Pause") return Type_Component::Pause;
		else if (idString == "Option_BG") return Type_Component::Option_BG;
		else if (idString == "Option_Return") return Type_Component::Option_Return;
		else if (idString == "Option_MuteBGM") return Type_Component::Option_MuteBGM;
		else if (idString == "Option_MuteSE") return Type_Component::Option_MuteSE;
		else if (idString == "Option_Title") return Type_Component::Option_Title;
		else return Type_Component::Dummy;
	}

	static ComponentCategory getComponentCategory(const std::string &str)
	{
		if (str == "Button")			return ComponentCategory::Button;
		else if (str == "ZButton")			return ComponentCategory::ZButton;
		else if (str == "ClearLabel")	return ComponentCategory::ClearLabel;
		else if (str == "Text")	return ComponentCategory::Text;
		else if (str == "Label")	return ComponentCategory::Label;
		else if (str == "CheckBox")	return ComponentCategory::CheckBox;
		else return ComponentCategory::Button;
	}
	static Anchor_X getAnchorX(char c)
	{
		if (c == 'R') return Anchor_X::R;
		else if (c == 'L') return Anchor_X::L;
		else if (c == 'C') return Anchor_X::C;
		else return Anchor_X::C;
	}
	static Anchor_Y getAnchorY(char c)
	{
		if (c == 'T') return Anchor_Y::T;
		else if (c == 'B') return Anchor_Y::B;
		else if (c == 'C') return Anchor_Y::C;
		else return Anchor_Y::C;
	}
};

inline cocos2d::Vec2 getAnchor(Anchor_X x, Anchor_Y y)
{
	cocos2d::Vec2 anchor;
	switch (x) {
	case Anchor_X::L: anchor.x = 0;		break;
	case Anchor_X::R: anchor.x = 1;		break;
	case Anchor_X::C: anchor.x = 0.5;	break;
	}
	switch (y) {
	case Anchor_Y::T: anchor.y = 1;		break;
	case Anchor_Y::B: anchor.y = 0;		break;
	case Anchor_Y::C: anchor.y = 0.5;	break;
	}
	return anchor;
}
