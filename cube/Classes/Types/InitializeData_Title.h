#pragma once

#include"math\Vec2.h"
#include"math\Vec3.h"

struct INI_TitleItem_Select {
	std::string buttonL_Normal;
	std::string buttonL_Press;
	std::string buttonR_Normal;
	std::string buttonR_Press;
	float buttonScaleX;
	float buttonGab;
	float itemScaleX;
	float posY;
	Anchor_Y anchorY;
	unsigned char defaultItemID;
	std::map<unsigned char, std::string> items;
};

struct INI_TitleItem_Button {
	std::string image1;
	std::string image2;
	float scaleX;
	float posX;
	float posY;
	Anchor_Y anchorY;
	unsigned char buttonID;
};

struct INI_Title {
	struct BackButton {
		std::string image;
		Anchor_Y anchorY;
		Anchor_X anchorX;
		cocos2d::Vec2 pos;
		float scaleX;
		float opacity;
		float timeOut;
	};
	struct TitleImage {
		std::string image;
		Anchor_Y anchorY;
		Anchor_X anchorX;
		cocos2d::Vec2 pos;
		float scaleX;
	};
	cocos2d::Vec2 posNorm;
	std::vector<INI_TitleItem_Button> buttons;
	INI_TitleItem_Select puzzle;
	INI_TitleItem_Select mode;
	BackButton backButton;
	TitleImage titleImage;
};