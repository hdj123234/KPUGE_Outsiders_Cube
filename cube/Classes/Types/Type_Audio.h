#pragma once

#include<string>
#include<map>
#include<set>

enum class Type_Audio : unsigned char {
	//인게임

	BGM_Main = 0,	// 브금 미정
	BGM2,
	BGM3,	// 브금 미정
	SE_Spin,
	SE_Spin1,
	SE_Spin2,
	SE_Spin3,
	SE_Spin4,
	SE_Spin5,
	SE_Spin6,
	SE_Spin7,
	SE_Clear,


	Logo_BGM,
	Title_BGM,
	_LastFlag,	//루프를 위해 사용됨
	Dummy = 254,
	None = 255,
};

inline const std::set<Type_Audio> getBGMList()
{
	return std::set<Type_Audio> {
		Type_Audio::BGM_Main,
			Type_Audio::BGM2,
			Type_Audio::BGM3,
			Type_Audio::Logo_BGM,
			Type_Audio::Title_BGM,
	};
}


inline std::map<Type_Audio, std::string> getParamNameMap()
{
	return std::map<Type_Audio, std::string>	{
		std::make_pair(Type_Audio::BGM_Main, "bgm_main"),
			std::make_pair(Type_Audio::BGM2, "bgm2"),
			std::make_pair(Type_Audio::BGM3, "bgm3"),
			std::make_pair(Type_Audio::SE_Spin1, "se_spin1"),
			std::make_pair(Type_Audio::SE_Spin2, "se_spin2"),
			std::make_pair(Type_Audio::SE_Spin3, "se_spin3"),
			std::make_pair(Type_Audio::SE_Spin4, "se_spin4"),
			std::make_pair(Type_Audio::SE_Spin5, "se_spin5"),
			std::make_pair(Type_Audio::SE_Spin6, "se_spin6"),
			std::make_pair(Type_Audio::SE_Spin7, "se_spin7"),
			std::make_pair(Type_Audio::SE_Clear, "se_clear"),
			std::make_pair(Type_Audio::Logo_BGM, "logo_bgm"),
			std::make_pair(Type_Audio::Title_BGM, "title_bgm"),
	};
}
