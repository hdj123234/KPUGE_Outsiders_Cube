#pragma once
#include <string>
#include"EnumDefine.h"

struct INI_Projection {
	float fov;
	float aspectRatio;
	float nearZ;
	float farZ;
	INI_Projection()
	{
		fov = 45.0f;
		nearZ = 0.001f;
		farZ = 2000.0f;
		aspectRatio = 1;
	}
};

struct INI_Camera {
	float distance;
	float rotationXY[2];
	float distanceRange[2];
	INI_Camera()
	{
		distance = 10;
		rotationXY[0] = 45;
		rotationXY[1] = 30;
		distanceRange[0] = 5;
		distanceRange[1] = 20;
	}
};


inline Type_Puzzle getCubeType(const std::string & rsType)
{
	if (rsType == "333")
		return Type_Puzzle::RubiksCube;
	else if (rsType == "222")
		return Type_Puzzle::Square2x2x2Cube;
	else if (rsType == "444")
		return Type_Puzzle::Square4x4x4Cube;
	else if (rsType == "555")
		return Type_Puzzle::Square5x5x5Cube;
	else
		return Type_Puzzle::DEBUG;
}