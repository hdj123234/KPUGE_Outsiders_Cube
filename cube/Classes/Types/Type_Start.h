#pragma once
#include"..\EnumDefine.h"

#include<string>


class StartOptionMaps {
public:
	static Type_Puzzle getPuzzleType(const std::string & idString)
	{
		if (idString == "2X2X2") return (Type_Puzzle::Square2x2x2Cube);
		else if (idString == "3X3X3") return Type_Puzzle::RubiksCube;
		else if (idString == "4X4X4") return Type_Puzzle::Square4x4x4Cube;
		else if (idString == "5X5X5") return Type_Puzzle::Square5x5x5Cube;

		else return Type_Puzzle::RubiksCube;
	}

	static Type_GameMode getModeType(const std::string &str)
	{
		if (str == "Auto")			return Type_GameMode::Auto;
		else if (str == "Manual")			return Type_GameMode::Manual;
		else if (str == "Free")	return Type_GameMode::Free;

		else return Type_GameMode::Auto;
	}
};