#pragma once

#include"AssistantComponentData.h"

#include"math\Vec2.h"
#include"math\Vec3.h"
#include"math\Vec4.h"

#include"InitializeData_Title.h"
#include"InitializeData_Camera.h"

//enum class Type_Component : unsigned char;
//enum class ComponentCategory : unsigned char;
//enum class Anchor_X : unsigned char;
//enum class Anchor_Y : unsigned char;

//struct AssistantComponentData;
enum class Type_Audio : unsigned char;
//enum class Anchor_Y : unsigned char;




struct INI_RenderTest {
	cocos2d::Vec4 bgColor;
	//		DirectX::XMFLOAT3 cameraPos;
	//		bool bCameraRotate;
	unsigned int blockCount;
	INI_RenderTest()
	{
		bgColor = cocos2d::Vec4(0, 0, 0, 1);
		cocos2d::Vec3 cameraPos = cocos2d::Vec3(0, 5, 20);
		bool bCameraRotate = true;
		unsigned int blockCount = 27;
	}

};

struct INI_Logo {
	std::string imageFile;
	float autoSkipTime;
	INI_Logo()
	{
		imageFile = "Logo.png";
		autoSkipTime = 2;
	}


};

struct INI_Credit {
	std::string imageFile;
	float imageScaleX;
	float scrollTime;
	std::string buttonImage1;
	std::string buttonImage2;
	Anchor_X buttonAnchor_x;
	Anchor_Y buttonAnchor_y;
	cocos2d::Vec2 buttonPos;
	float buttonScaleX;
	INI_Credit()
	{
		imageFile = "Logo.png";
		imageScaleX = 1;
		scrollTime =10;

	}


};
struct INI_Audio {
	std::map< Type_Audio, std::string > fileMap;
};

struct InitializeData {
	std::string iniPath;
	std::string resourcePath;
	INI_RenderTest renderTest;
	INI_Projection projection;
	std::map<unsigned char, INI_Camera> cameraMap;
	INI_Logo logo;
	INI_Title title;
	std::vector<AssistantComponentData *> assistantComponents;
	INI_Audio audio;
	INI_Credit credit;
	~InitializeData()
	{
		for (auto p : assistantComponents)
			if (p)delete p;
	}
};
