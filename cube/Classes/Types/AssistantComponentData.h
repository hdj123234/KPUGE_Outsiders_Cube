#pragma once

class IScene;
class SceneInitializer;

#include"math\Vec2.h"
#include"math\Vec3.h"
#include"math\Vec4.h"

//공용체를 사용하던 기존 코드를 전환
//데이터 은닉을 하지 않음


enum class Type_Component : unsigned char;
enum class ComponentCategory : unsigned char;
enum class Anchor_X : unsigned char;
enum class Anchor_Y : unsigned char;


//추상클래스
struct AssistantComponentData {
	Type_Component type;
	ComponentCategory category;

	virtual ~AssistantComponentData() {}
};

struct ACData_Button : public AssistantComponentData {
	virtual ~ACData_Button()  {}
	std::string image1;
	std::string image2;
	Anchor_X anchor_x;
	Anchor_Y anchor_y;
	cocos2d::Vec2 pos;
	float scale;
};

struct ACData_Label : public AssistantComponentData {
	virtual ~ACData_Label() {}
	std::string image;
	Anchor_X anchor_x;
	Anchor_Y anchor_y;
	cocos2d::Vec2 pos;
	float scale;
	float opacity;
};

struct ACData_Text : public AssistantComponentData {
	virtual ~ACData_Text() {}

	Anchor_X anchor_x;
	Anchor_Y anchor_y;
	cocos2d::Vec2 pos;
	cocos2d::Vec2 textBoxSize;
	cocos2d::Vec3 color;
	std::string fontFile;
	float fontSize;
};

struct ACData_ClearLabel : public AssistantComponentData {
	virtual ~ACData_ClearLabel() {}

	std::string image;
	Anchor_X anchor_x;
	Anchor_Y anchor_y;
	Anchor_X textAnchor_x;
	Anchor_Y textAnchor_y;
	cocos2d::Vec2 pos;
	cocos2d::Vec2 textPos;
	cocos2d::Vec2 textBoxSize;
	cocos2d::Vec3 color;
	float scale;
	std::string fontFile;
	float fontSize;
	float opacity;
	std::string subImage;
	Anchor_X subImageAnchor_x;
	Anchor_Y subImageAnchor_y;
	cocos2d::Vec2 subImagePos;
	float subImageScale;

};
struct ACData_ZButton : public AssistantComponentData {
	virtual ~ACData_ZButton() {}

	std::string image1;
	std::string image2;
	std::string bg;
	float scrollSizeX;
	float buttonSizeRate;
	float bgOpacity;
	float buttonOpacity;

};

struct ACData_CheckBox : public AssistantComponentData {
	virtual ~ACData_CheckBox() {}
	
	std::string		labelImage;
	Anchor_X		labelAnchor_x;
	Anchor_Y		labelAnchor_y;
	cocos2d::Vec2	labelPos;
	float			labelScale;

	std::string checkBoxImage1;
	std::string checkBoxImage2;
	Anchor_X	checkBoxAnchor_x;
	Anchor_Y	checkBoxAnchor_y;
	cocos2d::Vec2	checkBoxPos;
	float			checkBoxScale;
};
