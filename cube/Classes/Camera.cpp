#include "stdafx.h"
#include "Camera.h"

#include "Module_Manager\StartupManager.h"

#include"math\Vec3.h"
#include"math\Mat4.h"
#include"math\Quaternion.h"

#include"MyUtility_NEJ\MySimpleVector.h"
#include"Types\InitializeData_Camera.h"

//테스트용 생성자
Camera::Camera(unsigned char cameraID)
{
	auto ini = StartupManager::getInstance()->getCamera(cameraID);
	initialize(ini.distance, ini.rotationXY[0], ini.rotationXY[1]);

//	distanceRate = ini.distanceRate;
	distanceRange_sqrt[0] = ini.distanceRange[0]* ini.distanceRange[0];
	distanceRange_sqrt[1] = ini.distanceRange[1] * ini.distanceRange[1];
}

void Camera::initialize(float distance, float angleX, float angleY)
{
	using namespace cocos2d;


	Vec3 vPos = Vec3(0, 0, distance);
	Vec3 vR = Vec3(1, 0, 0);
	Vec3 vU = Vec3(0, 1, 0);
	Vec3 vL = Vec3(0, 0, 1);

	Mat4 m1, m2;
	Mat4::createRotationX(toRadian(angleX), &m1);
	Mat4::createRotationY(toRadian(angleY), &m2);
	Mat4 m = m2*m1;

	m.transformPoint(&vPos);
	m.transformPoint(&vR);
	m.transformPoint(&vU);
	m.transformPoint(&vL);

	transform = Mat4(vR.x, vR.y, vR.z, 0,
		vU.x, vU.y, vU.z, 0,
		vL.x, vL.y, vL.z, 0,
		vPos.x, vPos.y, vPos.z, 1);
	transform.transpose();

}

Matrix Camera::getMatrix()
{
	return transform;
}

bool Camera::changeDistance(float rate)
{
	float t[3] = { transform.m[12],	transform.m[13], transform.m[14] };
	transform.m[12] *= (rate);
	transform.m[13] *= (rate);
	transform.m[14] *= (rate);
	
	float dist_sqrt = (transform.m[12] * transform.m[12]) + (transform.m[13] * transform.m[13]) + (transform.m[14] * transform.m[14]);
	
	if (dist_sqrt > distanceRange_sqrt[1] || dist_sqrt < distanceRange_sqrt[0]) {
		transform.m[12] = t[0];
		transform.m[13] = t[1];
		transform.m[14] = t[2];
		return false;
	}
	return true;
}

void Camera::rotate(cocos2d::Vec3 axis, float dAngle)
{
	using namespace cocos2d;
	Quaternion q;
	Quaternion::createFromAxisAngle(axis, toRadian(dAngle), &q);
	Mat4 m;
	transform.transpose();
	Mat4::createRotation(q, &m);
	transform = transform*m;

	transform.transpose();
}

void Camera::rotateZ(float dAngle)
{
	using namespace cocos2d;
	Quaternion q;
	Quaternion::createFromAxisAngle(Vec3(transform.m[8], transform.m[9], transform.m[10]), toRadian(dAngle), &q);
	Mat4 m;
	Mat4::createRotation(q, &m);
	transform.transpose();
	transform = transform*m;
	//	XMStoreFloat4x4(&transform, XMLoadFloat4x4(&transform)*XMMatrixRotationAxis(XMVectorSet(transform._31, transform._32, transform._33, 0), XMConvertToRadians(dAngle)));
	transform.transpose();
}

void Camera::rotateXY(float dx, float dy, float dAngle)
{
	using namespace cocos2d;
	Quaternion q;

	Quaternion::createFromAxisAngle((dy*Vec3(transform.m[0], transform.m[1], transform.m[2])
		+ dx*Vec3(transform.m[4], transform.m[5], transform.m[6])).getNormalized(), -toRadian(dAngle), &q);
	//	Quaternion::createFromAxisAngle(Vec3(dx*transform.m[4], dx* transform.m[5], dx* transform.m[6]).getNormalized() , toRadian(dAngle), &q);
	Mat4 m;
	Mat4::createRotation(q, &m);
	transform.transpose();
	transform = transform*m;
	//	using namespace DirectX;
	//	if (dx == 0 && dy == 0)return;
	//	XMVECTOR axis = XMVector3NormalizeEst(	dy*   XMVectorSet(transform._11, transform._12, transform._13, 0) +
	//											dx*   XMVectorSet(transform._21, transform._22, transform._23, 0));
	//
	//	XMStoreFloat4x4(&transform, XMLoadFloat4x4(&transform)*XMMatrixRotationAxis(axis, XMConvertToRadians(dAngle)));
	transform.transpose();
}