#pragma once
#include "IScene.h"

#include<vector>

class InGameData;
class SceneInitializer;

//추후제거
class SceneTestScene;


class InGameScene : public IScene
{
	friend SceneInitializer;
	IUIManager *uiManager;
	InGameData *inGameData;

	//추후제거
	friend SceneTestScene;

public:
	virtual void pure_virtual() {};
	InGameScene();
	~InGameScene();

	std::vector<SceneMsg*> getSceneMsgs();
	void update(float dTime);
	IUIManager* getUIManager() { return uiManager; }
};