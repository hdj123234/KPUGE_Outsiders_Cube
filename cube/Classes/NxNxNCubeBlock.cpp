#include"stdafx.h"

#include "NxNxNCubeBlock.h"


AxisDir NxNxNCubeBlock::findAxis(Axis _a)
{
	if (_a.x == 1)
		return AxisDir::DIR_XP;
	if (_a.x == -1)
		return AxisDir::DIR_XM;
	if (_a.y == 1)
		return AxisDir::DIR_YP;
	if (_a.y == -1)
		return AxisDir::DIR_YM;
	if (_a.z == 1)
		return AxisDir::DIR_ZP;
	if (_a.z == -1)
		return AxisDir::DIR_ZM;
	return AxisDir::DIR_ERROR;
}

void NxNxNCubeBlock::rotateColor(RotateDir _dir)
{
	unsigned char temp[6];
	switch (_dir)
	{
	case RotateDir::DIR_XCW:
		temp[0] = color[1];
		temp[5] = color[0];
		temp[4] = color[5];
		temp[1] = color[4];
		temp[2] = color[2];
		temp[3] = color[3];
		break;
	case RotateDir::DIR_XCCW:
		temp[0] = color[5];
		temp[1] = color[0];
		temp[4] = color[1];
		temp[5] = color[4];
		temp[2] = color[2];
		temp[3] = color[3];
		break;
	case RotateDir::DIR_YCW:
		temp[1] = color[3];
		temp[2] = color[1];
		temp[3] = color[5];
		temp[5] = color[2];
		temp[0] = color[0];
		temp[4] = color[4];
		break;
	case RotateDir::DIR_YCCW:
		temp[1] = color[2];
		temp[2] = color[5];
		temp[3] = color[1];
		temp[5] = color[3];
		temp[0] = color[0];
		temp[4] = color[4];
		break;
	case RotateDir::DIR_ZCW:
		temp[0] = color[2];
		temp[2] = color[4];
		temp[3] = color[0];
		temp[4] = color[3];
		temp[1] = color[1];
		temp[5] = color[5];
		break;
	case RotateDir::DIR_ZCCW:
		temp[0] = color[3];
		temp[2] = color[0];
		temp[3] = color[4];
		temp[4] = color[2];
		temp[1] = color[1];
		temp[5] = color[5];
		break;
	}
	for (auto i = 0; i < 6; ++i)
		color[i] = temp[i];
}

NxNxNCubeBlock::NxNxNCubeBlock(int _id, int _x, int _y, int _z)
	: id(_id), pos{ _x,_y,_z }, color{ 0,0,0,0,0,0 }
{
	localRight = { 1,0,0 };
	localLook = { 0,0,1 };
}

NxNxNCubeBlock::NxNxNCubeBlock(int _id, int _x, int _y, int _z, unsigned char _color[6])
	: id(_id), pos{ _x,_y,_z },
	color{ _color[0],_color[1],_color[2],_color[3],_color[4],_color[5] }
{
	localRight = { 1,0,0 };
	localLook = { 0,0,1 };
}



NxNxNCubeBlock::~NxNxNCubeBlock()
{
}

int NxNxNCubeBlock::getPosID(int _cube_size)
{
	return pos[0] + (pos[1] * _cube_size * _cube_size) + pos[2] * _cube_size;
}

BlockState NxNxNCubeBlock::getState(int _cube_size)
{
	BlockState state;
	state.blockID = static_cast<unsigned char>(id);
	state.posID = static_cast<unsigned char>(getPosID(_cube_size));
	state.localRight = findAxis(localRight) - 1;
	state.localLook = findAxis(localLook) - 1;
	return state;
}

void NxNxNCubeBlock::rotateCube(int _cubeSize, RotateDir _dir)
{
	int mid = _cubeSize - 1;
	pos[0] *= 2; pos[1] *= 2; pos[2] *= 2;
	pos[0] -= mid; pos[1] -= mid; pos[2] -= mid;

	int pos_temp[3];
	Axis lar_temp;	// local axis right temp
	Axis lal_temp;	// local axis look temp
	switch (_dir) {
	case RotateDir::DIR_XCW:
		pos_temp[0] = pos[0];
		pos_temp[1] = pos[2];
		pos_temp[2] = -pos[1];
		lar_temp = { localRight.x, localRight.z,-localRight.y };
		lal_temp = { localLook.x, localLook.z,-localLook.y };
		//m = { 1,0,0,0,0,1,0,-1,0 };
		break;
	case RotateDir::DIR_XCCW:
		pos_temp[0] = pos[0];
		pos_temp[1] = -pos[2];
		pos_temp[2] = pos[1];
		//m = { 1,0,0,0,0,-1,0,1,0 };
		lar_temp = { localRight.x, -localRight.z,localRight.y };
		lal_temp = { localLook.x, -localLook.z,localLook.y };
		break;
	case RotateDir::DIR_YCW:
		pos_temp[0] = -pos[2];
		pos_temp[1] = pos[1];
		pos_temp[2] = pos[0];
		//m = { 0,0,-1,0,1,0,1,0,0 };
		lar_temp = { -localRight.z, localRight.y,localRight.x };
		lal_temp = { -localLook.z, localLook.y,localLook.x };
		break;
	case RotateDir::DIR_YCCW:
		pos_temp[0] = pos[2];
		pos_temp[1] = pos[1];
		pos_temp[2] = -pos[0];
		//m = { 0,0,1,0,1,0,-1,0,0 };
		lar_temp = { localRight.z, localRight.y,-localRight.x };
		lal_temp = { localLook.z, localLook.y,-localLook.x };
		break;
	case RotateDir::DIR_ZCW:
		pos_temp[0] = pos[1];
		pos_temp[1] = -pos[0];
		pos_temp[2] = pos[2];
		lar_temp = { localRight.y, -localRight.x,localRight.z };
		lal_temp = { localLook.y, -localLook.x,localLook.z };
		//m = { 0,1,0,-1,0,0,0,0,1 };
		break;
	case RotateDir::DIR_ZCCW:
		pos_temp[0] = -pos[1];
		pos_temp[1] = pos[0];
		pos_temp[2] = pos[2];
		lar_temp = { -localRight.y, localRight.x,localRight.z };
		lal_temp = { -localLook.y, localLook.x,localLook.z };
		//m = { 0,-1,0,1,0,0,0,0,1 };
		break;
	}
	pos[0] = mid + pos_temp[0];
	pos[1] = mid + pos_temp[1];
	pos[2] = mid + pos_temp[2];
	pos[0] /= 2; pos[1] /= 2; pos[2] /= 2;
	localRight = lar_temp;
	localLook = lal_temp;
	rotateColor(_dir);
}

unsigned char NxNxNCubeBlock::getColor(unsigned char _dir)
{
	if (_dir < 6)
		return color[_dir];
	return 0;
}

