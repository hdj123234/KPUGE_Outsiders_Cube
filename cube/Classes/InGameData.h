#pragma once
#include "ISpace.h"
#include "OutputMsgCollector.h"




class SceneInitializer;
class ITwistyPuzzle;
class Camera;
class TimerLabel;
class IAssistantComponent;
enum class Type_GameMode : unsigned char;

enum class GameState { 
	InputShuffle,	// 오토셔플을 입력하는 상태
	Shuffling,		// 셔플중인 상태(입력 불가)
	ManualShuffle,	// 매뉴얼 셔플을 입력중인 상태
	GameStart,		// 게임 시작 초기화 상태
	Play,			// 게임 중 상태
	Pause,			// 게임 일시정지 상태
	Clear			// 클리어 상태
};
class IInputEvent;
class IOutputMsg;

class InGameData : public ISpace, IOutputMsgTarget
{
	friend SceneInitializer;

	ITwistyPuzzle *twistyPuzzle;
	Type_GameMode gameMode;
	Camera *camera;
	TimerLabel *timeLabel;
	std::vector<IAssistantComponent*> assistantComponents;
	GameState gameState;
	GameState preState;


	OutputMsgCollector outputMsgCollector;

public:
	InGameData();
	virtual ~InGameData();

	void handleInputData(std::vector<IInputEvent*> v);
	//std::vector<IOutputMsg*> getOutputMsgs();
	void getOutputMsgs(std::vector<IOutputMsg*> &r);
	virtual std::vector<IPickableObject*> getObjects();
	virtual ICamera* getCamera();
	virtual Type_Puzzle getPuzzleType();

	virtual void visit(OutputMsgCollector *p);
	void update(float dTime);
};