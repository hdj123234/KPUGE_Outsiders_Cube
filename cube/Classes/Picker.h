#pragma once

#include"math/Vec3.h"

class ISpace; 
typedef unsigned char ID_Pos;
enum ID_Face : unsigned char{	//1���� ����
	Face_Up = 1,
	Face_Front,
	Face_Left,
	Face_Right,
	Face_Down,
	Face_Back
};


struct _PickingResult
{
	ID_Face faceID;
	ID_Pos posID;
};

struct PickingResult {
	bool bSuccess;
	_PickingResult result;

};


class Picker
{
private:
	struct CollisionPoint {
		bool bCollide;
		cocos2d::Vec3 point;

	};
private:
	struct Ray {
		cocos2d::Vec3 startPoint;
		cocos2d::Vec3 dir;
	};
	struct Target {
		ID_Face faceID;
		ID_Pos posID;
		std::vector<cocos2d::Vec3> polygon;
	};

	ISpace *space;
public:
	void setSpace(ISpace* _space);
	PickingResult pick(float posX, float posY);

private:
	bool isCollided(Ray &ray,Target &target);
	cocos2d::Vec4 getPlaneFromPoints(cocos2d::Vec3 p1, cocos2d::Vec3 p2, cocos2d::Vec3 p3);
	CollisionPoint getCollistionPoint(cocos2d::Vec4 plane, cocos2d::Vec3 p1, cocos2d::Vec3 p2);
	float getAngleBetweenVectors(cocos2d::Vec3 v1, cocos2d::Vec3 v2);
};

