#pragma once
#include "IUIManager.h"

class InputManager;
class OutputManager;
class SceneInitializer;

class UIManager : public IUIManager {
	friend SceneInitializer;

	InputManager *inputManager;
	OutputManager *outputManager;

public:
	UIManager();
	~UIManager();

	// 코코스에서 발생한 인풋이벤트를 UI매니저에게 넘겨주는 코코스 전용 함수
	virtual void setCCInputEvent(ICCInputEvent* _iccInputEvent);

	// UI매니저에 저장된 인풋이벤트를 가져오는 함수
	virtual std::vector<IInputEvent *> getInputEvents();

	// 코코스에서 처리할 아웃풋 메시지를 UI매니저에게서 받아가는 코코스 전용 함수
	virtual std::vector<ICCOutputMsg*> getCCOutputMsgs();

	// 게임에서 발생한 아웃풋 메시지를 UI매니저에 저장하는 함수
	virtual void setOutputMsgs(std::vector<IOutputMsg *> _outputMsg);
};