#pragma once

#include <vector>
#include <array>
#include "BlockState.h"
#include "Utility.h"
#include "OutputMsgCollector.h"

class NxNxNCubeBlock;
enum class RotateDir : unsigned char;
class IPickableObject;
class ISpinState;
class IOutputMsg;

enum NxNxNCubeBoneState {
	State_Stop,		// 조작이 없는 상태
	State_Rotate,	// 조작이 끝나 회전하는 상태
	State_Replace	// 새로 바뀐 형태로 다시 저장하는 상태
};

class NxNxNCubeBone : public IOutputMsgTarget
{
	std::vector<std::vector<std::vector<NxNxNCubeBlock*>>> vCubeBlock;
	std::vector<ISpinState*> spinStates;
	NxNxNCubeBoneState state = NxNxNCubeBoneState::State_Rotate;;
	int cube_size;	// 큐브의 크기 

	bool bWork;
	bool bSpinCompleteSound;

	bool rotateCube(RotateDir _dir, int _linenumber);
	void spinUpdate(float dTime);
public:
	virtual void pure_virtual() {};
	NxNxNCubeBone();
	virtual ~NxNxNCubeBone();
	void initialize(int _size);
	void reset();
	void update(float dTime);
	std::vector<NxNxNCubeBlock*> getPickableObjects();
	const NxNxNCubeBoneState getState() { return state; }
	void setState(NxNxNCubeBoneState _state) { state= _state; }

	void pushRotate(RotateDir _dir, int _linenumber, float _speed);
	void pushRotateCompulsion(RotateDir _dir, int _linenumber, float _speed);
	void getBlockState(std::vector<BlockState> &r);
	void getSpinState(std::vector<CubeSpinState> &r);
	void setAllSpinSpeed(float _speed);
	bool checkClear();
	bool isSpinning() { return spinStates.size() != 0; }

	virtual void visit(OutputMsgCollector *p);
};