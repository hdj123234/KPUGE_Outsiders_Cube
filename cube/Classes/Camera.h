#pragma once
#include "ICamera.h"

#include"math/Vec3.h"

class Camera : public ICamera
{	
	Matrix transform;	//월드변환행렬
//	float distanceRate;
	float distanceRange_sqrt[2];


public:
	//테스트용 생성자
	Camera(unsigned char cameraID);

	void initialize(float distance, float angleX, float angleY);
	virtual Matrix getMatrix();
	bool changeDistance(float rate);
	void rotate(cocos2d::Vec3 axis,float dAngle);
	void rotateZ(float dAngle);
	void rotateXY(float dx, float dy, float dAngle);
};
