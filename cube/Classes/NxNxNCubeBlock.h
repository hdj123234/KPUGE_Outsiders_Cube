#pragma once
#include "BlockState.h"

class IOutputMsg;
enum class RotateDir : unsigned char {
	DIR_NOTHING = 0,
	DIR_XCW,
	DIR_XCCW,
	DIR_YCW,
	DIR_YCCW,
	DIR_ZCW,
	DIR_ZCCW
};

struct Axis {
	char x, y, z;
};

enum AxisDir : char {
	DIR_ERROR,
	DIR_XP,
	DIR_XM,
	DIR_YP,
	DIR_YM,
	DIR_ZP,
	DIR_ZM,
};

class NxNxNCubeBlock
{
	int id;
	int pos[3];
	Axis localRight;
	Axis localLook;

	unsigned char color[6];

	AxisDir findAxis(Axis _a);
	void rotateColor(RotateDir _dir);
public:
	NxNxNCubeBlock(int _id, int _x, int _y, int _z);
	NxNxNCubeBlock(int _id, int _x, int _y, int _z, unsigned char _color[6]);
	~NxNxNCubeBlock();

	int getBlockID() { return id; };
	int getPosID(int _cube_size);

	BlockState getState(int _cube_size);
	const int getX() { return pos[0]; }
	const int getY() { return pos[1]; }
	const int getZ() { return pos[2]; }
	void setRocate(int _x, int _y, int _z) { pos[0] = _x; pos[1] = _y; pos[2] = _z; }
	void rotateCube(int _cubeSize, RotateDir _dir);
	AxisDir getLocalRight() { return findAxis(localRight); }
	AxisDir getLocalLook() { return findAxis(localLook); }
	unsigned char getColor(unsigned char _dir);
};

